__author__ = 'ksm'
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------------------------------------------------------------
# Lib
#-----------------------------------------------------------------------------------------------------------------------
from flask import Response
import hashlib
import time
from datetime import datetime, timedelta
import platform
import os
import logging
from logging import handlers
import qrcode
import StringIO
from otpauth import OtpAuth
import smtplib
import email.utils
from email.header import Header
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

# Lib - scaffold
from models import *
from env import *

#-----------------------------------------------------------------------------------------------------------------------
# Static
#-----------------------------------------------------------------------------------------------------------------------
TYPE_SELF_PK = 0
TYPE_UNKNOWN = "UNKNOWN"
#user active type
TYPE_ACTIVE = "Active"
TYPE_SUSPEND = "Suspend"
#user type
TYPE_SUPER = "Super"
TYPE_ADMIN = "Admin"
TYPE_MANAGER = "Manager"
TYPE_MEMBER = "Member"
TYPE_GUEST = "Guest"
#agent type
TYPE_AGENT = "Agent"
#call type
TYPE_DEV_INIT = "device_init"
TYPE_DT_INIT = "detail_init"
TYPE_DT_POLL = "detail_poll"
#device type
TYPE_DESKTOP = "Desktop"
TYPE_TABLET = "Tablet"
TYPE_MOBILE = "Mobile"
TYPE_LAPTOP = "Laptop"
#device os
TYPE_WINDOWS = "Windows"
TYPE_LINUX = "Linux"
TYPE_OS_X = "OS_X"
TYPE_ANDROID = "Android"
TYPE_IOS = "IOS"

FATAL_LOG = "FATAL"
ERROR_LOG = "ERROR"
WARN_LOG = "WARN"
INFO_LOG = "INFO"

OTP_ISSUER = ""

platform = platform.system()
if platform == "Linux":
    LOG_DEFAULT_DIR = '/var/log/lmm/'
elif platform == "Windows":
    LOG_DEFAULT_DIR = './log/'
elif platform == "Darwin":
    LOG_DEFAULT_DIR = './log/'

log_file = {
    "history": "history.log",
    "debug": "debug.log"
}
#-----------------------------------------------------------------------------------------------------------------------
# Class
#-----------------------------------------------------------------------------------------------------------------------
# Singleton Source from http://stackoverflow.com/questions/42558/python-and-the-singleton-pattern
class Singleton:
    """
    A non-thread-safe helper class to ease implementing singletons.
    This should be used as a decorator -- not a metaclass -- to the
    class that should be a singleton.

    The decorated class can define one `__init__` function that
    takes only the `self` argument. Other than that, there are
    no restrictions that apply to the decorated class.

    To get the singleton instance, use the `Instance` method. Trying
    to use `__call__` will result in a `TypeError` being raised.

    Limitations: The decorated class cannot be inherited from.
    """

    def __init__(self, decorated):
        self._decorated = decorated

    def instance(self):
        """
        Returns the singleton instance. Upon its first call, it creates a
        new instance of the decorated class and calls its `__init__` method.
        On all subsequent calls, the already created instance is returned.

        """
        try:
            return self._instance
        except AttributeError:
            self._instance = self._decorated()
            return self._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `Instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)

@Singleton
class TokenManager(object):
    def set_token(self, id):
        m = hashlib.sha1()

        m.update(str(id))
        m.update(datetime.now().isoformat())

        return m.hexdigest()

    def validate_token(self, token):
        token = Tokens.query.filter_by(token=token).first()

        if token is None:
            return False
        return True

    def clear_token(self, pk):
        old_token_list = Tokens.query.filter_by(member_fk=pk).all()
        if len(old_token_list) != 0:
            for old_token in old_token_list:
                db.session.delete(old_token)
                db.session.commit()

    def validate_agent_token(self, agent_token):
        var_device = None
        if agent_token is not None:
            var_device = Devices.query.filter_by(agent_token=agent_token).first()
        if var_device is None:
            return False
        return True

class ProcessManager(object):
    # GET User
    def get_user(self, pk):
        user = Users.query.filter_by(pk=pk).first()

        if user is None:
            return False
        return user
    # GET User Token
    def get_token(self, token):
        token = Tokens.query.filter_by(token=token).first()

        if token is None:
            return False
        return token
    # GET User Type
    def check_user_type(self, type):
        rank_type = TYPE_UNKNOWN
        if type is not None:
            # type: Member
            if type == 1:
                rank_type = TYPE_MEMBER
            # type: Manager
            elif type == 2:
                rank_type = TYPE_MANAGER
            # type: Admin
            elif type == 3:
                rank_type = TYPE_ADMIN
            # type: SuperAdmin
            elif type == 4:
                rank_type = TYPE_SUPER
            else:
                rank_type = TYPE_GUEST
        return rank_type
    # GET User Auth
    def check_user_auth(self, auth):
        auth_obj = []
        if auth is not None:
            # auth: Device Information Modified
            if auth & 1:
                auth_obj.append(2)
            # auth: Device Control
            if auth & 2:
                auth_obj.append(4)
            # auth: View Report
            if auth & 4:
                auth_obj.append(8)
        return auth_obj
    # GET User Active Status
    def check_user_active(self, flag):
        var_flag = TYPE_SUSPEND
        if flag:
            var_flag = TYPE_ACTIVE
        else:
            var_flag = TYPE_SUSPEND
        return var_flag
    # GET Device
    def get_device(self, agent_token):
        device = Devices.query.filter_by(agent_token=agent_token).first()

        if device is None:
            return False
        return device
    # GET Device Detail
    def get_detail_device(self, device):
        var_os_id = device.dev_os_id
        if var_os_id == -1:
            detail_device = DevWindows.query.filter_by(device_fk=device.pk).first()
        elif var_os_id == 0:
            detail_device = DevWindows.query.filter_by(device_fk=device.pk).first()
        elif var_os_id == 1:
            detail_device = DevLinux.query.filter_by(device_fk=device.pk).first()
        elif var_os_id == 2:
            detail_device = DevOSX.query.filter_by(device_fk=device.pk).first()
        elif var_os_id == 3:
            detail_device = DevAndroid.query.filter_by(device_fk=device.pk).first()
        elif var_os_id == 4:
            detail_device = DevIOS.query.filter_by(device_fk=device.pk).first()

        if detail_device is None:
            return False
        return detail_device
    # GET Device Detail Model
    def get_detail_device_model(self, device):
        var_os_id = device.dev_os_id
        if var_os_id == -1:
            detail_device_model = DevWindows
        elif var_os_id == 0:
            detail_device_model = DevWindows
        elif var_os_id == 1:
            detail_device_model = DevLinux
        elif var_os_id == 2:
            detail_device_model = DevOSX
        elif var_os_id == 3:
            detail_device_model = DevAndroid
        elif var_os_id == 4:
            detail_device_model = DevIOS

        return detail_device_model
    # GET Device Type
    def get_device_type(self, type_id):
        var_type = ""
        if type_id == 0:
            var_type = TYPE_UNKNOWN
        elif type_id == 1:
            var_type = TYPE_DESKTOP
        elif type_id == 2:
            var_type = TYPE_TABLET
        elif type_id == 3:
            var_type = TYPE_MOBILE
        elif type_id == 4:
            var_type = TYPE_LAPTOP
        return var_type
    # GET Device OS Ver
    def get_device_os(self, os_id):
        var_os = ""
        if os_id == 0:
            var_os = TYPE_UNKNOWN
        elif os_id == 1:
            var_os = TYPE_WINDOWS
        elif os_id == 2:
            var_os = TYPE_LINUX
        elif os_id == 3:
            var_os = TYPE_OS_X
        elif os_id == 4:
            var_os = TYPE_ANDROID
        elif os_id == 5:
            var_os = TYPE_IOS
        return var_os

class SmtpManager(object):
    def __init__(self):
        self.frm = "system@linkmemine.com"
        self.smtp_server = "smtp.mandrillapp.com"
        self.port = 587
        self.user_id = "hchkim@unet.kr"
        self.user_pw = "T238oVg4BMzaqAbSEOsQaQ"

    def send_mail(self, to_user, subject, message):
        # create message
        msg = MIMEMultipart("alternative")
        msg['From'] = email.utils.formataddr(('LMM Author', self.frm))
        msg['To'] = email.utils.formataddr(('LMM Recipient', to_user))
        msg["Subject"] = Header(s=subject, charset="utf-8")
        msg.attach(MIMEText(message, "html", _charset="utf-8"))

        # send
        smtp = smtplib.SMTP(self.smtp_server, self.port)
        smtp.login(self.user_id, self.user_pw)
        smtp.sendmail(self.frm, to_user, msg.as_string())
        smtp.close()

# Class: Logger
#   make history log
class LogManager(object):
    @staticmethod
    def logger(api_name, file_name, debug_info):
        log_dir_path = LOG_DEFAULT_DIR

        try:
            if not os.path.exists(log_dir_path):
                os.mkdir(log_dir_path)

            full_path = '%s/%s' % (log_dir_path, file_name)

            # create logger
            debug_logger = logging.getLogger(api_name)
            debug_logger.setLevel(logging.DEBUG)
            #create filter
            f = ContextFilter()
            f.set_info(debug_info)
            debug_logger.addFilter(f)
            # create file handler
            fh = handlers.RotatingFileHandler(
                full_path,
                maxBytes=(1024 * 1024 * 10),
                backupCount=5
            )
            fh.setLevel(logging.DEBUG)
            # create console handler
            ch = logging.StreamHandler()
            ch.setLevel(logging.ERROR)
            # create formatter
            var_fmt = '%(asctime)-28s %(name)-25s %(levelname)-8s IP: %(ip)-15s User: %(user)-30s Notice: %(notice)-53s Data: %(message)s'
            formatter = logging.Formatter(var_fmt)
            fh.setFormatter(formatter)
            ch.setFormatter(formatter)
            # add the handlers
            debug_logger.addHandler(fh)
            debug_logger.addHandler(ch)

            return debug_logger

        except IOError, e:
            print("Can't create log directory for '%s': \n  %s" % (file_name, str(e)))
            return logging.getLogger(api_name)

        except OSError, e:
            print("Can't create log directory for '%s': \n  %s" % (file_name, str(e)))
            return logging.getLogger(api_name)

class ContextFilter(logging.Filter):
    def __init__(self):
        self.debug_info = None

    def set_info(self, debug_info):
        self.debug_info = debug_info

    def filter(self, record):
        record.ip = self.debug_info["ip"]
        record.user = self.debug_info["user"]
        record.notice = self.debug_info["notice"]
        return True

#-----------------------------------------------------------------------------------------------------------------------
# Function
#-----------------------------------------------------------------------------------------------------------------------

# Func: Result
#   make response, html response define, log level define
#   ex) result(self.API_NAME, None, None, 200)
def result(api_name, objects, meta, status):
    html = {
        200: "OK",
        201: "Created",
        400: "Bad Request",
        401: "Unauthorized",
        404: "Not Found",
        408: "Request Timeout",
        500: "Internal Server Error",
        501: "Not Implemented",
        503: "Service Unavailable"
    }

    res = {
        "api": api_name,
        "status": status,
        "objects": objects,
        #"objects": json.dumps(objects, ensure_ascii=False),
        "meta": meta,
        "message": html[status]
    }
    return res

# Func: get_current_time
def get_current_time(type):
    if type == "UTC":
        now = time.gmtime()
    elif type == "LOCAL":
        now = datetime.now()

    var_current = str(now.tm_year).zfill(4)+'-'+str(now.tm_mon).zfill(2)+'-'+str(now.tm_mday).zfill(2)+' '+str(now.tm_hour).zfill(2)+':'+str(now.tm_min).zfill(2)+':'+str(now.tm_sec).zfill(2)
    return var_current

# Func: get_current_time
def get_interval_time(type, sign, interval_label, interval_value):
    if sign is None:
        if type == "UTC":
            now = time.gmtime()
        elif type == "LOCAL":
            now = datetime.now()
    else:

        if interval_label == "D":
            var_time_delta = timedelta(days=interval_value)
        elif interval_label == "S":
            var_time_delta = timedelta(seconds=interval_value)
        elif interval_label == "microS":
            var_time_delta = timedelta(microseconds=interval_value)
        elif interval_label == "milliS":
            var_time_delta = timedelta(milliseconds=interval_value)
        elif interval_label == "M":
            var_time_delta = timedelta(minutes=interval_value)
        elif interval_label == "H":
            var_time_delta = timedelta(hours=interval_value)
        elif interval_label == "W":
            var_time_delta = timedelta(weeks=interval_value)

        if sign == "+":
            if type == "UTC":
                now = datetime.utcnow() + var_time_delta
            elif type == "LOCAL":
                now = datetime.now() + var_time_delta
        elif sign == "-":
            if type == "UTC":
                now = datetime.utcnow() - var_time_delta
            elif type == "LOCAL":
                now = datetime.now() - var_time_delta

    return now

# Func: json_encode
#   make output json
def time_encode(thing):
    date_hh = str(thing).split(":")
    if hasattr(thing, 'isoformat'):
        if len(date_hh[0]) == 1:
            return "0" + thing.isoformat()
        return thing.isoformat()
    else:
        if len(date_hh[0]) == 1:
            return "0" + str(thing)
        return str(thing)

# Func: pw_encode
#   make hashed password
def pw_encode(password):
    pass1 = hashlib.sha1(password).digest()
    pass2 = hashlib.sha1(pass1).hexdigest()
    hashed_pw = "*" + pass2.upper()
    return hashed_pw

# Func: get_parse_name
#   get QRCode, Logo image name
def get_parse_name(email):
    user_email = "" + email
    res = user_email.replace("@", "_")
    img_name = res.replace(".", "_")
    return img_name

def get_user_id(email):
    user_email = "" + email
    user_id = user_email.replace("@", "[") + "]"
    return user_id

# Func: set_otp
#   createQRCode
def set_otp(email, pw, otp_key, server_key, server_label):
    auth = OtpAuth(server_key + email + pw + otp_key)
    qrcode_uri = auth.to_uri('totp', get_user_id(email) + server_label, OTP_ISSUER)

    img = qrcode.make(qrcode_uri)

    # img size limit 300
    if img.size[0] < 300:
        base_width = img.size[0]
    else:
        base_width = 300
    wpercent = (base_width/float(img.size[0]))
    hsize = int((float(img.size[1])*float(wpercent)))
    img = img.resize((base_width, hsize))

    stream = StringIO.StringIO()
    capture_path = QR_CODE_ROOT + get_parse_name(email) + ".png"
    img.save(capture_path)
    image = stream.getvalue()
    stream.close()

    return Response(image, mimetype='image/png')

# Func: str2bool
#   string to boolean
def str2bool(v):
    if v is None:
        return False
    return v.lower() in ("yes", "true", "1")