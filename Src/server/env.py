__author__ = 'ksm'
# -*- coding: utf-8 -*-

# local ----------------------------------------------------------------------------------------------------------------
#    build
DOMAIN_LABEL = "local"
QR_CODE_ROOT = "../client/app/images/test/qr_code/"
UPLOAD_ROOT = "../client/app/images/test/upload/"

# test server ----------------------------------------------------------------------------------------------------------
#    build
#DOMAIN_LABEL = "test"
#QR_CODE_ROOT = "../qr_code/"
#UPLOAD_ROOT = '../upload/'

# local & test server --------------------------------------------------------------------------------------------------
#    build
DEBUG_ACT = True

