__author__ = 'ksm'
# -*- coding: utf-8 -*-

from flask.ext.sqlalchemy import SQLAlchemy
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

db = SQLAlchemy()

#-----------------------------------------------------------------------------------------------------------------------
# Informations
#-----------------------------------------------------------------------------------------------------------------------
# Servers
#    Server Information
class Servers(db.Model):
    __tablename__ = "server_tb"
    pk = db.Column('pk', db.Integer, primary_key=True)
    domain = db.Column('domain', db.String(255))
    domain_lb = db.Column('domain_lb', db.String(255))
    sv_sk = db.Column('sv_sk', db.String(255))
    otp_lb = db.Column('otp_lb', db.String(40))
    df_dv_vol = db.Column('df_dv_vol', db.Integer)
    df_member_vol = db.Column('df_member_vol', db.Integer)
    df_warrant_vol = db.Column('df_warrant_vol', db.Integer)

# Users
#    User Information
class Users(db.Model):
    __tablename__ = "member_tb"
    pk = db.Column('pk', db.Integer, primary_key=True)
    mail = db.Column('mail', db.String(255), unique=True)
    mail_auth = db.Column('mail_auth', db.Integer)
    pw = db.Column('pw', db.String(255))
    phone = db.Column('phone', db.String(30))
    tel = db.Column('tel', db.String(30))
    name = db.Column('name', db.String(255))
    department = db.Column('department', db.String(255))
    position = db.Column('position', db.String(255))
    time_zone_id = db.Column('time_zone_id', db.Integer)
    date_fmt_id = db.Column('date_fmt_id', db.Integer)
    member_type_id = db.Column('member_type_id', db.Integer)
    member_auth_id = db.Column('member_auth_id', db.Integer)
    refresh_interval = db.Column('refresh_interval', db.Integer)
    img_refresh_interval = db.Column('img_refresh_interval', db.Integer)
    flag_active = db.Column('flag_active', db.Boolean)
    flag_refresh = db.Column('flag_refresh', db.Boolean)
    flag_img_refresh = db.Column('flag_img_refresh', db.Boolean)
    flag_recept_mail = db.Column('flag_recept_mail', db.Boolean)
    flag_use_otp = db.Column('flag_use_otp', db.Boolean)
    flag_create_qrcode = db.Column('flag_create_qrcode', db.Boolean)
    flag_duplicate_prevent = db.Column('flag_duplicate_prevent', db.Boolean)
    otp_sk = db.Column('otp_sk', db.String(255))
    dev_vol = db.Column('dev_vol', db.Integer)
    create_member_vol = db.Column('create_member_vol', db.Integer)
    create_time = db.Column('create_time', db.DateTime)
    warrant_start_time = db.Column('warrant_start_time', db.DateTime)
    warrant_expire_time = db.Column('warrant_expire_time', db.DateTime)
    login_time = db.Column('login_time', db.DateTime)
    logout_time = db.Column('logout_time', db.DateTime)
    member_fk = db.Column('member_fk', db.Integer)

# Tokens
#    Token Information
class Tokens(db.Model):
    __tablename__ = "token_tb"
    pk = db.Column('pk', db.Integer, primary_key=True)
    reg_mail = db.Column('reg_mail', db.String(255))
    token = db.Column('token', db.String(255))
    member_fk = db.Column('member_fk', db.Integer)

# Corps
#    Corperation Information
class Corps(db.Model):
    __tablename__ = "corp_tb"
    pk = db.Column('pk', db.Integer, primary_key=True)
    name = db.Column('name', db.String(255))
    ceo_name = db.Column('ceo_name', db.String(255))
    corp_reg_no = db.Column('corp_reg_no', db.String(255))
    tel = db.Column('tel', db.String(30))
    fax = db.Column('fax', db.String(30))
    hp_uri = db.Column('hp_uri', db.String(255))
    member_fk = db.Column('member_fk', db.Integer)

#-----------------------------------------------------------------------------------------------------------------------
# Contents
#-----------------------------------------------------------------------------------------------------------------------
# Devices
#    Device Information
class Devices(db.Model):
    __tablename__ = "device_tb"
    pk = db.Column('pk', db.Integer, primary_key=True)
    guid = db.Column('guid', db.String(255))
    agent_token = db.Column('agent_token', db.String(255))
    alias = db.Column('alias', db.String(255))
    dev_type_id = db.Column('dev_type_id', db.Integer)
    dev_os_id = db.Column('dev_os_id', db.Integer)
    dev_ver = db.Column('dev_ver', db.String(255))
    memo = db.Column('memo', db.String(65535))
    member_fk = db.Column('member_fk', db.Integer)
    device_group_fk = db.Column('device_group_fk', db.Integer)

# Device Windows
#    Device Windows Information
class DevWindows(db.Model):
    __tablename__ = "device_windows_tb"
    pk = db.Column('pk', db.Integer, primary_key=True)
    cpu_usage = db.Column('cpu_usage', db.Integer)
    hdd_usage = db.Column('hdd_usage', db.Integer)
    memory_usage = db.Column('memory_usage', db.Integer)
    cpu_temp = db.Column('cpu_temp', db.Integer)
    hdd_temp = db.Column('hdd_temp', db.Integer)
    fan_speed_id = db.Column('fan_speed_id', db.Integer)
    network_type_id = db.Column('network_type_id', db.Integer)
    response_time = db.Column('response_time', db.Integer)
    running_time = db.Column('running_time', db.String(100))
    system_time = db.Column('system_time', db.String(100))
    time_zone_id = db.Column('time_zone_id', db.Integer)
    prcss_name = db.Column('prcss_name', db.String(255))
    prcss_cpu_usage = db.Column('prcss_cpu_usage', db.String(255))
    prcss_memory_usage = db.Column('prcss_memory_usage', db.String(255))
    software_name = db.Column('software_name', db.String(65535))
    software_list = db.Column('software_list', db.String(65535))
    hardware_name = db.Column('hardware_name', db.String(255))
    hardware_txt = db.Column('hardware_txt', db.String(255))
    processor = db.Column('processor', db.String(255))
    bios = db.Column('bios', db.String(255))
    memory_capacity = db.Column('memory_capacity', db.String(255))
    hdd_capacity = db.Column('hdd_capacity', db.String(255))
    mac_addr = db.Column('mac_addr', db.String(100))
    ip_addr = db.Column('ip_addr', db.String(100))
    netmask = db.Column('netmask', db.String(100))
    gateway = db.Column('gateway', db.String(100))
    dns = db.Column('dns', db.String(100))
    manufacture = db.Column('manufacture', db.String(255))
    model = db.Column('model', db.String(255))
    serial_no = db.Column('serial_no', db.String(255))
    device_fk = db.Column('device_fk', db.Integer)

# Device Linux
#    Device Linux Information
class DevLinux(db.Model):
    __tablename__ = "device_linux_tb"
    pk = db.Column('pk', db.Integer, primary_key=True)
    cpu_usage = db.Column('cpu_usage', db.Integer)
    hdd_usage = db.Column('hdd_usage', db.Integer)
    memory_usage = db.Column('memory_usage', db.Integer)
    cpu_temp = db.Column('cpu_temp', db.Integer)
    hdd_temp = db.Column('hdd_temp', db.Integer)
    fan_speed_id = db.Column('fan_speed_id', db.Integer)
    network_type_id = db.Column('network_type_id', db.Integer)
    response_time = db.Column('response_time', db.Integer)
    running_time = db.Column('running_time', db.String(100))
    system_time = db.Column('system_time', db.String(100))
    time_zone_id = db.Column('time_zone_id', db.Integer)
    prcss_name = db.Column('prcss_name', db.String(255))
    prcss_cpu_usage = db.Column('prcss_cpu_usage', db.String(255))
    prcss_memory_usage = db.Column('prcss_memory_usage', db.String(255))
    software_name = db.Column('software_name', db.String(65535))
    software_list = db.Column('software_list', db.String(65535))
    hardware_name = db.Column('hardware_name', db.String(255))
    hardware_txt = db.Column('hardware_txt', db.String(255))
    processor = db.Column('processor', db.String(255))
    bios = db.Column('bios', db.String(255))
    memory_capacity = db.Column('memory_capacity', db.String(255))
    hdd_capacity = db.Column('hdd_capacity', db.String(255))
    mac_addr = db.Column('mac_addr', db.String(100))
    ip_addr = db.Column('ip_addr', db.String(100))
    netmask = db.Column('netmask', db.String(100))
    gateway = db.Column('gateway', db.String(100))
    dns = db.Column('dns', db.String(100))
    manufacture = db.Column('manufacture', db.String(255))
    model = db.Column('model', db.String(255))
    serial_no = db.Column('serial_no', db.String(255))
    device_fk = db.Column('device_fk', db.Integer)

# Device OS X
#    Device OS X Information
class DevOSX(db.Model):
    __tablename__ = "device_os_x_tb"
    pk = db.Column('pk', db.Integer, primary_key=True)
    cpu_usage = db.Column('cpu_usage', db.Integer)
    hdd_usage = db.Column('hdd_usage', db.Integer)
    memory_usage = db.Column('memory_usage', db.Integer)
    cpu_temp = db.Column('cpu_temp', db.Integer)
    hdd_temp = db.Column('hdd_temp', db.Integer)
    fan_speed_id = db.Column('fan_speed_id', db.Integer)
    network_type_id = db.Column('network_type_id', db.Integer)
    response_time = db.Column('response_time', db.Integer)
    running_time = db.Column('running_time', db.String(100))
    system_time = db.Column('system_time', db.String(100))
    time_zone_id = db.Column('time_zone_id', db.Integer)
    prcss_name = db.Column('prcss_name', db.String(255))
    prcss_cpu_usage = db.Column('prcss_cpu_usage', db.String(255))
    prcss_memory_usage = db.Column('prcss_memory_usage', db.String(255))
    software_name = db.Column('software_name', db.String(65535))
    software_list = db.Column('software_list', db.String(65535))
    hardware_name = db.Column('hardware_name', db.String(255))
    hardware_txt = db.Column('hardware_txt', db.String(255))
    processor = db.Column('processor', db.String(255))
    bios = db.Column('bios', db.String(255))
    memory_capacity = db.Column('memory_capacity', db.String(255))
    hdd_capacity = db.Column('hdd_capacity', db.String(255))
    mac_addr = db.Column('mac_addr', db.String(100))
    ip_addr = db.Column('ip_addr', db.String(100))
    netmask = db.Column('netmask', db.String(100))
    gateway = db.Column('gateway', db.String(100))
    dns = db.Column('dns', db.String(100))
    manufacture = db.Column('manufacture', db.String(255))
    model = db.Column('model', db.String(255))
    serial_no = db.Column('serial_no', db.String(255))
    device_fk = db.Column('device_fk', db.Integer)

# Device Android
#    Device Android Information
class DevAndroid(db.Model):
    __tablename__ = "device_android_tb"
    pk = db.Column('pk', db.Integer, primary_key=True)
    cpu_usage = db.Column('cpu_usage', db.Integer)
    hdd_usage = db.Column('hdd_usage', db.Integer)
    memory_usage = db.Column('memory_usage', db.Integer)
    cpu_temp = db.Column('cpu_temp', db.Integer)
    hdd_temp = db.Column('hdd_temp', db.Integer)
    fan_speed_id = db.Column('fan_speed_id', db.Integer)
    network_type_id = db.Column('network_type_id', db.Integer)
    response_time = db.Column('response_time', db.Integer)
    running_time = db.Column('running_time', db.String(100))
    system_time = db.Column('system_time', db.String(100))
    time_zone_id = db.Column('time_zone_id', db.Integer)
    prcss_name = db.Column('prcss_name', db.String(255))
    prcss_cpu_usage = db.Column('prcss_cpu_usage', db.String(255))
    prcss_memory_usage = db.Column('prcss_memory_usage', db.String(255))
    software_name = db.Column('software_name', db.String(65535))
    software_list = db.Column('software_list', db.String(65535))
    hardware_name = db.Column('hardware_name', db.String(255))
    hardware_txt = db.Column('hardware_txt', db.String(255))
    processor = db.Column('processor', db.String(255))
    bios = db.Column('bios', db.String(255))
    memory_capacity = db.Column('memory_capacity', db.String(255))
    hdd_capacity = db.Column('hdd_capacity', db.String(255))
    mac_addr = db.Column('mac_addr', db.String(100))
    ip_addr = db.Column('ip_addr', db.String(100))
    netmask = db.Column('netmask', db.String(100))
    gateway = db.Column('gateway', db.String(100))
    dns = db.Column('dns', db.String(100))
    manufacture = db.Column('manufacture', db.String(255))
    model = db.Column('model', db.String(255))
    serial_no = db.Column('serial_no', db.String(255))
    device_fk = db.Column('device_fk', db.Integer)

# Device IOS
#    Device IOS Information
class DevIOS(db.Model):
    __tablename__ = "device_ios_tb"
    pk = db.Column('pk', db.Integer, primary_key=True)
    cpu_usage = db.Column('cpu_usage', db.Integer)
    hdd_usage = db.Column('hdd_usage', db.Integer)
    memory_usage = db.Column('memory_usage', db.Integer)
    cpu_temp = db.Column('cpu_temp', db.Integer)
    hdd_temp = db.Column('hdd_temp', db.Integer)
    fan_speed_id = db.Column('fan_speed_id', db.Integer)
    network_type_id = db.Column('network_type_id', db.Integer)
    response_time = db.Column('response_time', db.Integer)
    running_time = db.Column('running_time', db.String(100))
    system_time = db.Column('system_time', db.String(100))
    time_zone_id = db.Column('time_zone_id', db.Integer)
    prcss_name = db.Column('prcss_name', db.String(255))
    prcss_cpu_usage = db.Column('prcss_cpu_usage', db.String(255))
    prcss_memory_usage = db.Column('prcss_memory_usage', db.String(255))
    software_name = db.Column('software_name', db.String(65535))
    software_list = db.Column('software_list', db.String(65535))
    hardware_name = db.Column('hardware_name', db.String(255))
    hardware_txt = db.Column('hardware_txt', db.String(255))
    processor = db.Column('processor', db.String(255))
    bios = db.Column('bios', db.String(255))
    memory_capacity = db.Column('memory_capacity', db.String(255))
    hdd_capacity = db.Column('hdd_capacity', db.String(255))
    mac_addr = db.Column('mac_addr', db.String(100))
    ip_addr = db.Column('ip_addr', db.String(100))
    netmask = db.Column('netmask', db.String(100))
    gateway = db.Column('gateway', db.String(100))
    dns = db.Column('dns', db.String(100))
    manufacture = db.Column('manufacture', db.String(255))
    model = db.Column('model', db.String(255))
    serial_no = db.Column('serial_no', db.String(255))
    device_fk = db.Column('device_fk', db.Integer)

# Device Groups
#    Device Group Information
class DeviceGroups(db.Model):
    __tablename__ = "device_group_tb"
    pk = db.Column('pk', db.Integer, primary_key=True)
    name = db.Column('name', db.String(40))
    create_time = db.Column('create_time', db.DateTime)
    update_time = db.Column('update_time', db.DateTime)
    flag_df_group = db.Column('flag_df_group', db.Boolean)
    flag_confirm = db.Column('flag_confirm', db.Boolean)
    member_fk = db.Column('member_fk', db.Integer)

# Device Groups
#    Device Group Information
class ConnMemberNDeviceGroup(db.Model):
    __tablename__ = "conn_member_n_device_group_tb"
    pk = db.Column('pk', db.Integer, primary_key=True)
    member_fk = db.Column('member_fk', db.Integer, db.ForeignKey(Users.pk))
    device_group_fk = db.Column('device_group_fk', db.Integer, db.ForeignKey(DeviceGroups.pk))
    members = db.relationship('Users')
    deviceGroups = db.relationship('DeviceGroups')

#-----------------------------------------------------------------------------------------------------------------------
# Logs
#-----------------------------------------------------------------------------------------------------------------------
# AccessHistory
#    Access log Information
class AccessHistory(db.Model):
    __tablename__ = "hs_access_tb"
    pk = db.Column('pk', db.Integer, primary_key=True)
    reg_mail = db.Column('reg_mail', db.String(255))
    type_id = db.Column('type_id', db.Integer)
    ip_addr = db.Column('ip_addr', db.String(255))
    os = db.Column('os', db.String(255))
    browser = db.Column('browser', db.String(255))
    member_fk = db.Column('member_fk', db.Integer)

# RemoteHistory
#    Remote log Information
class RemoteHistory(db.Model):
    __tablename__ = "hs_remote_tb"
    pk = db.Column('pk', db.Integer, primary_key=True)
    reg_mail = db.Column('reg_mail', db.String(255))
    name = db.Column('name', db.String(255))
    guid = db.Column('guid', db.String(255))
    alias = db.Column('alias', db.String(255))
    dev_os_id = db.Column('dev_os_id', db.Integer)
    host_public_ip = db.Column('host_public_ip', db.String(255))
    host_private_ip = db.Column('host_private_ip', db.String(255))
    viewer_public_ip = db.Column('viewer_public_ip', db.String(255))
    viewer_private_ip = db.Column('viewer_private_ip', db.String(255))
    start_time = db.Column('start_time', db.DateTime)
    end_time = db.Column('end_time', db.DateTime)
    member_fk = db.Column('member_fk', db.Integer, db.ForeignKey(Users.pk))
    members = db.relationship('Users')
    device_fk = db.Column('device_fk', db.Integer, db.ForeignKey(Devices.pk))
    devices = db.relationship('Devices')

# AlertHistory
#    Alert log Information
class AlertHistory(db.Model):
    __tablename__ = "hs_alert_tb"
    pk = db.Column('pk', db.Integer, primary_key=True)
    category_id = db.Column('category_id', db.Integer)
    th_item_id = db.Column('th_item_id', db.Integer)
    th_lv1 = db.Column('th_lv1', db.Integer)
    th_lv2 = db.Column('th_lv2', db.Integer)
    device_group_fk = db.Column('device_group_fk', db.Integer)
    start_time = db.Column('start_time', db.DateTime)
    end_time = db.Column('end_time', db.DateTime)

# AuditHistory
#    Audit log Information
class AuditHistory(db.Model):
    __tablename__ = "hs_audit_tb"
    pk = db.Column('pk', db.Integer, primary_key=True)
    reg_mail = db.Column('reg_mail', db.String(255))
    act_time = db.Column('act_time', db.DateTime)
    act_name_id = db.Column('act_name_id', db.Integer)
    act_type_id = db.Column('act_type_id', db.Integer)
    old_value = db.Column('old_value', db.String(255))
    new_value = db.Column('new_value', db.String(255))
    device_fk = db.Column('device_fk', db.Integer)