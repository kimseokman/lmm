__author__ = 'ksm'
# -*- coding: utf-8 -*-

"""
LMM - Link Me Mine API Server
"""

#-----------------------------------------------------------------------------------------------------------------------
# Lib
#-----------------------------------------------------------------------------------------------------------------------
from flask import Flask, request, jsonify, Response
from flask.ext.cors import CORS
from flask.ext.restful import reqparse, Resource, Api
from werkzeug import secure_filename
from sqlalchemy import or_, and_
from datetime import datetime, timedelta
from pytz import timezone
import random
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

import json
# Lib - scaffold
from models import *
from utils import *
from env import *

app = Flask(__name__)

# config
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:1234@127.0.0.1/lmm'
app.config['UPLOAD_FOLDER'] = UPLOAD_ROOT
ALLOWED_EXTENSIONS = set(['png'])
CORS(app, resources=r'/lmm/api/*', headers={'Content-Type', 'token', 'If-Modified-Since', 'Cache-Control', 'Pragma'})

db.init_app(app)

#-----------------------------------------------------------------------------------------------------------------------
# [ WEB ]
#-----------------------------------------------------------------------------------------------------------------------
# Root
#   Check Alive Server
class RootAPI(Resource):
    def __init__(self):
        self.API_NAME = "RootAPI"
        self.API = "/lmm/api/v1.0/"
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        #---------------------------------------------------------------------------------------------------------------
        super(RootAPI, self).__init__()

    def get(self):
        if DEBUG_ACT:
            self.debug["notice"] = "A Live API Server"
            self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")
        return result(self.API_NAME, None, None, 200)

# File Upload
#   Logo Image
class FileUploadAPI(Resource):
    def __init__(self):
        self.API_NAME = "RootAPI"
        self.API = "/lmm/api/v1.0/file_upload"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        #---------------------------------------------------------------------------------------------------------------
        super(FileUploadAPI, self).__init__()

    def allowed_file(self, filename):
        return '.' in filename and \
               filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

    def post(self):
        file = request.files['file']
        if file and self.allowed_file(file.filename):
            filename = secure_filename(file.filename)
            custom_file_name = get_parse_name(self.user.mail) + "." + "png"
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], custom_file_name))
            if DEBUG_ACT:
                self.debug["notice"] = "File Upload Success"
                self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")
            return result(self.API_NAME, None, None, 200)
        if DEBUG_ACT:
            self.debug["notice"] = "File Upload Fail"
            self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")
        return result(self.API_NAME, None, None, 500)

# Access
#    Check User Login
class AccessAPI(Resource):
    def __init__(self):
        self.API_NAME = "AccessAPI"
        self.API = "/lmm/api/v1.0/access"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        # Filter -------------------------------------------------------------------------------------------------------
        self.filter_auth = []
        # Param --------------------------------------------------------------------------------------------------------
        self.parser.add_argument("type", type=str, location="json")
        self.parser.add_argument("mail", type=str, location="json")
        self.parser.add_argument("pw", type=str, location="json")
        self.parser.add_argument("code", type=str, location="json")
        #---------------------------------------------------------------------------------------------------------------
        super(AccessAPI, self).__init__()

    def post(self):
        input = self.parser.parse_args()

        is_login = "Fail"
        step1 = "Fail"
        step2 = "Fail"
        step3 = "Fail"

        # [ case by ] Login 1 Step
        if input["type"] == '1STEP':
            user = db.session.query(Users).filter_by(mail=input["mail"], pw=pw_encode(input["pw"])).first()

            if user is None:
                # [ Case by ] Login 1 Step : Fail
                object = {
                    "isLogin": is_login,
                    "step1": step1
                }
                if DEBUG_ACT:
                    self.debug["notice"] = "Access 1STEP Fail"
                    self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(object)
                return result(self.API_NAME, object, None, 200)
            else:
                step1 = "Success"

                # [ Case by ] Login 1 Step : Success & 2 Step Fail - E-mail confirmation required
                if user.mail_auth == 0:
                    object = {
                        "isLogin": is_login,
                        "mail": user.mail,
                        "step1": step1,
                        "step2": step2
                    }
                    if DEBUG_ACT:
                        self.debug["notice"] = "Access 2STEP Fail"
                        self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(object)
                    return result(self.API_NAME, object, None, 200)
                # [ Case by ] Login 1 Step : Success - E-mail confirmation OK
                elif user.mail_auth != 0:
                    step2 = "Success"
                    use_otp = "None"

                    # [ case by ] Login : Success - OTP not use
                    if user.flag_use_otp is False:
                        use_otp = "None"
                        is_login = "Success"
                        var_update = {}
                        var_update["login_time"] = get_current_time("UTC")
                        db.session.query(Users).filter_by(pk=user.pk).update(var_update)
                        db.session.commit()
                    else:
                        use_otp = "Use"

                    # duplicate login prevent
                    if user.flag_duplicate_prevent:
                        # clear token
                        self.token_manager.clear_token(user.pk)

                    # set new token
                    new_token = Tokens()
                    new_token.reg_mail = user.mail
                    new_token.token = self.token_manager.set_token(user.mail)
                    new_token.member_fk = user.pk

                    db.session.add(new_token)
                    db.session.commit()

                    # [ Case by ] Login 3 Step : Always Ready
                    if user.flag_use_otp and user.flag_create_qrcode is False:
                        server = db.session.query(Servers).filter_by(domain_lb=DOMAIN_LABEL).first()
                        set_otp(user.mail, user.pw, user.otp_sk, server.sv_sk, server.otp_lb)

                    object = {
                        "isLogin": is_login,
                        "token": new_token.token,
                        "step1": step1,
                        "step2": step2,
                        "useOTP": use_otp,
                        "step3": step3
                    }
                    if DEBUG_ACT:
                        self.debug["notice"] = "Access 2STEP Success"
                        self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(object)
                    return result(self.API_NAME, object, None, 200)

        # [ case by ] 3 Step
        elif input["type"] == '3STEP':
            server = db.session.query(Servers).filter_by(domain_lb=DOMAIN_LABEL).first()
            user = db.session.query(Users).filter_by(pk=self.user.pk).first()

            auth = OtpAuth(server.sv_sk + user.mail + user.pw + user.otp_sk)
            if input["code"] and auth.valid_totp(input["code"]):
                is_login = "Success"
                step3 = "Success"

            if step3 == "Success":
                updateList = {}
                updateList["flag_create_qrcode"] = True
                updateList["login_time"] = get_current_time("UTC")

                db.session.query(Users).filter_by(pk=self.user.pk).update(updateList)
                db.session.commit()

            object = {
                "isLogin": is_login,
                "step3": step3
            }
            if DEBUG_ACT:
                self.debug["notice"] = "Access 3STEP"
                self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(object)
            return result(self.API_NAME, object, None, 200)

# Auth
#    User mail Auth & Auth mail Resend
class AuthAPI(Resource):
    def __init__(self):
        self.API_NAME = "AuthAPI"
        self.API = "/lmm/api/v1.0/auth"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.mail_manager = SmtpManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Param --------------------------------------------------------------------------------------------------------
        self.parser.add_argument("key", type=str, location="json")
        self.parser.add_argument("type", type=str, location="json")
        self.parser.add_argument("mail", type=str, location="json")
        #---------------------------------------------------------------------------------------------------------------
        super(AuthAPI, self).__init__()

    def post(self):
        input = self.parser.parse_args()

        # [ case by ] auth mail resend
        if input["type"] == 'resend':
            user = db.session.query(Users).filter_by(mail=input['mail']).first()

            if user is None:
                if DEBUG_ACT:
                    self.debug["notice"] = "Auth Mail Send Fail"
                    self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")
            else:
                server = db.session.query(Servers).filter_by(domain_lb=DOMAIN_LABEL).first()
                old_token = db.session.query(Tokens).filter_by(member_fk=user.pk).first()
                html = "<html>" \
                  "<head>" \
                  "<title>Mail Content</title>" \
                  "</head>" \
                  "<body>" \
                  "<div id='wrap'>" \
                  "<h3>Link Me Mine 재인증 메일</h3>" \
                  "<p>로그인을 위한 인증이 필요 합니다. 아래의 링크를 클릭하여주시기 바랍니다.</p>" \
                  "<a target='_blank' href='"+str(server.domain)+"access/auth?key="+str(old_token.token)+"'>인증</a>" \
                  "</div>" \
                  "</body>" \
                  "</html>"

                self.mail_manager.send_mail(input['mail'], "[인증] 귀하께서 요청하신 재인증 관련 메일입니다.", html)

                if DEBUG_ACT:
                    self.debug["notice"] = "Auth Email Send"
                    self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(html)

                return result(self.API_NAME, None, None, 200)
        # [ case by ] auth process
        elif input["type"] == 'auth':
            token_info = db.session.query(Tokens).filter_by(token=input["key"]).first()
            if token_info is None:
                # [ case by ] auth Fail
                if DEBUG_ACT:
                    self.debug["notice"] = "Mail Auth Fail"
                    self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")
                object = {"result": "Fail"}
                return result(self.API_NAME, object, None, 200)
            else:
                user = db.session.query(Users).filter_by(pk=token_info.member_fk).first()

                # [ case by ] auth Abled : auth Success
                if user.mail_auth == 0:
                    updateList = {}
                    updateList["mail_auth"] = 1

                    db.session.query(Users).filter_by(pk=token_info.member_fk).update(updateList)
                    db.session.commit()

                    if DEBUG_ACT:
                        self.debug["notice"] = "Mail Auth Success"
                        self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")

                    object = {"result": "Success"}
                    return result(self.API_NAME, object, None, 200)
                # [ case by ] Already auth : auth Fail
                elif user.mail_auth == 1:
                    if DEBUG_ACT:
                        self.debug["notice"] = "Already Mail Auth"
                        self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")
                    object = {"result": "Already"}
                    return result(self.API_NAME, object, None, 200)
                # [ case by ] auth Disabled : auth Fail
                elif user.mail_auth == 2:
                    if DEBUG_ACT:
                        self.debug["notice"] = "Mail Auth Fail"
                        self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")
                    object = {"result": "Fail"}
                    return result(self.API_NAME, object, None, 200)

# AutoComplete
#    auto complete filter - return only objects
class AutoCompleteAPI(Resource):
    def __init__(self):
        self.API_NAME = "/lmm/api/v1.0/auto_complete"
        self.API = "/lmm/api/v1.0/resetPW"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Param --------------------------------------------------------------------------------------------------------
        #---------------------------------------------------------------------------------------------------------------
        super(AutoCompleteAPI, self).__init__()

    def get(self):
        param = {}
        param["type"] = request.args.get('type')
        param["field"] = request.args.get('field')
        param["text"] = request.args.get('text')

        like_filter = '%' + param["text"] + '%'

        if param["type"] == "devices":
            device_query = ''
            device_filter = ''

            if param["field"] == "alias":
                device_query = Devices.alias.distinct().label("alias")
                device_filter = Devices.alias.like(like_filter)

            device_results = db.session.query(device_query).filter(device_filter).all()

            objects = []
            for device in device_results:
                objects.append({
                    "label": param["field"],
                    "id": device.alias,
                    "text": device.alias
                })
            return objects

# ResetPW
#    Init Password
class ResetPWAPI(Resource):
    def __init__(self):
        self.API_NAME = "ResetPWAPI"
        self.API = "/lmm/api/v1.0/resetPW"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.mail_manager = SmtpManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        # Filter -------------------------------------------------------------------------------------------------------
        self.filter_auth = []
        # Param --------------------------------------------------------------------------------------------------------
        self.parser.add_argument("mail", type=str, location="json")
        #---------------------------------------------------------------------------------------------------------------
        super(ResetPWAPI, self).__init__()

    def post(self):
        input = self.parser.parse_args()
        check_mail = db.session.query(Users).filter_by(mail=input['mail']).first()
        if check_mail is None:
            if DEBUG_ACT:
                self.debug["notice"] = "Password Reset Fail"
                self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")
        else:
            resetPW = "testkoino"
            updateList = {}
            updateList['pw'] = pw_encode(resetPW)
            db.session.query(Users).filter_by(mail=input['mail']).update(updateList)
            db.session.commit()

            html = "<html>" \
              "<head>" \
              "<title>Mail Content</title>" \
              "</head>" \
              "<body>" \
              "<div id='wrap'>" \
              "<h3>Password 초기화 관련 안내 글</h3>" \
              "<p>아래와 같은 문자로 Password가 초기화 되었음을 알려드립니다.</p>" \
              "<p> Password : "+resetPW+"</p>" \
              "</div>" \
              "</body>" \
              "</html>"

            self.mail_manager.send_mail(input['mail'], "요청하신 Password Reset 관련 안내글입니다.", html)

            if DEBUG_ACT:
                self.debug["notice"] = "Password Reset Success"
                self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")

# Check
#    Check Information
class CheckAPI(Resource):
    def __init__(self):
        self.API_NAME = "CheckAPI"
        self.API = "/lmm/api/v1.0/check"
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        #---------------------------------------------------------------------------------------------------------------
        super(CheckAPI, self).__init__()

    def get(self):
        type = request.args.get('type')
        value = request.args.get('value')

        if type == 'mail':
            mail_check = db.session.query(Users).filter_by(mail=value).first()
            if mail_check is None:
                object = {'available': True}
            else:
                object = {'available': False}
            return result(self.API_NAME, object, None, 200)

#-----------------------------------------------------------------------------------------------------------------------
# [ WEB ] REST
#-----------------------------------------------------------------------------------------------------------------------
# DeviceGroupList
#    @GET : Device Group List
#    @POST : Device Group 등록
class DeviceGroupListAPI(Resource):
    def __init__(self):
        self.API_NAME = "DeviceGroupListAPI"
        self.API = "/lmm/api/v1.0/device-groups"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        # Filter -------------------------------------------------------------------------------------------------------
        self.filter_auth = []
        # Param --------------------------------------------------------------------------------------------------------
        self.parser.add_argument("name", type=str, location="json")
        self.parser.add_argument("flag_df_group", type=bool, location="json")
        self.parser.add_argument("flag_confirm", type=bool, location="json")
        self.parser.add_argument("member_fk", type=int, location="json")
        #---------------------------------------------------------------------------------------------------------------
        super(DeviceGroupListAPI, self).__init__()

    def get(self):
        #print self.user_type
        if self.validateToken:
            #param
            param = {}
            param["search_mode"] = request.args.get('search_mode')
            param["member_pk"] = request.args.get('member_pk')
            param["manager_pk"] = request.args.get('manager_pk')

            order_by = DeviceGroups.create_time
            if param["search_mode"] == 'pagination':
                #pagination setting
                limit = int(request.args.get('limit'))
                offset = int(request.args.get('offset'))
                order = request.args.get('order')
                by = request.args.get('by')
                order_by = by + " " + order.upper()

            filter_member_device_group = []

            # [ make ] query
            if self.user_type == TYPE_MANAGER:
                if param["member_pk"]:
                    device_group_list = db.session.query(DeviceGroups).filter_by(member_fk=self.user.pk).order_by(DeviceGroups.create_time.desc()).order_by(order_by)
                elif param["manager_pk"]:
                    device_group_list = db.session.query(DeviceGroups).filter_by(member_fk=param["manager_pk"]).order_by(DeviceGroups.create_time.desc()).order_by(order_by)
                else:
                    device_group_list = db.session.query(DeviceGroups).filter_by(member_fk=self.user.pk).order_by(DeviceGroups.create_time.desc()).order_by(order_by)
            elif self.user_type == TYPE_MEMBER:
                filter_member_device_group.append(ConnMemberNDeviceGroup.member_fk == self.user.pk)
                device_group_list = db.session.query(DeviceGroups).join(ConnMemberNDeviceGroup, DeviceGroups.pk == ConnMemberNDeviceGroup.device_group_fk).filter(or_(*filter_member_device_group)).order_by(DeviceGroups.create_time.desc()).order_by(order_by)

            total_count = device_group_list.count()
            if param["search_mode"] == 'pagination':
                device_group_list = device_group_list.limit(limit).offset(offset)
            current_count = device_group_list.count()

            if param["search_mode"] == 'pagination':
                # [ create ] Pagination
                previous = None
                next = None
                offset_list = {}
                offset_list["previous"] = offset - limit
                offset_list["next"] = offset + limit

                if offset != 0:
                    previous = self.API + "?limit=" + str(limit) + "&offset=" + str(offset_list["previous"])
                    if (total_count - (offset+current_count)) > limit:
                        next = self.API + "?limit=" + str(limit) + "&offset=" + str(offset_list["next"])
                    elif (total_count - (offset+current_count)) <= limit:
                        next = None

            meta = {}
            objects = []
            if current_count == 0:
                meta["total_count"] = 0
                if param["search_mode"] == 'pagination':
                    meta["limit"] = limit
                    meta["offset"] = offset
                    meta["next"] = 0
                    meta["previous"] = 0

                objects.append({
                    "pk": "EMPTY",
                })

                if DEBUG_ACT:
                    self.debug["notice"] = "Device Group List EMPTY"
                    self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(objects)
                return result(self.API_NAME, objects, meta, 200)
            else:
                meta["total_count"] = total_count
                if param["search_mode"] == 'pagination':
                    meta["limit"] = limit
                    meta["offset"] = offset
                    meta["next"] = next
                    meta["previous"] = previous

                for device_group in device_group_list:
                    objects.append({
                        "pk": device_group.pk,
                        "name": device_group.name,
                        "create_time": time_encode(device_group.create_time),
                        "update_time": time_encode(device_group.update_time),
                        "flag_df_group": device_group.flag_df_group,
                        "flag_confirm": device_group.flag_confirm,
                        "member_fk": device_group.member_fk
                    })

                if DEBUG_ACT:
                    self.debug["notice"] = "GET the Device Group List Success"
                    self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(objects)
                return result(self.API_NAME, objects, meta, 200)

    def post(self):
        input = self.parser.parse_args()

        new_dg = DeviceGroups()

        new_dg.name = input["name"]
        new_dg.create_time = get_current_time("UTC")
        new_dg.update_time = get_current_time("UTC")
        new_dg.flag_df_group = input["flag_df_group"]
        new_dg.flag_confirm = input["flag_confirm"]
        new_dg.member_fk = input["member_fk"]

        db.session.add(new_dg)
        db.session.commit()

        object = {
            "pk": new_dg.pk,
            "name": new_dg.name,
            "create_time": time_encode(new_dg.create_time),
            "update_time": time_encode(new_dg.update_time),
            "flag_df_group": new_dg.flag_df_group,
            "flag_confirm": new_dg.flag_confirm,
            "member_fk": new_dg.member_fk
        }

        if DEBUG_ACT:
            self.debug["notice"] = "Create NEW Device Group Success"
            self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(object)

        return result(self.API_NAME, object, None, 200)

# DeviceGroup
#    @GET : Device Group 정보
#    @PUT : Device Group 정보 수정
#    @DEL : Device Group 정보 삭제
class DeviceGroupAPI(Resource):
    def __init__(self):
        self.API_NAME = "DeviceGroupAPI"
        self.API = "/lmm/api/v1.0/device-groups"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        # Filter -------------------------------------------------------------------------------------------------------
        self.filter_auth = []
        # Param --------------------------------------------------------------------------------------------------------
        self.parser.add_argument("flag_confirm", type=bool, location="json")
        #---------------------------------------------------------------------------------------------------------------
        super(DeviceGroupAPI, self).__init__()

    def get(self, pk):
        # [ case by ] base device search
        if pk == TYPE_SELF_PK:
            default_device_group = db.session.query(DeviceGroups).filter_by(member_fk=self.user.pk, flag_df_group=True).first()

            object = {
                "pk": default_device_group.pk,
                "name": default_device_group.name,
                "create_time": time_encode(default_device_group.create_time),
                "update_time": time_encode(default_device_group.update_time),
                "flag_df_group": default_device_group.flag_df_group,
                "flag_confirm": default_device_group.flag_confirm,
                "member_fk": default_device_group.member_fk
            }

            if DEBUG_ACT:
                self.debug["notice"] = "GET the Default Device Group Success"
                self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(object)

            return result(self.API_NAME, object, None, 200)
        else:
            device_group = db.session.query(DeviceGroups).filter_by(pk=pk).first()

            object = {
                "pk": device_group.pk,
                "name": device_group.name,
                "create_time": time_encode(device_group.create_time),
                "update_time": time_encode(device_group.update_time),
                "flag_df_group": device_group.flag_df_group,
                "flag_confirm": device_group.flag_confirm,
                "member_fk": device_group.member_fk
            }

            if DEBUG_ACT:
                self.debug["notice"] = "GET the Device Group Success"
                self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(object)

            return result(self.API_NAME, object, None, 200)

    def put(self, pk):
        input = self.parser.parse_args()

        # [ case ] group new label update
        dg_updateList = {}
        if input["flag_confirm"]:
            dg_updateList["flag_confirm"] = input["flag_confirm"]

        db.session.query(DeviceGroups).filter_by(pk=pk).update(dg_updateList)
        db.session.commit()

        if DEBUG_ACT:
            self.debug["notice"] = "Device Group UPDATE Success"
            self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(dg_updateList)

        return result(self.API_NAME, None, None, 200)

class DeviceGroupConnListAPI(Resource):
    def __init__(self):
        self.API_NAME = "DeviceGroupConnListAPI"
        self.API = "/lmm/api/v1.0/conn/device-groups"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        # Filter -------------------------------------------------------------------------------------------------------
        self.filter_auth = []
        # Param --------------------------------------------------------------------------------------------------------
        self.parser.add_argument("member_fk", type=int, location="json")
        self.parser.add_argument("device_group_fk", type=int, location="json")
        #---------------------------------------------------------------------------------------------------------------
        super(DeviceGroupConnListAPI, self).__init__()

    def get(self):
        #param
        param = {}
        param["member_fk"] = request.args.get('member_fk')
        param["device_group_fk"] = request.args.get('device_group_fk')

        filter_conn = []
        if param["member_fk"]:
            filter_conn.append(ConnMemberNDeviceGroup.member_fk == param["member_fk"])
        if param["device_group_fk"]:
            filter_conn.append(ConnMemberNDeviceGroup.device_group_fk == param["device_group_fk"])

        conn_list = db.session.query(ConnMemberNDeviceGroup).filter(and_(*filter_conn))

        objects = []
        if conn_list.count() != 0:
            for conn in conn_list:
                objects.append({
                    "pk": conn.pk,
                    "member_fk": conn.member_fk,
                    "device_group_fk": conn.device_group_fk
                })
            return result(self.API_NAME, objects, None, 200)
        else:
            if DEBUG_ACT:
                self.debug["notice"] = "Member and Device Group connect EMPTY"
                self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(objects)
            return result(self.API_NAME, objects, None, 404)

    def post(self):
        input = self.parser.parse_args()

        # validate param
        var_check_param = ["member_fk", "device_group_fk"]
        for i in range(len(var_check_param)):
            if not str(var_check_param[i]) in input:
                if DEBUG_ACT:
                    self.debug["notice"] = "Required Parameter"
                    self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")
                return result(self.API_NAME, None, None, 400)
        conn_member_n_device_group = ConnMemberNDeviceGroup()
        conn_member_n_device_group.member_fk = input["member_fk"]
        conn_member_n_device_group.device_group_fk = input["device_group_fk"]

        db.session.add(conn_member_n_device_group)
        db.session.commit()

        return result(self.API_NAME, None, None, 200)

class DeviceGroupConnAPI(Resource):
    def __init__(self):
        self.API_NAME = "DeviceGroupConnAPI"
        self.API = "/lmm/api/v1.0/conn/device-groups/"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        # Filter -------------------------------------------------------------------------------------------------------
        self.filter_auth = []
        # Param --------------------------------------------------------------------------------------------------------
        self.parser.add_argument("member_fk", type=int, location="json")
        #---------------------------------------------------------------------------------------------------------------
        super(DeviceGroupConnAPI, self).__init__()

    def delete(self, pk):
        conn_member_n_device_group = db.session.query(ConnMemberNDeviceGroup).filter_by(pk=pk).one()
        db.session.delete(conn_member_n_device_group)
        db.session.commit()
        return result(self.API_NAME, None, None, 200)

# DeviceList
#    @GET : Device List
#    @POST : Device 등록
class DeviceListAPI(Resource):
    def __init__(self):
        self.API_NAME = "DeviceListAPI"
        self.API = "/lmm/api/v1.0/devices"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        # Filter -------------------------------------------------------------------------------------------------------
        self.filter_auth = []
        # Param --------------------------------------------------------------------------------------------------------
        #---------------------------------------------------------------------------------------------------------------
        super(DeviceListAPI, self).__init__()

    def get(self):
        #param
        param = {}
        param["search_mode"] = request.args.get('search_mode')
        param["device_group_pk"] = request.args.get('device_group_pk')
        param["alias"] = request.args.get('alias')
        param["dev_type_id"] = request.args.get('dev_type_id')
        param["dev_os_id"] = request.args.get('dev_os_id')

        order_by = Devices.guid
        if param["search_mode"] == 'pagination':
            #pagination setting
            limit = int(request.args.get('limit'))
            offset = int(request.args.get('offset'))
            order = request.args.get('order')
            by = request.args.get('by')
            order_by = by + " " + order.upper()

        filter_and_group = []
        filter_or_group = []

        # [ make ] filter
        # [ case by ] group
        if param["device_group_pk"] == 'all':
            filter_and_group.append(Devices.member_fk == self.user.pk)
        else:
            filter_and_group.append(Devices.device_group_fk == param["device_group_pk"])
        # [ case by ] alias
        if param["alias"] is not None:
            filter_and_group.append(Devices.alias == param["alias"])
        # [ case by ] device type
        if param["dev_type_id"] is not None:
            filter_and_group.append(Devices.dev_type_id == param["dev_type_id"])
        # [ case by ] device os
        if param["dev_os_id"] is not None:
            filter_and_group.append(Devices.dev_os_id == param["dev_os_id"])

        # [ make ] query
        device_list = db.session.query(Devices).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).order_by(order_by)
        total_count = device_list.count()
        if param["search_mode"] == 'pagination':
            device_list = device_list.limit(limit).offset(offset)
        current_count = device_list.count()

        if param["search_mode"] == 'pagination':
            # [ create ] Pagination
            previous = None
            next = None
            offset_list = {}
            offset_list["previous"] = offset - limit
            offset_list["next"] = offset + limit

            if offset != 0:
                previous = self.API + "?limit=" + str(limit) + "&offset=" + str(offset_list["previous"])
                if (total_count - (offset+current_count)) > limit:
                    next = self.API + "?limit=" + str(limit) + "&offset=" + str(offset_list["next"])
                elif (total_count - (offset+current_count)) <= limit:
                    next = None

        meta = {}
        objects = []
        if current_count == 0:
            meta["total_count"] = 0
            if param["search_mode"] == 'pagination':
                meta["limit"] = limit
                meta["offset"] = offset
                meta["next"] = 0
                meta["previous"] = 0

            objects.append({
                "pk": "EMPTY",
            })

            if DEBUG_ACT:
                self.debug["notice"] = "Devices EMPTY"
                self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(objects)
            return result(self.API_NAME, objects, meta, 200)
        else:
            meta["total_count"] = total_count
            if param["search_mode"] == 'pagination':
                meta["limit"] = limit
                meta["offset"] = offset
                meta["next"] = next
                meta["previous"] = previous

            for device in device_list:
                objects.append({
                    "pk": device.pk,
                    "guid": device.guid,
                    "alias": device.alias,
                    "dev_type_id": device.dev_type_id,
                    "dev_os_id": device.dev_os_id,
                    "dev_ver": device.dev_ver,
                    "memo": device.memo,
                    "member_fk": device.member_fk,
                    "device_group_fk": device.device_group_fk
                })

            if DEBUG_ACT:
                self.debug["notice"] = "GET Device List Success"
                self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(objects)
            return result(self.API_NAME, objects, meta, 200)

# Device
#    @GET : Device 정보
#    @PUT : Device 정보 수정
#    @DEL : Device 정보 삭제
class DeviceAPI(Resource):
    def __init__(self):
        self.API_NAME = "DeviceAPI"
        self.API = "/lmm/api/v1.0/devices/"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        # Filter -------------------------------------------------------------------------------------------------------
        self.filter_auth = []
        # Param --------------------------------------------------------------------------------------------------------
        self.parser.add_argument("alias", type=str, location="json")
        self.parser.add_argument("memo", type=str, location="json")
        self.parser.add_argument("device_group_fk", type=int, location="json")
        #---------------------------------------------------------------------------------------------------------------
        super(DeviceAPI, self).__init__()

    def get(self, pk):
        if self.validateToken:
            # [ case by ] base device search
            if pk == TYPE_SELF_PK:
                base_device = db.session.query(Devices).filter_by(member_fk=self.user.pk).first()

                if base_device is None:
                    object = {
                        "pk": "EMPTY"
                    }
                    if DEBUG_ACT:
                        self.debug["notice"] = "Device EMPTY"
                else:
                    object = {
                        "pk": base_device.pk,
                        "guid": base_device.guid,
                        "alias": base_device.alias,
                        "dev_type_id": base_device.dev_type_id,
                        "dev_os_id": base_device.dev_os_id,
                        "dev_ver": base_device.dev_ver,
                        "memo": base_device.memo,
                        "member_fk": base_device.member_fk,
                        "device_group_fk": base_device.device_group_fk
                    }
                    if DEBUG_ACT:
                        self.debug["notice"] = "GET Device Success"

                if DEBUG_ACT:
                    self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(object)

                return result(self.API_NAME, object, None, 200)
            else:
                device = db.session.query(Devices).filter_by(pk=pk).first()

                if device is None:
                    object = {
                        "pk": "EMPTY"
                    }
                else:
                    var_this_device_os = self.process_manager.get_device_os(device.dev_os_id)
                    if var_this_device_os == TYPE_UNKNOWN or var_this_device_os == TYPE_WINDOWS:
                        dev_os = db.session.query(DevWindows).filter_by(device_fk=device.pk).first()
                    elif var_this_device_os == TYPE_LINUX:
                        dev_os = db.session.query(DevLinux).filter_by(device_fk=device.pk).first()
                    elif var_this_device_os == TYPE_OS_X:
                        dev_os = db.session.query(DevOSX).filter_by(device_fk=device.pk).first()
                    elif var_this_device_os == TYPE_ANDROID:
                        dev_os = db.session.query(DevAndroid).filter_by(device_fk=device.pk).first()
                    elif var_this_device_os == TYPE_IOS:
                        dev_os = db.session.query(DevIOS).filter_by(device_fk=device.pk).first()

                    if dev_os is None:
                        object = {
                            "pk": device.pk,
                            "guid": device.guid,
                            "alias": device.alias,
                            "dev_type_id": device.dev_type_id,
                            "dev_os_id": device.dev_os_id,
                            "dev_ver": device.dev_ver,
                            "memo": device.memo,
                            "member_fk": device.member_fk,
                            "device_group_fk": device.device_group_fk,
                            "os_pk": "EMPTY"
                        }
                    else:
                        object = {
                            "pk": device.pk,
                            "guid": device.guid,
                            "alias": device.alias,
                            "dev_type_id": device.dev_type_id,
                            "dev_os_id": device.dev_os_id,
                            "dev_ver": device.dev_ver,
                            "device_group_fk": device.device_group_fk,
                            "os_pk": dev_os.pk,
                            "cpu_usage": dev_os.cpu_usage,
                            "hdd_usage": dev_os.hdd_usage,
                            "cpu_temp": dev_os.cpu_temp,
                            "hdd_temp": dev_os.hdd_temp,
                            "fan_speed_id": dev_os.fan_speed_id,
                            "network_type_id": dev_os.network_type_id,
                            "response_time": dev_os.response_time,
                            "running_time": dev_os.running_time,
                            "system_time": dev_os.system_time,
                            "time_zone_id": dev_os.time_zone_id,
                            "prcss_name": dev_os.prcss_name,
                            "prcss_cpu_usage": dev_os.prcss_cpu_usage,
                            "prcss_memory_usage": dev_os.prcss_memory_usage,
                            "software_name": dev_os.software_name,
                            "software_list": dev_os.software_list,
                            "hardware_name": dev_os.hardware_name,
                            "hardware_txt": dev_os.hardware_txt,
                            "processor": dev_os.processor,
                            "bios": dev_os.bios,
                            "memory_capacity": dev_os.memory_capacity,
                            "hdd_capacity": dev_os.hdd_capacity,
                            "mac_addr": dev_os.mac_addr,
                            "ip_addr": dev_os.ip_addr,
                            "netmask": dev_os.netmask,
                            "gateway": dev_os.gateway,
                            "dns": dev_os.dns,
                            "manufacture": dev_os.manufacture,
                            "model": dev_os.model,
                            "serial_no": dev_os.serial_no
                        }

                return result(self.API_NAME, object, None, 200)

    def put(self, pk):
        input = self.parser.parse_args()

        old_device = db.session.query(Devices).filter_by(pk=pk).first()

        # -1: UNKNOWN
        var_act_name_id = -1
        var_act_type_id = -1
        var_old_value = TYPE_UNKNOWN
        var_new_value = TYPE_UNKNOWN

        var_update_list = {}

        # [ check ] param
        if input['alias'] is not None:
            var_update_list["alias"] = input['alias']
            # 11: Device Info - Alias
            var_act_name_id = 11
            # 4: Touch
            var_act_type_id = 4
            var_old_value = old_device.alias
            var_new_value = input['alias']
        if input['memo'] is not None:
            var_update_list["memo"] = input['memo']
            # 12: Device Info - Memo
            var_act_name_id = 12
            # 4: Touch
            var_act_type_id = 4
            var_old_value = old_device.memo
            var_new_value = input['memo']
        if input['device_group_fk'] is not None:
            var_update_list["device_group_fk"] = input['device_group_fk']
            # 21: Device Setting - Group
            var_act_name_id = 21
            # 4: Touch
            var_act_type_id = 4
            var_old_value = old_device.device_group_fk
            var_new_value = input['device_group_fk']

        db.session.query(Devices).filter_by(pk=pk).update(var_update_list)
        db.session.commit()

        # [ register ] Audit History
        new_audit = AuditHistory()
        new_audit.reg_mail = self.user.mail
        new_audit.act_time = get_current_time("UTC")
        new_audit.act_name_id = var_act_name_id
        new_audit.act_type_id = var_act_type_id
        new_audit.old_value = var_old_value
        new_audit.new_value = var_new_value
        new_audit.device_fk = pk

        db.session.add(new_audit)
        db.session.commit()

        return result(self.API_NAME, None, None, 200)

# ManagerList
#    @GET : Manager List
#    @POST : Manager 등록 및 인증 메일 보내기
class ManagerListAPI(Resource):
    def __init__(self):
        self.API_NAME = "ManagerListAPI"
        self.API = "/lmm/api/v1.0/managers"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        self.mail_manager = SmtpManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        # Filter -------------------------------------------------------------------------------------------------------
        self.filter_auth = []
        # Param --------------------------------------------------------------------------------------------------------
        self.parser.add_argument("mail", type=str, location="json")
        self.parser.add_argument("pw", type=str, location="json")
        #---------------------------------------------------------------------------------------------------------------
        super(ManagerListAPI, self).__init__()

    def get(self):
        pass

    def post(self):
        input = self.parser.parse_args()

        server = db.session.query(Servers).filter_by(domain_lb=DOMAIN_LABEL).first()

        new_user = Users()
        new_user.mail = input['mail']
        # 0: mail 미 인증
        new_user.mail_auth = 0
        new_user.pw = pw_encode(input['pw'])
        # 17: UTC
        new_user.time_zone_id = 17
        new_user.date_fmt_id = 0
        new_user.member_type_id = 2
        new_user.member_auth_id = 0
        new_user.flag_active = 1
        new_user.refresh_interval = 3000
        new_user.img_refresh_interval = 30
        new_user.create_time = get_current_time("UTC")
        new_user.warrant_start_time = get_current_time("UTC")
        new_user.warrant_expire_time = get_interval_time("UTC", "+", "D", server.df_warrant_vol)
        new_user.flag_refresh = False
        new_user.flag_img_refresh = True
        new_user.flag_recept_mail = False
        new_user.flag_use_otp = False
        new_user.flag_create_qrcode = False
        new_user.flag_duplicate_prevent = False
        new_user.otp_sk = random.random()
        new_user.dev_vol = server.df_dv_vol
        new_user.create_member_vol = server.df_member_vol

        db.session.add(new_user)
        db.session.commit()

        # set token
        new_token = Tokens()
        new_token.reg_mail = new_user.mail
        new_token.token = self.token_manager.set_token(new_user.mail)
        new_token.member_fk = new_user.pk

        db.session.add(new_token)
        db.session.commit()

        # set default Group
        new_device_group = DeviceGroups()
        new_device_group.name = "ROOT"
        new_device_group.create_time = get_current_time("UTC")
        new_device_group.update_time = get_current_time("UTC")
        new_device_group.flag_df_group = True
        new_device_group.flag_confirm = False
        new_device_group.member_fk = new_user.pk

        db.session.add(new_device_group)
        db.session.commit()

        html = "<html>" \
              "<head>" \
              "<title>Mail Content</title>" \
              "</head>" \
              "<body>" \
              "<div id='wrap'>" \
              "<h3>Link Me Mine 가입 ★ 축 ★</h3>" \
              "<p>로그인을 위한 인증이 필요 합니다. 아래의 링크를 클릭하여주시기 바랍니다.</p>" \
              "<a target='_blank' href='"+str(server.domain)+"access/auth?key="+str(new_token.token)+"'>인증</a>" \
              "</div>" \
              "</body>" \
              "</html>"

        self.mail_manager.send_mail(input['mail'], "[인증] 귀하의 Link Me Mine 가입을 축하합니다.", html)

        if DEBUG_ACT:
            self.debug["notice"] = "Register New Manager Send Mail"
            self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")

# Manager
#    @GET : Manager 정보
#    @PUT : Manager 정보 수정
#    @DEL : Manager 정보 삭제
class ManagerAPI(Resource):
    def __init__(self):
        self.API_NAME = "ManagerAPI"
        self.API = "/lmm/api/v1.0/managers/"
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        #---------------------------------------------------------------------------------------------------------------
        super(ManagerAPI, self).__init__()

# UserList
#    @GET : User List
#    @POST : User 생성 및 인증 메일 보내기
class UserListAPI(Resource):
    def __init__(self):
        self.API_NAME = "UserListAPI"
        self.API = "/lmm/api/v1.0/users"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        self.mail_manager = SmtpManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        # Filter -------------------------------------------------------------------------------------------------------
        self.filter_auth = []
        # Param --------------------------------------------------------------------------------------------------------
        self.parser.add_argument("mail", type=str, location="json")
        self.parser.add_argument("flag_recept_mail", type=bool, location="json")
        self.parser.add_argument("pw", type=str, location="json")
        self.parser.add_argument("flag_duplicate_prevent", type=bool, location="json")
        self.parser.add_argument("name", type=str, location="json")
        self.parser.add_argument("phone", type=str, location="json")
        self.parser.add_argument("tel", type=str, location="json")
        self.parser.add_argument("position", type=str, location="json")
        self.parser.add_argument("department", type=str, location="json")
        #---------------------------------------------------------------------------------------------------------------
        super(UserListAPI, self).__init__()

    def get(self):
        #param
        param = {}
        param["search_mode"] = request.args.get('search_mode')
        param["servant_flag"] = request.args.get('flag_servant')

        order_by = Users.create_time
        if param["search_mode"] == 'pagination':
            #pagination setting
            limit = int(request.args.get('limit'))
            offset = int(request.args.get('offset'))
            order = request.args.get('order')
            by = request.args.get('by')
            order_by = by + " " + order.upper()

        filter_and_group = []
        filter_or_group = []

        # [ make ] servant
        if param["servant_flag"]:
            filter_and_group.append(Users.member_fk == self.user.pk)

        # [ make ] query
        user_list = db.session.query(Users).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).order_by(order_by)
        total_count = user_list.count()
        if param["search_mode"] == 'pagination':
            user_list = db.session.query(Users).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).order_by(order_by).limit(limit).offset(offset)
        current_count = user_list.count()

        if param["search_mode"] == 'pagination':
            # [ create ] Pagination
            previous = None
            next = None
            offset_list = {}
            offset_list["previous"] = offset - limit
            offset_list["next"] = offset + limit

            if offset != 0:
                previous = self.API + "?limit=" + str(limit) + "&offset=" + str(offset_list["previous"])
                if (total_count - (offset+current_count)) > limit:
                    next = self.API + "?limit=" + str(limit) + "&offset=" + str(offset_list["next"])
                elif (total_count - (offset+current_count)) <= limit:
                    next = None

        meta = {}
        objects = []
        if current_count == 0:
            meta["total_count"] = 0
            if param["search_mode"] == 'pagination':
                meta["limit"] = limit
                meta["offset"] = offset
                meta["next"] = 0
                meta["previous"] = 0

            objects.append({
                "pk": "EMPTY",
            })
            if DEBUG_ACT:
                self.debug["notice"] = "User EMPTY"
                self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(objects)
            return result(self.API_NAME, objects, meta, 200)
        else:
            meta["total_count"] = total_count
            if param["search_mode"] == 'pagination':
                meta["limit"] = limit
                meta["offset"] = offset
                meta["next"] = next
                meta["previous"] = previous

            for user in user_list:
                dev_cnt = db.session.query(Devices).filter_by(member_fk=user.pk).count()
                create_member_cnt = db.session.query(Users).filter_by(member_fk=user.pk).count()
                objects.append({
                    "pk": user.pk,
                    "mail": user.mail,
                    "name": user.name,
                    "department": user.department,
                    "position": user.position,
                    "phone": user.phone,
                    "tel": user.tel,
                    "time_zone_id": user.time_zone_id,
                    "date_fmt_id": user.date_fmt_id,
                    "member_type_id": user.member_type_id,
                    "member_auth_id": user.member_auth_id,
                    "refresh_interval": user.refresh_interval,
                    "img_refresh_interval": user.img_refresh_interval,
                    "flag_active": user.flag_active,
                    "flag_refresh": user.flag_refresh,
                    "flag_img_refresh": user.flag_img_refresh,
                    "flag_recept_mail": user.flag_recept_mail,
                    "flag_use_otp": user.flag_use_otp,
                    "flag_create_qrcode": user.flag_create_qrcode,
                    "flag_duplicate_prevent": user.flag_duplicate_prevent,
                    "dev_vol": user.dev_vol,
                    "dev_cnt": dev_cnt,
                    "create_member_vol": user.create_member_vol,
                    "create_member_cnt": create_member_cnt,
                    "create_time": time_encode(user.create_time),
                    "warrant_start_time": time_encode(user.warrant_start_time),
                    "warrant_expire_time": time_encode(user.warrant_expire_time)
                })

            if DEBUG_ACT:
                self.debug["notice"] = "Get User"
                self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(objects)
            return result(self.API_NAME, objects, meta, 200)

    def post(self):
        input = self.parser.parse_args()

        var_manager = self.user
        create_member_cnt = db.session.query(Users).filter_by(member_fk=var_manager.pk).count()

        # [ validate ] member volume
        if create_member_cnt >= var_manager.create_member_vol:
            if DEBUG_ACT:
                self.debug["notice"] = "create member volume over"
                self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")
                return result(self.API_NAME, None, None, 401)

        server = db.session.query(Servers).filter_by(domain_lb=DOMAIN_LABEL).first()

        new_user = Users()
        # [ validate ] member volume
        check_user = db.session.query(Users).filter_by(mail=input['mail']).first()
        if check_user is None:
            new_user.mail = input['mail']
        else:
            if DEBUG_ACT:
                self.debug["notice"] = "duplicate User Mail"
                self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")
                return result(self.API_NAME, None, None, 401)

        # 0: mail 미 인증
        new_user.mail_auth = 0
        new_user.pw = pw_encode(input['pw'])
        # 17: UTC
        new_user.time_zone_id = 17
        new_user.date_fmt_id = 0
        new_user.member_type_id = 1
        new_user.member_auth_id = 0
        new_user.flag_active = 1
        new_user.refresh_interval = 3000
        new_user.img_refresh_interval = 30
        new_user.create_time = get_current_time("UTC")
        new_user.warrant_start_time = var_manager.warrant_start_time
        new_user.warrant_expire_time = var_manager.warrant_expire_time
        new_user.flag_refresh = False
        new_user.flag_img_refresh = True
        if input["flag_recept_mail"] is not None:
            new_user.flag_recept_mail = input['flag_recept_mail']
        else:
            new_user.flag_recept_mail = False
        new_user.flag_use_otp = False
        new_user.flag_create_qrcode = False
        new_user.flag_duplicate_prevent = False
        new_user.otp_sk = random.random()
        new_user.dev_vol = var_manager.dev_vol
        new_user.create_member_vol = var_manager.create_member_vol
        if input["name"] is not None:
            new_user.name = input["name"]
        if input["phone"] is not None:
            new_user.phone = input["phone"]
        if input["tel"] is not None:
            new_user.tel = input["tel"]
        if input["position"] is not None:
            new_user.position = input["position"]
        if input["department"] is not None:
            new_user.department = input["department"]
        new_user.member_fk = self.user.pk

        db.session.add(new_user)
        db.session.commit()

        # set token
        new_token = Tokens()
        new_token.reg_mail = new_user.mail
        new_token.token = self.token_manager.set_token(new_user.mail)
        new_token.member_fk = new_user.pk

        db.session.add(new_token)
        db.session.commit()

        html = "<html>" \
              "<head>" \
              "<title>Mail Content</title>" \
              "</head>" \
              "<body>" \
              "<div id='wrap'>" \
              "<h3>Link Me Mine 가입 ★ 축 ★</h3>" \
              "<p>로그인을 위한 인증이 필요 합니다. 아래의 링크를 클릭하여주시기 바랍니다.</p>" \
              "<a target='_blank' href='"+str(server.domain)+"access/auth?key="+str(new_token.token)+"'>인증</a>" \
              "</div>" \
              "</body>" \
              "</html>"

        self.mail_manager.send_mail(input['mail'], "[인증] 귀하의 Link Me Mine 가입을 축하합니다.", html)

        if DEBUG_ACT:
            self.debug["notice"] = "Register New User Send Mail"
            self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")

        return result(self.API_NAME, None, None, 200)

# User
#    @GET : User 정보
#    @PUT : User 정보 수정
#    @DEL : User 정보 삭제
class UserAPI(Resource):
    def __init__(self):
        self.API_NAME = "UserAPI"
        self.API = "/lmm/api/v1.0/users/"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        # Filter -------------------------------------------------------------------------------------------------------
        self.filter_auth = []
        # Param --------------------------------------------------------------------------------------------------------
        self.parser.add_argument("mail", type=str, location="json")
        self.parser.add_argument("pw", type=str, location="json")
        self.parser.add_argument("flag_use_otp", type=bool, location="json")
        self.parser.add_argument("time_zone_id", type=int, location="json")
        self.parser.add_argument("date_fmt_id", type=int, location="json")
        self.parser.add_argument("flag_duplicate_prevent", type=bool, location="json")
        self.parser.add_argument("flag_create_qrcode", type=bool, location="json")
        self.parser.add_argument("name", type=str, location="json")
        self.parser.add_argument("phone", type=str, location="json")
        self.parser.add_argument("tel", type=str, location="json")
        self.parser.add_argument("position", type=str, location="json")
        self.parser.add_argument("department", type=str, location="json")
        self.parser.add_argument("flag_active", type=bool, location="json")
        self.parser.add_argument("flag_refresh", type=bool, location="json")
        self.parser.add_argument("flag_img_refresh", type=bool, location="json")
        self.parser.add_argument("refresh_interval", type=int, location="json")
        self.parser.add_argument("img_refresh_interval", type=int, location="json")
        self.parser.add_argument("flag_recept_mail", type=bool, location="json")
        self.parser.add_argument("logout", type=str, location="json")
        #---------------------------------------------------------------------------------------------------------------
        super(UserAPI, self).__init__()

    def get(self, pk):
        if pk == TYPE_SELF_PK:
            dev_cnt = db.session.query(Devices).filter_by(member_fk=self.user.pk).count()
            create_member_cnt = db.session.query(Users).filter_by(member_fk=self.user.pk).count()
            object = {
                "pk": self.user.pk,
                "mail": self.user.mail,
                "name": self.user.name,
                "department": self.user.department,
                "position": self.user.position,
                "phone": self.user.phone,
                "tel": self.user.tel,
                "time_zone_id": self.user.time_zone_id,
                "date_fmt_id": self.user.date_fmt_id,
                "member_type_id": self.user.member_type_id,
                "member_auth_id": self.user.member_auth_id,
                "refresh_interval": self.user.refresh_interval,
                "img_refresh_interval": self.user.img_refresh_interval,
                "flag_active": self.user.flag_active,
                "flag_refresh": self.user.flag_refresh,
                "flag_img_refresh": self.user.flag_img_refresh,
                "flag_recept_mail": self.user.flag_recept_mail,
                "flag_use_otp": self.user.flag_use_otp,
                "flag_create_qrcode": self.user.flag_create_qrcode,
                "flag_duplicate_prevent": self.user.flag_duplicate_prevent,
                "dev_vol": self.user.dev_vol,
                "dev_cnt": dev_cnt,
                "create_member_vol": self.user.create_member_vol,
                "create_member_cnt": create_member_cnt,
                "create_time": time_encode(self.user.create_time),
                "warrant_start_time": time_encode(self.user.warrant_start_time),
                "warrant_expire_time": time_encode(self.user.warrant_expire_time),
                "type": self.user_type
            }

            return result(self.API_NAME, object, None, 200)
        else:
            user = db.session.query(Users).filter_by(pk=pk).first()
            dev_cnt = db.session.query(Devices).filter_by(member_fk=pk).count()
            create_member_cnt = db.session.query(Users).filter_by(member_fk=pk).count()
            object = {
                "pk": user.pk,
                "mail": user.mail,
                "name": user.name,
                "department": user.department,
                "position": user.position,
                "phone": user.phone,
                "tel": user.tel,
                "time_zone_id": user.time_zone_id,
                "date_fmt_id": user.date_fmt_id,
                "member_type_id": user.member_type_id,
                "member_auth_id": user.member_auth_id,
                "refresh_interval": user.refresh_interval,
                "img_refresh_interval": user.img_refresh_interval,
                "flag_active": user.flag_active,
                "flag_refresh": user.flag_refresh,
                "flag_img_refresh": user.flag_img_refresh,
                "flag_recept_mail": user.flag_recept_mail,
                "flag_use_otp": user.flag_use_otp,
                "flag_create_qrcode": user.flag_create_qrcode,
                "flag_duplicate_prevent": user.flag_duplicate_prevent,
                "dev_vol": user.dev_vol,
                "dev_cnt": dev_cnt,
                "create_member_vol": user.create_member_vol,
                "create_member_cnt": create_member_cnt,
                "create_time": time_encode(user.create_time),
                "warrant_start_time": time_encode(user.warrant_start_time),
                "warrant_expire_time": time_encode(user.warrant_expire_time)
            }

            return result(self.API_NAME, object, None, 200)

    def put(self, pk):
        input = self.parser.parse_args()

        var_update_list = {}

        if input["mail"] is not None:
            var_update_list["mail"] = input["mail"]
        if input["pw"] is not None:
            var_update_list["pw"] = pw_encode(input["pw"])
        if input["flag_use_otp"] is not None:
            var_update_list["flag_use_otp"] = input["flag_use_otp"]
        if input["time_zone_id"] is not None:
            var_update_list["time_zone_id"] = input["time_zone_id"]
        if input["date_fmt_id"] is not None:
            var_update_list["date_fmt_id"] = input["date_fmt_id"]
        if input["flag_duplicate_prevent"] is not None:
            var_update_list["flag_duplicate_prevent"] = input["flag_duplicate_prevent"]
        if input["flag_create_qrcode"] is not None:
            var_update_list["flag_create_qrcode"] = input["flag_create_qrcode"]
            var_update_list["otp_sk"] = random.random()
        if input["name"] is not None:
            var_update_list["name"] = input["name"]
        if input["phone"] is not None:
            var_update_list["phone"] = input["phone"]
        if input["tel"] is not None:
            var_update_list["tel"] = input["tel"]
        if input["position"] is not None:
            var_update_list["position"] = input["position"]
        if input["department"] is not None:
            var_update_list["department"] = input["department"]
        if input["flag_active"] is not None:
            var_update_list["flag_active"] = input["flag_active"]
        if input["flag_refresh"] is not None:
            var_update_list["flag_refresh"] = input["flag_refresh"]
        if input["refresh_interval"] is not None:
            var_update_list["refresh_interval"] = input["refresh_interval"]
        if input["flag_img_refresh"] is not None:
            var_update_list["flag_img_refresh"] = input["flag_img_refresh"]
        if input["img_refresh_interval"] is not None:
            var_update_list["img_refresh_interval"] = input["img_refresh_interval"]
        if input["flag_recept_mail"] is not None:
            var_update_list["flag_recept_mail"] = input["flag_recept_mail"]
        if input["logout"] is not None:
            var_update_list["logout_time"] = get_current_time("UTC")

        if pk == TYPE_SELF_PK:
            db.session.query(Users).filter_by(pk=self.user.pk).update(var_update_list)
        else:
            db.session.query(Users).filter_by(pk=pk).update(var_update_list)
        db.session.commit()

        return result(self.API_NAME, None, None, 200)

    def delete(self, pk):
        delete_user = db.session.query(Users).filter_by(pk=pk).first()

        if delete_user is not None:
            db.session.delete(delete_user)
            db.session.commit()

        return result(self.API_NAME, None, None, 200)

# CompanyList
#    @GET : Company List
class CompanyListAPI(Resource):
    def __init__(self):
        self.API_NAME = "UserAPI"
        self.API = "/lmm/api/v1.0/company"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        # Filter -------------------------------------------------------------------------------------------------------
        self.filter_auth = []
        # Param --------------------------------------------------------------------------------------------------------
        #---------------------------------------------------------------------------------------------------------------
        super(CompanyListAPI, self).__init__()

    def get(self):
        pass

# Company
#    @GET : Company 정보
#    @PUT : Company 정보 수정
#    @DEL : Company 정보 삭제
class CompanyAPI(Resource):
    def __init__(self):
        self.API_NAME = "UserAPI"
        self.API = "/lmm/api/v1.0/company/"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        # Filter -------------------------------------------------------------------------------------------------------
        self.filter_auth = []
        # Param --------------------------------------------------------------------------------------------------------
        self.parser.add_argument("name", type=str, location="json")
        self.parser.add_argument("ceo_name", type=str, location="json")
        self.parser.add_argument("corp_reg_no", type=str, location="json")
        self.parser.add_argument("tel", type=str, location="json")
        self.parser.add_argument("fax", type=str, location="json")
        self.parser.add_argument("hp_uri", type=str, location="json")
        #---------------------------------------------------------------------------------------------------------------
        super(CompanyAPI, self).__init__()

    def get(self, pk):
        # [ case by ] pk == 0
        if pk == TYPE_SELF_PK:
            corp = db.session.query(Corps).filter_by(member_fk=self.user.pk).first()
            if corp is not None:
                object = {
                    "pk": corp.pk,
                    "name": corp.name,
                    "ceo_name": corp.ceo_name,
                    "corp_reg_no": corp.corp_reg_no,
                    "tel": corp.tel,
                    "fax": corp.fax,
                    "hp_uri": corp.hp_uri,
                    "member_fk": corp.member_fk
                }

                return result(self.API_NAME, object, None, 200)

        # [ case by ] pk != 0
        corp = db.session.query(Corps).filter_by(member_fk=pk).first()
        if corp is not None:
            object = {
                "pk": corp.pk,
                "name": corp.name,
                "ceo_name": corp.ceo_name,
                "corp_reg_no": corp.corp_reg_no,
                "tel": corp.tel,
                "fax": corp.fax,
                "hp_uri": corp.hp_uri,
                "member_fk": corp.member_fk
            }

            return result(self.API_NAME, object, None, 200)

        # [ case by ] EMPTY
        return result(self.API_NAME, None, None, 404)

    def put(self, pk):
        input = self.parser.parse_args()

        var_update_list = {}

        if input["name"] is not None:
            var_update_list["name"] = input["name"]
        if input["ceo_name"] is not None:
            var_update_list["ceo_name"] = input["ceo_name"]
        if input["corp_reg_no"] is not None:
            var_update_list["corp_reg_no"] = input["corp_reg_no"]
        if input["tel"] is not None:
            var_update_list["tel"] = input["tel"]
        if input["fax"] is not None:
            var_update_list["fax"] = input["fax"]
        if input["hp_uri"] is not None:
            var_update_list["hp_uri"] = input["hp_uri"]

        if pk == TYPE_SELF_PK:
            db.session.query(Corps).filter_by(member_fk=self.user.pk).update(var_update_list)
        else:
            db.session.query(Corps).filter_by(member_fk=pk).update(var_update_list)
        db.session.commit()

        return result(self.API_NAME, None, None, 200)

# AuditHistoryList
#    @GET : Audit History List
class AuditHistoryListAPI(Resource):
    def __init__(self):
        self.API_NAME = "AuditHistoryListAPI"
        self.API = "/lmm/api/v1.0/history/audit"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        # Filter -------------------------------------------------------------------------------------------------------
        self.filter_auth = []
        # Param --------------------------------------------------------------------------------------------------------
        #---------------------------------------------------------------------------------------------------------------
        super(AuditHistoryListAPI, self).__init__()

    def get(self):
        #param
        param = {}
        param["device_pk"] = request.args.get('device_pk')

        #pagination setting
        limit = int(request.args.get('limit'))
        offset = int(request.args.get('offset'))
        order = request.args.get('order')
        by = request.args.get('by')
        order_by = by + " " + order.upper()

        filter_and_group = []
        filter_or_group = []

        # [ make ] filter
        if param["device_pk"] is not None:
            filter_and_group.append(AuditHistory.device_fk == param["device_pk"])

        # [ make ] query
        audit_list = db.session.query(AuditHistory).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).order_by(order_by)
        total_count = audit_list.count()
        audit_list = audit_list.limit(limit).offset(offset)
        current_count = audit_list.count()

        # [ create ] Pagination
        previous = None
        next = None
        offset_list = {}
        offset_list["previous"] = offset - limit
        offset_list["next"] = offset + limit

        if offset != 0:
            previous = self.API + "?limit=" + str(limit) + "&offset=" + str(offset_list["previous"])
            if (total_count - (offset+current_count)) > limit:
                next = self.API + "?limit=" + str(limit) + "&offset=" + str(offset_list["next"])
            elif (total_count - (offset+current_count)) <= limit:
                next = None

        meta = {}
        objects = []
        if current_count == 0:
            meta["total_count"] = 0
            meta["limit"] = limit
            meta["offset"] = offset
            meta["next"] = 0
            meta["previous"] = 0

            objects.append({
                "pk": "EMPTY",
            })
            if DEBUG_ACT:
                self.debug["notice"] = "Audit EMPTY"
                self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(objects)
            return result(self.API_NAME, objects, meta, 200)
        else:
            meta["total_count"] = total_count
            meta["limit"] = limit
            meta["offset"] = offset
            meta["next"] = next
            meta["previous"] = previous

            for audit in audit_list:
                objects.append({
                    "pk": audit.pk,
                    "reg_mail": audit.reg_mail,
                    "act_time": time_encode(audit.act_time),
                    "act_name_id": audit.act_name_id,
                    "act_type_id": audit.act_type_id,
                    "old_value": audit.old_value,
                    "new_value": audit.new_value,
                    "device_fk": audit.device_fk
                })

            if DEBUG_ACT:
                self.debug["notice"] = "GET Audit List Success"
                self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(objects)
            return result(self.API_NAME, objects, meta, 200)

# AuditHistory
#    @GET : Audit History 정보
#    @PUT : Audit History 정보 수정
#    @DEL : Audit History 정보 삭제
class AuditHistoryAPI(Resource):
    def __init__(self):
        self.API_NAME = "AuditHistoryAPI"
        self.API = "/lmm/api/v1.0/history/audit/"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        #---------------------------------------------------------------------------------------------------------------
        super(AuditHistoryAPI, self).__init__()

# RemoteHistoryList
#    @GET : Remote History List
class RemoteHistoryListAPI(Resource):
    def __init__(self):
        self.API_NAME = "RemoteHistoryListAPI"
        self.API = "/lmm/api/v1.0/history/remote"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        # Filter -------------------------------------------------------------------------------------------------------
        self.filter_user_type = []
        if self.user_type == TYPE_MANAGER:
            self.filter_user_type.append(Devices.member_fk == self.user.pk)

        if self.user_type == TYPE_MEMBER:
            device_groups = ConnMemberNDeviceGroup.query.join(Users, ConnMemberNDeviceGroup.member_fk == Users.pk).filter(Users.pk == self.user.pk).all()

            # User doesn't have any device group
            if len(device_groups) == 0:
                self.filter_user_type.append(Devices.pk == 0)
            else:
                for dg in device_groups:
                    devices = Devices.query.filter(Devices.device_group_fk == dg.pk).filter(Devices.member_fk == self.user.member_fk).all()
                    if len(devices) == 0:
                        self.filter_user_type.append(Devices.pk == 0)
                    else:
                        for device in devices:
                            self.filter_user_type.append(Devices.pk == device.pk)
        #---------------------------------------------------------------------------------------------------------------
        super(RemoteHistoryListAPI, self).__init__()

    def get(self):
        if self.validateToken is False:
            return result(self.API_NAME, None, None, 401)

        #param
        #param_user_pk = request.args.get('user_pk')

        #pagination setting
        limit = int(request.args.get('limit'))
        offset = int(request.args.get('offset'))
        order = request.args.get('order')
        by = request.args.get('by')
        order_by = by + " " + order.upper()

        filter_and_group = []
        filter_or_group = []

        # [ make ] query
        remote_history_list = db.session.query(RemoteHistory).join(Devices, RemoteHistory.device_fk == Devices.pk).\
                              filter(and_(*filter_and_group)).\
                              filter(or_(*filter_or_group)).\
                              filter(or_(*self.filter_user_type)).\
                              order_by(order_by)

        total_count = remote_history_list.count()
        remote_history_list = remote_history_list.limit(limit).offset(offset)
        current_count = remote_history_list.count()

        # [ create ] Pagination
        previous = None
        next = None
        offset_list = {}
        offset_list["previous"] = offset - limit
        offset_list["next"] = offset + limit

        if offset != 0:
            previous = self.API + "?limit=" + str(limit) + "&offset=" + str(offset_list["previous"])
            if (total_count - (offset+current_count)) > limit:
                next = self.API + "?limit=" + str(limit) + "&offset=" + str(offset_list["next"])
            elif (total_count - (offset+current_count)) <= limit:
                next = None

        meta = {}
        objects = []
        if current_count == 0:
            meta["total_count"] = 0
            meta["limit"] = limit
            meta["offset"] = offset
            meta["next"] = 0
            meta["previous"] = 0

            objects.append({
                "pk": "EMPTY",
            })
            return result(self.API_NAME, objects, meta, 200)
        else:
            meta["total_count"] = total_count
            meta["limit"] = limit
            meta["offset"] = offset
            meta["next"] = next
            meta["previous"] = previous

            for remote_history in remote_history_list:
                objects.append({
                    "pk": remote_history.pk,
                    "reg_mail": remote_history.reg_mail,
                    "name": remote_history.name,
                    "guid": remote_history.guid,
                    "alias": remote_history.alias,
                    "dev_os_id": remote_history.dev_os_id,
                    "host_public_ip": remote_history.host_public_ip,
                    "host_private_ip": remote_history.host_private_ip,
                    "viewer_public_ip": remote_history.viewer_public_ip,
                    "viewer_private_ip": remote_history.viewer_private_ip,
                    "start_time": time_encode(remote_history.start_time),
                    "end_time": time_encode(remote_history.end_time),
                    "member_fk": remote_history.member_fk,
                    "device_fk": remote_history.device_fk
                })

            return result(self.API_NAME, objects, meta, 200)

# RemoteHistory
#    @GET : Remote History 정보
#    @PUT : Remote History 정보 수정
#    @DEL : Remote History 정보 삭제
class RemoteHistoryAPI(Resource):
    def __init__(self):
        self.API_NAME = "RemoteHistoryAPI"
        self.API = "/lmm/api/v1.0/history/remote/"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        #---------------------------------------------------------------------------------------------------------------
        super(RemoteHistoryAPI, self).__init__()

# AlertHistoryList
#    @GET : Alert History List
class AlertHistoryListAPI(Resource):
    def __init__(self):
        self.API_NAME = "AlertHistoryListAPI"
        self.API = "/lmm/api/v1.0/history/alert"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        #---------------------------------------------------------------------------------------------------------------
        super(AlertHistoryListAPI, self).__init__()

    def get(self):
        if self.validateToken is False:
            return result(self.API_NAME, None, None, 401)

        #param
        #param_user_pk = request.args.get('user_pk')

        #pagination setting
        limit = int(request.args.get('limit'))
        offset = int(request.args.get('offset'))
        order = request.args.get('order')
        by = request.args.get('by')
        order_by = by + " " + order.upper()

        filter_and_group = []
        filter_or_group = []

        # [ make ] filter
        if self.user_type == TYPE_MANAGER:
            filter_or_group.append(Devices.member_fk == self.user.pk)

        if self.user_type == TYPE_MEMBER:
            device_groups = ConnMemberNDeviceGroup.query.join(Users, ConnMemberNDeviceGroup.member_fk == Users.pk).filter(Users.pk == self.user.pk).all()

            # User doesn't have any device group
            if len(device_groups) == 0:
                filter_or_group.append(Devices.pk == 0)
            else:
                for dg in device_groups:
                    devices = Devices.query.filter(Devices.device_group_fk == dg.pk).filter(Devices.member_fk == self.user.member_fk).all()
                    if len(devices) == 0:
                        filter_or_group.append(Devices.pk == 0)
                    else:
                        for device in devices:
                            filter_or_group.append(Devices.pk == device.pk)
            filter_and_group.append(RemoteHistory.member_fk == self.user.pk)

        # [ make ] query
        remote_history_list = db.session.query(RemoteHistory).filter(and_(*filter_and_group)).filter(or_(*filter_or_group)).order_by(order_by)
        total_count = remote_history_list.count()
        remote_history_list = remote_history_list.limit(limit).offset(offset)
        current_count = remote_history_list.count()

        # [ create ] Pagination
        previous = None
        next = None
        offset_list = {}
        offset_list["previous"] = offset - limit
        offset_list["next"] = offset + limit

        if offset != 0:
            previous = self.API + "?limit=" + str(limit) + "&offset=" + str(offset_list["previous"])
            if (total_count - (offset+current_count)) > limit:
                next = self.API + "?limit=" + str(limit) + "&offset=" + str(offset_list["next"])
            elif (total_count - (offset+current_count)) <= limit:
                next = None

        meta = {}
        objects = []
        if current_count == 0:
            meta["total_count"] = 0
            meta["limit"] = limit
            meta["offset"] = offset
            meta["next"] = 0
            meta["previous"] = 0

            objects.append({
                "pk": "EMPTY",
            })
            return result(self.API_NAME, objects, meta, 200)
        else:
            meta["total_count"] = total_count
            meta["limit"] = limit
            meta["offset"] = offset
            meta["next"] = next
            meta["previous"] = previous

            for remote_history in remote_history_list:
                objects.append({
                    "pk": remote_history.pk,
                    "reg_mail": remote_history.reg_mail,
                    "name": remote_history.name,
                    "guid": remote_history.guid,
                    "alias": remote_history.alias,
                    "dev_os_id": remote_history.dev_os_id,
                    "host_public_ip": remote_history.host_public_ip,
                    "host_private_ip": remote_history.host_private_ip,
                    "viewer_public_ip": remote_history.viewer_public_ip,
                    "viewer_private_ip": remote_history.viewer_private_ip,
                    "start_time": time_encode(remote_history.start_time),
                    "end_time": time_encode(remote_history.end_time),
                    "member_fk": remote_history.member_fk,
                    "device_fk": remote_history.device_fk
                })

            return result(self.API_NAME, objects, meta, 200)

# AlertHistory
#    @GET : Alert History 정보
#    @PUT : Alert History 정보 수정
#    @DEL : Alert History 정보 삭제
class AlertHistoryAPI(Resource):
    def __init__(self):
        self.API_NAME = "AlertHistoryAPI"
        self.API = "/lmm/api/v1.0/history/alert/"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        #---------------------------------------------------------------------------------------------------------------
        super(AlertHistoryAPI, self).__init__()

# RemoteDashListAPI
#    @GET : Remote Dash List
class RemoteDashListAPI(Resource):
    def __init__(self):
        self.API_NAME = "RemoteDashListAPI"
        self.API = "/lmm/api/v1.0/dash/remote"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        # Filter -------------------------------------------------------------------------------------------------------
        self.filter_user_type = []
        if self.user_type == TYPE_MANAGER:
            self.filter_user_type.append(Devices.member_fk == self.user.pk)

        if self.user_type == TYPE_MEMBER:
            device_groups = ConnMemberNDeviceGroup.query.join(Users, ConnMemberNDeviceGroup.member_fk == Users.pk).filter(Users.pk == self.user.pk).all()

            # User doesn't have any device group
            if len(device_groups) == 0:
                self.filter_user_type.append(Devices.pk == 0)
            else:
                for dg in device_groups:
                    devices = Devices.query.filter(Devices.device_group_fk == dg.pk).filter(Devices.member_fk == self.user.member_fk).all()
                    if len(devices) == 0:
                        self.filter_user_type.append(Devices.pk == 0)
                    else:
                        for device in devices:
                            self.filter_user_type.append(Devices.pk == device.pk)
        #---------------------------------------------------------------------------------------------------------------
        super(RemoteDashListAPI, self).__init__()

    def get(self):
        if self.validateToken is False:
            return result(self.API_NAME, None, None, 401)

        #param
        param = {}
        param["type"] = request.args.get('type')
        param["search_mode"] = request.args.get('search_mode')

        param["start_date"] = str(request.args.get('start_date'))
        param["end_date"] = str(request.args.get('end_date'))
        start_date_obj = datetime.strptime(param["start_date"], "%Y-%m-%d")
        end_date_obj = datetime.strptime(param["end_date"], "%Y-%m-%d")
        start_date = start_date_obj.replace(hour=0, minute=0, second=0, microsecond=0, tzinfo=timezone('UTC'))
        end_date = end_date_obj.replace(hour=0, minute=0, second=0, microsecond=0, tzinfo=timezone('UTC')) + timedelta(days=1)

        #pagination setting
        order_by = RemoteHistory.start_time
        if param["search_mode"] == 'pagination':
            limit = int(request.args.get('limit'))
            offset = int(request.args.get('offset'))
            order = request.args.get('order')
            by = request.args.get('by')
            order_by = by + " " + order.upper()

        filter_and_group = []
        filter_or_group = []
        filter_and_group.append(RemoteHistory.start_time >= start_date.strftime("%Y-%m-%d %H:%M:%S"))
        filter_and_group.append(RemoteHistory.start_time < end_date.strftime("%Y-%m-%d %H:%M:%S"))

        # [ make ] query
        remote_history_list = db.session.query(RemoteHistory).join(Devices, RemoteHistory.device_fk == Devices.pk).\
                              filter(and_(*filter_and_group)).\
                              filter(or_(*filter_or_group)).\
                              filter(or_(*self.filter_user_type)).\
                              order_by(order_by)

        users_history_list = db.session.query(RemoteHistory).join(Devices, RemoteHistory.device_fk == Devices.pk).\
                             filter(or_(*filter_or_group)).\
                             filter(and_(*filter_and_group)).\
                             filter(or_(*self.filter_user_type)).\
                             group_by(RemoteHistory.member_fk).\
                             order_by(order_by)

        remote_history_total_count = remote_history_list.count()

        total_count = users_history_list.count()
        if param["search_mode"] == 'pagination':
            users_history = users_history_list.limit(limit).offset(offset)
        current_count = users_history_list.count()

        if param["search_mode"] == 'pagination':
            # [ create ] Pagination
            previous = None
            next = None
            offset_list = {}
            offset_list["previous"] = offset - limit
            offset_list["next"] = offset + limit

            if offset != 0:
                previous = self.API + "?limit=" + str(limit) + "&offset=" + str(offset_list["previous"])
                if (total_count - (offset+current_count)) > limit:
                    next = self.API + "?limit=" + str(limit) + "&offset=" + str(offset_list["next"])
                elif (total_count - (offset+current_count)) <= limit:
                    next = None

        meta = {}
        objects = []
        if current_count == 0:
            meta["total_count"] = 0
            if param["search_mode"] == 'pagination':
                meta["limit"] = limit
                meta["offset"] = offset
                meta["next"] = 0
                meta["previous"] = 0

            objects.append({
                "pk": "EMPTY",
            })
            return result(self.API_NAME, objects, meta, 200)
        else:
            meta["total_count"] = total_count
            if param["search_mode"] == 'pagination':
                meta["limit"] = limit
                meta["offset"] = offset
                meta["next"] = next
                meta["previous"] = previous

            for user_history in users_history_list:
                member_cnt = db.session.query(RemoteHistory).filter_by(device_fk=user_history.device_fk).count()
                usage = float(member_cnt)/float(remote_history_total_count) * 100
                objects.append({
                    "pk": user_history.pk,
                    "reg_mail": user_history.reg_mail,
                    "usage": usage,
                    "count": member_cnt,
                    "total": remote_history_total_count
                })

            # [ Case by ] length != 10
            if len(objects) != 10:
                limit_range = 10 - len(objects)
                for i in range(0, limit_range):
                    objects.append({
                        "pk": 'vertual_key' + str(i),
                        "reg_mail": "",
                        "usage": -1,
                        "count": -1,
                        "total": -1
                    })

            return result(self.API_NAME, objects, meta, 200)

# RemoteDashAPI
#    @GET : Remote Dash 정보
#    @PUT : Remote Dash 정보 수정
#    @DEL : Remote Dash 정보 삭제
class RemoteDashAPI(Resource):
    def __init__(self):
        self.API_NAME = "RemoteDashAPI"
        self.API = "/lmm/api/v1.0/dash/remote/"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        #---------------------------------------------------------------------------------------------------------------
        super(RemoteDashAPI, self).__init__()

# RemoteChartListAPI
#    @GET : Remote Chart List
class RemoteChartListAPI(Resource):
    def __init__(self):
        self.API_NAME = "RemoteChartListAPI"
        self.API = "/lmm/api/v1.0/chart/remote"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("token", type=str, location="headers")
        self.validateToken = self.token_manager.validate_token(self.parser.parse_args()["token"])

        if self.validateToken:
            self.token_info = self.process_manager.get_token(self.parser.parse_args()["token"])
            self.user = self.process_manager.get_user(self.token_info.member_fk)
            if DEBUG_ACT:
                self.debug["user"] = self.user.mail
            self.user_auth_obj = self.process_manager.check_user_auth(self.user.member_auth_id)
            self.user_type = self.process_manager.check_user_type(self.user.member_type_id)
            self.user_active = self.process_manager.check_user_active(self.user.flag_active)
        #---------------------------------------------------------------------------------------------------------------
        super(RemoteChartListAPI, self).__init__()

    def get(self):
        param = {}
        param["type"] = request.args.get('type')

        if param["type"] == "monthly":
            param["year"] = int(request.args.get('year'))
        if param["type"] == "daily":
            param["year"] = int(request.args.get('year'))
            param["month"] = int(request.args.get('month'))
        if param["type"] == "hourly" or param["type"] == "user-specific":
            param["start_date"] = str(request.args.get('start_date'))
            param["end_date"] = str(request.args.get('end_date'))
            start_date_obj = datetime.strptime(param["start_date"], "%Y-%m-%d")
            end_date_obj = datetime.strptime(param["end_date"], "%Y-%m-%d")
            start_date = start_date_obj.replace(hour=0, minute=0, second=0, microsecond=0, tzinfo=timezone('UTC'))
            end_date = end_date_obj.replace(hour=0, minute=0, second=0, microsecond=0, tzinfo=timezone('UTC')) + timedelta(days=1)

        if param["type"] == "monthly":
            month_range = range(1, 13)
        if param["type"] == "daily":
            month_range = range(1, param["month"]+1)
        if param["type"] == "hourly":
            time_range = range(0, 24)

        user_and_filter = []
        user_or_filter = []

        # [ make ] filter
        if self.user_type == TYPE_MANAGER:
            user_or_filter.append(RemoteHistory.member_fk == self.user.pk)
            member_list = db.session.query(Users).filter_by(member_fk=self.user.pk).all()
            if len(member_list) != 0:
                for servant in member_list:
                    user_or_filter.append(RemoteHistory.member_fk == servant.pk)

        if self.user_type == TYPE_MEMBER:
            user_and_filter.append(RemoteHistory.member_fk == self.user.pk)

        # [ make ] query
        remote_history_list = db.session.query(RemoteHistory).join(Users, RemoteHistory.member_fk == Users.pk).\
                              filter(and_(*user_and_filter)).\
                              filter(or_(*user_or_filter))

        total_count = remote_history_list.count()

        if param["type"] == "monthly":
            objects = []
            for month in month_range:
                date_and_filter = []
                start_date = datetime.utcnow().replace(year=param["year"], month=month, day=1, hour=0, minute=0, second=0, microsecond=0)
                date_and_filter.append(RemoteHistory.start_time >= start_date.strftime("%Y-%m-%d %H:%M:%S"))
                if (month+1) <= 12:
                    end_date = start_date.replace(year=param["year"], month=month+1, day=1, hour=0, minute=0, second=0, microsecond=0)
                else:
                    end_date = start_date.replace(year=param["year"]+1, month=1, day=1, hour=0, minute=0, second=0, microsecond=0)
                date_and_filter.append(RemoteHistory.start_time < end_date.strftime("%Y-%m-%d %H:%M:%S"))

                current_count = remote_history_list.filter(and_(*date_and_filter)).count()
                count_ratio = float(current_count) / float(total_count) * float(100)

                objects.append({
                    "pk": month,
                    "start_date": time_encode(start_date.strftime("%Y-%m")),
                    "end_date": time_encode(end_date.strftime("%Y-%m")),
                    "title_date": time_encode(start_date.strftime("%Y")),
                    "value_date": time_encode(start_date.strftime("%m")),
                    "access_cnt": current_count,
                    "access_ratio": count_ratio
                })

            return result(self.API_NAME, objects, None, 200)

        if param["type"] == "daily":
            objects = []
            for month in month_range:
                if (month+1) <= 12:
                    last_date = datetime.utcnow().replace(year=param["year"], month=month+1, day=1) - timedelta(days=1)
                else:
                    last_date = datetime.utcnow().replace(year=param["year"]+1, month=1, day=1) - timedelta(days=1)
                day_range = range(1, last_date.day+1)

            for day in day_range:
                date_and_filter = []
                start_date = datetime.utcnow().replace(year=param["year"], month=param["month"], day=day, hour=0, minute=0, second=0, microsecond=0)
                end_date = start_date + timedelta(days=1)
                date_and_filter.append(RemoteHistory.start_time >= start_date.strftime("%Y-%m-%d %H:%M:%S"))
                date_and_filter.append(RemoteHistory.start_time < end_date.strftime("%Y-%m-%d %H:%M:%S"))

                current_count = remote_history_list.filter(and_(*date_and_filter)).count()
                count_ratio = float(current_count) / float(total_count) * float(100)

                objects.append({
                    "pk": day,
                    "start_date": time_encode(start_date.strftime("%Y-%m-%d")),
                    "end_date": time_encode(end_date.strftime("%Y-%m-%d")),
                    "title_date": time_encode(start_date.strftime("%Y-%m")),
                    "value_date": time_encode(start_date.strftime("%d")),
                    "access_cnt": current_count,
                    "access_ratio": count_ratio
                })

            return result(self.API_NAME, objects, None, 200)

        if param["type"] == "hourly":
            objects = []

            temp_date = start_date
            for time in time_range:
                date_and_filter = []
                temp_start_date = temp_date.replace(hour=time)
                temp_end_date = temp_start_date + timedelta(hours=1)
                date_and_filter.append(RemoteHistory.start_time >= start_date.strftime("%Y-%m-%d %H:%M:%S"))
                date_and_filter.append(RemoteHistory.start_time < end_date.strftime("%Y-%m-%d %H:%M:%S"))
                date_and_filter.append(db.func.time(RemoteHistory.start_time) >= temp_start_date.strftime("%H:%M:%S"))
                if temp_start_date.strftime("%H") != "23":
                    date_and_filter.append(db.func.time(RemoteHistory.start_time) < temp_end_date.strftime("%H:%M:%S"))

                total_time = "%d:%02d:%02d" % (0, 0, 0)
                average_time = "%d:%02d:%02d" % (0, 0, 0)
                remote_total_seconds = 0
                remote_logs = remote_history_list.filter(and_(*date_and_filter)).all()
                for remote_log in remote_logs:
                    remote_total_seconds += (remote_log.end_time - remote_log.start_time).total_seconds()

                if remote_total_seconds != 0:
                    m, s = divmod(remote_total_seconds, 60)
                    h, m = divmod(m, 60)
                    total_time = "%d:%02d:%02d" % (h, m, s)

                current_count = remote_history_list.filter(and_(*date_and_filter)).count()

                if (remote_total_seconds != 0) and (current_count != 0):
                    m, s = divmod(remote_total_seconds/current_count, 60)
                    h, m = divmod(m, 60)
                    average_time = "%d:%02d:%02d" % (h, m, s)

                count_ratio = float(current_count) / float(total_count) * float(100)

                objects.append({
                    "pk": time + 1,
                    "start_date": time_encode(start_date.strftime("%Y-%m-%d")),
                    "end_date": time_encode(end_date.strftime("%Y-%m-%d")),
                    "title_date": time_encode(start_date.strftime("%Y-%m-%d")) + "~" + time_encode(end_date.strftime("%Y-%m-%d")),
                    "value_date": time_encode(temp_start_date.strftime("%H")),
                    "total_time": total_time,
                    "average_time": average_time,
                    "access_cnt": current_count,
                    "access_ratio": count_ratio
                })

            return result(self.API_NAME, objects, None, 200)

# RemoteChartAPI
#    @GET : Remote Chart 정보
#    @PUT : Remote Chart 정보 수정
#    @DEL : Remote Chart 정보 삭제
class RemoteChartAPI(Resource):
    def __init__(self):
        self.API_NAME = "RemoteChartAPI"
        self.API = "/lmm/api/v1.0/chart/remote/"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_GUEST
        #---------------------------------------------------------------------------------------------------------------
        super(RemoteChartAPI, self).__init__()

#-----------------------------------------------------------------------------------------------------------------------
# [ AGENT ]
#-----------------------------------------------------------------------------------------------------------------------
# AgentAuth
#    @GET : Agent Auth 정보
#    @POST : Agent Auth Step 1-3
class AgentAuthAPI(Resource):
    def __init__(self):
        self.API_NAME = "AgentAuthAPI"
        self.API = "/lmm/api/v1.0/agent/auth"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_AGENT
        # Param --------------------------------------------------------------------------------------------------------
        self.parser.add_argument("step", type=int, location="json")
        self.parser.add_argument("mail", type=str, location="json")
        self.parser.add_argument("pw", type=str, location="json")
        self.parser.add_argument("manager_pk", type=int, location="json")
        self.parser.add_argument("agent_token", type=str, location="json")
        self.parser.add_argument("guid", type=str, location="json")
        #---------------------------------------------------------------------------------------------------------------
        super(AgentAuthAPI, self).__init__()

    def post(self):
        input = self.parser.parse_args()

        # [ case by ] STEP 1
        if input["step"] == 1:
            # validate param
            var_check_param = ["mail", "pw"]
            for i in range(len(var_check_param)):
                if not str(var_check_param[i]) in input:
                    if DEBUG_ACT:
                        self.debug["notice"] = "Required Parameter"
                        self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")
                    return result(self.API_NAME, None, None, 400)

            # validate info
            user = db.session.query(Users).filter_by(mail=input["mail"]).first()
            if user is None:
                if DEBUG_ACT:
                    self.debug["notice"] = "No User"
                    self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")
                return result(self.API_NAME, None, None, 404)

            if user.flag_active == 0:
                if DEBUG_ACT:
                    self.debug["notice"] = "No Auth"
                    self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")
                return result(self.API_NAME, None, None, 401)

            if user.pw == pw_encode(input["pw"]):
                user_type = self.process_manager.check_user_type(user.member_type_id)
                manager = None
                if user_type is TYPE_MEMBER:
                    manager = db.session.query(Users).filter_by(pk=user.member_fk).first()
                if user_type is TYPE_MANAGER:
                    manager = user

                object = {
                    "manager_pk": manager.pk
                }

                return result(self.API_NAME, object, None, 200)
            else:
                if DEBUG_ACT:
                    self.debug["notice"] = "No Auth"
                    self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")
                return result(self.API_NAME, None, None, 401)

        # [ case by ] STEP 2
        if input["step"] == 2:
            # validate param
            var_check_param = ["agent_token", "guid"]
            for i in range(len(var_check_param)):
                if not str(var_check_param[i]) in input:
                    if DEBUG_ACT:
                        self.debug["notice"] = "Required Parameter"
                        self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")
                    return result(self.API_NAME, None, None, 400)

            # validate info
            manager = db.session.query(Users).filter_by(pk=input["manager_pk"]).first()
            if manager is None:
                if DEBUG_ACT:
                    self.debug["notice"] = "No Manager"
                    self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")
                return result(self.API_NAME, None, None, 401)

            old_device = db.session.query(Devices).filter_by(member_fk=input["manager_pk"], guid=input["guid"]).first()
            # case already register
            if old_device is not None:
                return result(self.API_NAME, None, None, 200)
            # case must register
            else:
                return result(self.API_NAME, None, None, 404)

#-----------------------------------------------------------------------------------------------------------------------
# [ AGENT ] REST
#-----------------------------------------------------------------------------------------------------------------------
# AgentList
#    @GET : Agent List
#    @POST : Agent 등록
class AgentListAPI(Resource):
    def __init__(self):
        self.API_NAME = "AgentListAPI"
        self.API = "/lmm/api/v1.0/agent/devices"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        self.process_manager = ProcessManager()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_AGENT
        # Param --------------------------------------------------------------------------------------------------------
        self.parser.add_argument("agent_token", type=str, location="json")
        self.parser.add_argument("manager_pk", type=int, location="json")
        self.parser.add_argument("guid", type=str, location="json")
        self.parser.add_argument("alias", type=str, location="json")
        self.parser.add_argument("dev_type_id", type=int, location="json")
        self.parser.add_argument("dev_os_id", type=int, location="json")
        self.parser.add_argument("dev_ver", type=str, location="json")
        #---------------------------------------------------------------------------------------------------------------
        super(AgentListAPI, self).__init__()

    def post(self):
        input = self.parser.parse_args()

        device_group = db.session.query(DeviceGroups).filter_by(member_fk=input["manager_pk"]).first()
        if device_group is None:
            if DEBUG_ACT:
                self.debug["notice"] = "No Group"
                self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug("")
            return result(self.API_NAME, None, None, 401)

        new_device = Devices()
        new_device.guid = input["guid"]
        new_device.agent_token = self.token_manager.set_token(input["guid"])
        new_device.alias = input["alias"]
        new_device.dev_type_id = input["dev_type_id"]
        new_device.dev_os_id = input["dev_os_id"]
        new_device.dev_ver = input["dev_ver"]
        new_device.member_fk = input["manager_pk"]
        new_device.device_group_fk = device_group.pk

        db.session.add(new_device)
        db.session.commit()

        device = db.session.query(Devices).filter_by(guid=input["guid"]).first()
        if device is not None:
            var_this_device_os = self.process_manager.get_device_os(device.dev_os_id)

            if var_this_device_os == TYPE_WINDOWS:
                new_detailDev = DevWindows()
            elif var_this_device_os == TYPE_LINUX:
                new_detailDev = DevLinux()
            elif var_this_device_os == TYPE_OS_X:
                new_detailDev = DevOSX()
            elif var_this_device_os == TYPE_ANDROID:
                new_detailDev = DevAndroid()
            elif var_this_device_os == TYPE_IOS:
                new_detailDev = DevIOS()
            else:
                new_detailDev = DevWindows()

            new_detailDev.fan_speed_id = 0
            new_detailDev.network_type_id = 0
            new_detailDev.time_zone_id = 17
            new_detailDev.device_fk = device.pk

            db.session.add(new_detailDev)
            db.session.commit()

            object = {
                "agent_token": device.agent_token
            }
        return result(self.API_NAME, object, None, 200)

# Agent
#    @GET : Agent 정보
#    @PUT : Agent 정보 수정
#    @DEL : Agent 정보 삭제
class AgentAPI(Resource):
    def __init__(self):
        self.API_NAME = "AgentAPI"
        self.API = "/lmm/api/v1.0/agent/devices/"
        self.parser = reqparse.RequestParser()
        # Manager ------------------------------------------------------------------------------------------------------
        self.log_manager = LogManager()
        self.token_manager = TokenManager.instance()
        # Debug --------------------------------------------------------------------------------------------------------
        self.debug = {}
        if DEBUG_ACT:
            self.debug["notice"] = ""
            self.debug["ip"] = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
            self.debug["user"] = TYPE_AGENT
        # Process ------------------------------------------------------------------------------------------------------
        self.parser.add_argument("agent_token", type=str, location="json")
        self.validateToken = self.token_manager.validate_agent_token(self.parser.parse_args()["agent_token"])
        # Param --------------------------------------------------------------------------------------------------------
        #    Device - init
        self.parser.add_argument("alias", type=str, location="json")
        self.parser.add_argument("dev_ver", type=str, location="json")
        self.parser.add_argument("flag_agent_token_update", type=bool, location="json")
        #---------------------------------------------------------------------------------------------------------------
        super(AgentAPI, self).__init__()

    def post(self, guid):
        if self.validateToken is False:
            return result(self.API_NAME, None, None, 401)

        input = self.parser.parse_args()

        var_device_init_data = {}
        if input["alias"] is not None:
            var_device_init_data["alias"] = input["alias"]
        if input["dev_ver"] is not None:
            var_device_init_data["dev_ver"] = input["dev_ver"]
        if input["flag_agent_token_update"] is True:
            var_device_init_data["agent_token"] = self.token_manager.set_token(guid)

        db.session.query(Devices).filter_by(guid=guid).update(var_device_init_data)
        db.session.commit()

        if DEBUG_ACT:
            self.debug["notice"] = "Device init Data Update List"
            self.log_manager.logger(self.API_NAME, log_file["debug"], self.debug).debug(var_device_init_data)

        return result(self.API_NAME, var_device_init_data, None, 200)

## Api resource routing
api = Api(app)

# Server ---------------------------------------------------------------------------------------------------------------
api.add_resource(RootAPI, "/lmm/api/v1.0/")
# Common
api.add_resource(AuthAPI, "/lmm/api/v1.0/auth")
api.add_resource(AccessAPI, "/lmm/api/v1.0/access")
api.add_resource(ResetPWAPI, "/lmm/api/v1.0/resetPW")
api.add_resource(CheckAPI, "/lmm/api/v1.0/check")
api.add_resource(AutoCompleteAPI, "/lmm/api/v1.0/auto_complete")
api.add_resource(FileUploadAPI, "/lmm/api/v1.0/file_upload")
# Manager
api.add_resource(ManagerListAPI, "/lmm/api/v1.0/managers")
#api.add_resource(ManagerAPI, "/lmm/api/v1.0/managers/<int:pk>")
# User
api.add_resource(UserListAPI, "/lmm/api/v1.0/users")
api.add_resource(UserAPI, "/lmm/api/v1.0/users/<int:pk>")
# Devices
api.add_resource(DeviceListAPI, "/lmm/api/v1.0/devices")
api.add_resource(DeviceAPI, "/lmm/api/v1.0/devices/<int:pk>")
# Groups
api.add_resource(DeviceGroupListAPI, "/lmm/api/v1.0/device-groups")
api.add_resource(DeviceGroupAPI, "/lmm/api/v1.0/device-groups/<int:pk>")
api.add_resource(DeviceGroupConnListAPI, "/lmm/api/v1.0/conn-device-groups")
api.add_resource(DeviceGroupConnAPI, "/lmm/api/v1.0/conn-device-groups/<int:pk>")
# Corp
api.add_resource(CompanyListAPI, "/lmm/api/v1.0/company")
api.add_resource(CompanyAPI, "/lmm/api/v1.0/company/<int:pk>")
# History
api.add_resource(AuditHistoryListAPI, "/lmm/api/v1.0/history/audit")
#api.add_resource(AuditHistoryAPI, "/lmm/api/v1.0/history/audit/<int:pk>")
api.add_resource(RemoteHistoryListAPI, "/lmm/api/v1.0/history/remote")
#api.add_resource(RemoteHistoryAPI, "/lmm/api/v1.0/history/remote/<int:pk>")
api.add_resource(AlertHistoryListAPI, "/lmm/api/v1.0/history/alert")
#api.add_resource(AlertHistoryAPI, "/lmm/api/v1.0/history/alert/<int:pk>")
# Dashboard
api.add_resource(RemoteDashListAPI, "/lmm/api/v1.0/dash/remote")
#api.add_resource(RemoteDashAPI, "/lmm/api/v1.0/dash/remote/<int:pk>")
api.add_resource(RemoteChartListAPI, "/lmm/api/v1.0/chart/remote")
#api.add_resource(RemoteChartAPI, "/lmm/api/v1.0/chart/remote/<int:pk>")

# Agent ----------------------------------------------------------------------------------------------------------------
#    Agents
api.add_resource(AgentListAPI, "/lmm/api/v1.0/agent/devices")
api.add_resource(AgentAPI, "/lmm/api/v1.0/agent/devices/<string:guid>")
#    Auth
api.add_resource(AgentAuthAPI, "/lmm/api/v1.0/agent/auth")

# MAIN -----------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8088)