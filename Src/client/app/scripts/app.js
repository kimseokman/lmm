'use strict';

angular
    .module('lmmApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ngTable',
        'ui.bootstrap',
        'ui.select2',
        'xeditable',
        'pascalprecht.translate',
        'highcharts-ng',
        'angularFileUpload'
    ])
    .config(function ($routeProvider, $translateProvider) {
        $routeProvider
             .when('/intro', {
                 templateUrl: 'views/intro/intro.html',
                 controller: 'IntroCtrl'
             })
             .when('/intro/howItWorks', {
                 templateUrl: 'views/intro/howItWorks.html',
                 controller: 'HowItWorksCtrl'
             })
            .when('/intro/features', {
                templateUrl: 'views/intro/features.html',
                controller: 'FeaturesCtrl'
            })
            .when('/intro/price', {
                templateUrl: 'views/intro/price.html',
                controller: 'PriceCtrl'
            })
            .when('/intro/resources', {
                templateUrl: 'views/intro/resources.html',
                controller: 'ResourcesCtrl'
            })
            .when('/access/auth', {
                templateUrl: 'views/access/auth.html',
                controller: 'AuthCtrl'
            })
            .when('/access/register', {
                templateUrl: 'views/access/register.html',
                controller: 'RegisterCtrl'
            })
            .when('/access/forgot', {
                templateUrl: 'views/access/forgot.html',
                controller: 'ForgotCtrl'
            })
            .when('/access/login', {
                templateUrl: 'views/access/login.html',
                controller: 'LoginCtrl'
            })
            .when('/access/otp', {
                templateUrl: 'views/access/otp.html',
                controller: 'OtpCtrl'
            })
            .when('/access/logout', {
                templateUrl: 'views/access/logout.html',
                controller: 'LogoutCtrl'
            })
            .when('/main/devices/:deviceGroupPK', {
                templateUrl: 'views/main/devices/devices.html',
                controller: 'DevicesCtrl'
            })
            .when('/main/live/:deviceVer/:devicePK', {
                templateUrl: function(params){return 'views/main/live/'+params.deviceVer+'.html'},
                controller: 'LiveCtrl'
            })
            .when('/main/reports/:reportPageName/:detailPageName/:searchTag?', {
                templateUrl: function(params){
                    var targetTemplate = "";

                    if(params.detailPageName){
                        targetTemplate ='views/main/reports/' + params.reportPageName + '/' + params.detailPageName + '.html';
                    }else{
                        targetTemplate ='views/main/reports/' + params.reportPageName + '.html';
                    }
                    return targetTemplate;
                },
                controller: 'ReportsCtrl'
            })
            .when('/main/admin/:adminPageName/:detailPageName?', {
                templateUrl: function(params){
                    var targetTemplate = "";

                    if(params.detailPageName){
                        targetTemplate ='views/main/admin/' + params.adminPageName + '/' + params.detailPageName + '.html';
                    }else{
                        targetTemplate ='views/main/admin/' + params.adminPageName + '.html';
                    }

                    return targetTemplate;
                },
                controller: 'AdminCtrl'
            })
            .when('/main/help/:helpPageName', {
                templateUrl: function(params){return 'views/main/help/'+params.helpPageName+'.html'},
                controller: 'HelpCtrl'
            })
            .otherwise({
                redirectTo: '/intro'
            });

        $translateProvider.useStaticFilesLoader({
            prefix: 'l10n/',
            suffix: '.json'
        });

        $translateProvider.preferredLanguage('en_US');
    })
    .run(function($location, $rootScope, $route, Session, editableOptions){
        /**
         * Refresh without Loading
         */
        var original = $location.path;
        $location.path = function (path, reload) {
            if (reload === false) {
                var lastRoute = $route.current;
                var un = $rootScope.$on('$locationChangeSuccess', function () {
                    $route.current = lastRoute;
                    un();
                });
            }

            return original.apply($location, [path]);
        };

        $rootScope.$on('$locationChangeStart', function (event, next) {
            var token = Session.get('token');
            var passStep1 = Session.get('step1');
            var passStep2 = Session.get('step2');
            var useOTP = Session.get('useOTP');
            var passStep3 = Session.get('step3');
            var isLogin = Session.get('isLogin');

            var nextUri = ""+ next;

            //----------------------------------------------------------------------------------------------------------
            // force route
            //----------------------------------------------------------------------------------------------------------
            if(passStep1 == "Success" && passStep2 == "Success"){
                if(useOTP == "Use"){
                    if(passStep3 == "Success" && isLogin == "Success"){
                    }else{//login fail
                        if (nextUri.indexOf('main') != -1) {
                            $location.path('/access/login', true);
                        }
                    }
                }else if(useOTP == "None"){
                    if(isLogin == "Success"){

                    }else{//login fail
                        if (nextUri.indexOf('main') != -1) {
                            $location.path('/access/login', true);
                        }
                    }
                }
            }else{//login fail
                if (nextUri.indexOf('main') != -1) {
                    $location.path('/access/login', true);
                }
            }
            //----------------------------------------------------------------------------------------------------------
            // valide OTP
            //----------------------------------------------------------------------------------------------------------
            /*
            if(useOTP=='true'){
                if(authOTP=='false'){
                    if (next.templateUrl != 'views/access/otp.html') {
                        $location.path('/login/otp', true);
                    }
                }
            }
            */

            editableOptions.theme = 'bs3';
        });
    });
