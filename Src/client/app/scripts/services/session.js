/**
 * Created by ksm on 2015-02-06.
 */
'use strict';

angular.module('lmmApp')
    .provider('Session', function Session() {
        this.$get = function () {
            // access key list
            var access_key = {
                token: "token",        // token
                step1: "step1",        // step1
                step2: "step2",        // step2
                useOTP: "useOTP",     // useOTP
                step3: "step3",       // step3
                isLogin: "isLogin"   // isLogin
            };

            var live_status = {//live status
                device_pk: "current_device_pk",
                device_group_pk: "current_device_group_pk"
            };

            return {
                get: function (sessionKey) {
                    return sessionStorage[sessionKey];
                },
                set: function (sessionKey, sessionValue) {
                    sessionStorage[sessionKey] = sessionValue;
                },
                clear: function (sessionKey) {
                    sessionStorage.removeItem(sessionKey);
                },
                all_clear: function(){
                    sessionStorage.removeItem(access_key.token);
                    sessionStorage.removeItem(access_key.step1);
                    sessionStorage.removeItem(access_key.step2);
                    sessionStorage.removeItem(access_key.useOTP);
                    sessionStorage.removeItem(access_key.step3);
                    sessionStorage.removeItem(access_key.isLogin);
                    sessionStorage.removeItem(live_status.device_pk);
                    sessionStorage.removeItem(live_status.device_group_pk);
                }
            };
        }
    });
