/**
 * Created by ksm on 2015-03-23.
 */
'use strict';

angular.module('lmmApp')
    .constant('COM', {
        uri: {
            loading: "views/loading.html",
            header: "views/header.html",
            footer: "views/footer.html",
            device_side: "views/main/devices/side.html",
            live_side: "views/main/live/side.html",
            report_side: "views/main/reports/side.html",
            admin_side: "views/main/admin/side.html",
            help_side: "views/main/help/side.html"
        },
        self_pk: 0,
        setting: {
            on_off: {
                statuses:[
                    {value: true, text: 'ON'},
                    {value: false, text: 'OFF'}
                ]
            },
            active: {
                statuses:[
                    {value: true, text: 'Active'},
                    {value: false, text: 'Suspend'}
                ]
            },
            date_fmt: {
                statuses: [
                    {value: 0, text: "YYYY/MM/DD"},
                    {value: 1, text: "MM/DD/YYYY"},
                    {value: 2, text: "DD/MM/YYYY"}
                ]
            },
            time_fmt: {
                statuses: [
                    {value: 0, text: "hh:mm:ss"}
                ]
            },
            time_zone: {
                statuses: [
                    {value: 1, data: -((60*60*12)*1000), simple_text: 'UTC-12', text: 'UTC-12 (BIT)'},
                    {value: 2, data: -((60*60*11)*1000), simple_text: 'UTC-11', text: 'UTC-11 (NUT, SST)'},
                    {value: 3, data: -((60*60*10)*1000), simple_text: 'UTC-10',  text: 'UTC-10 (CKT, HAST, HST, TAHT)'},
                    {value: 4, data: -(((60*60*9) + (60*30))*1000), simple_text: 'UTC-09:30',  text: 'UTC-09:30 (MART, MIT)'},
                    {value: 5, data: -((60*60*9)*1000), simple_text: 'UTC-09',  text: 'UTC-09 (AKST, GAMT, GIT, HADT)'},
                    {value: 6, data: -((60*60*8)*1000), simple_text: 'UTC-08',  text: 'UTC-08 (AKDT, CIST, PST)'},
                    {value: 7, data: -((60*60*7)*1000), simple_text: 'UTC-07',  text: 'UTC-07 (MST, PDT)'},
                    {value: 8, data: -((60*60*6)*1000), simple_text: 'UTC-06',  text: 'UTC-06 (CST, EAST, GALT, MDT)'},
                    {value: 9, data: -((60*60*5)*1000), simple_text: 'UTC-05',  text: 'UTC-05 (CDT, COT, CST, EASST, ECT, EST, PET)'},
                    {value: 10, data: -(((60*60*4) + (60*30))*1000), simple_text: 'UTC-04:30',  text: 'UTC-04:30 (VET)'},
                    {value: 11, data: -((60*60*4)*1000), simple_text: 'UTC-04',  text: 'UTC-04 (AMT, AST, BOT, CDT, CLT, COST, ECT, EDT, FKT, GYT, PYT)'},
                    {value: 12, data: -(((60*60*3) + (60*30))*1000), simple_text: 'UTC-03:30',  text: 'UTC-03:30 (NST, NT)'},
                    {value: 13, data: -((60*60*3)*1000), simple_text: 'UTC-03',  text: 'UTC-03 (ADT, AMST, ART, BRT, CLST, FKST, GFT, PMST, PYST, ROTT, SRT, UYT)'},
                    {value: 14, data: -(((60*60*2) + (60*30))*1000), simple_text: 'UTC-02:30',  text: 'UTC-02:30 (NDT)'},
                    {value: 15, data: -((60*60*2)*1000), simple_text: 'UTC-02',  text: 'UTC-02 (FNT, GST, PMDT, UYST)'},
                    {value: 16, data: -((60*60*1)*1000), simple_text: 'UTC-01',  text: 'UTC-01 (AZOST, CVT, EGT)'},
                    {value: 17, data: ((60*60*0)*1000), simple_text: 'UTC',  text: 'UTC (GMT, UCT, WET, Z, EGST)'},
                    {value: 18, data: ((60*60*1)*1000), simple_text: 'UTC+01',  text: 'UTC+01 (BST, CET, DFT, IST, MET, WAT, WEDT, WEST)'},
                    {value: 19, data: ((60*60*2)*1000), simple_text: 'UTC+02',  text: 'UTC+02 (CAT, CEDT, CEST, EET, HAEC, IST, MEST, SAST, WAST)'},
                    {value: 20, data: ((60*60*3)*1000), simple_text: 'UTC+03',  text: 'UTC+03 (AST, EAT, EEDT, EEST, FET, IDT, IOT, SYOT)'},
                    {value: 21, data: ((60*60*3) + (60*30)*1000), simple_text: 'UTC+03:30',  text: 'UTC+03:30 (IRST)'},
                    {value: 22, data: ((60*60*4)*1000), simple_text: 'UTC+04',  text: 'UTC+04 (AMT, AZT, GET, GST, MSK, MUT, RET, SAMT, SCT, VOLT)'},
                    {value: 23, data: ((60*60*4) + (60*30)*1000), simple_text: 'UTC+04:30',  text: 'UTC+04:30 (AFT, IRDT)'},
                    {value: 24, data: ((60*60*5)*1000), simple_text: 'UTC+05',  text: 'UTC+05 (AMST, HMT, MAWT, MVT, ORAT, PKT, TFT, TJT, TMT, UZT)'},
                    {value: 25, data: ((60*60*5) + (60*30)*1000), simple_text: 'UTC+05:30',  text: 'UTC+05:30 (IST, SLST)'},
                    {value: 26, data: ((60*60*5) + (60*45)*1000), simple_text: 'UTC+05:45',  text: 'UTC+05:45 (NPT)'},
                    {value: 27, data: ((60*60*6)*1000), simple_text: 'UTC+06:00',  text: 'UTC+06:00 (BIOT, BST, BTT, KGT, VOST, YEKT)'},
                    {value: 28, data: ((60*60*6) + (60*30)*1000), simple_text: 'UTC+06:30',  text: 'UTC+06:30 (CCT, MMT, MST)'},
                    {value: 29, data: ((60*60*7)*1000), simple_text: 'UTC+07',  text: 'UTC+07 (CXT, DAVT, HOVT, ICT, KRAT, OMST, THA, WIT)'},
                    {value: 30, data: ((60*60*8)*1000), simple_text: 'UTC+08',  text: 'UTC+08 (ACT, AWST, BDT, CHOT, CIT, CST, CT, HKT, MST, MYT, PST, SGT, SST, ULAT, WST)'},
                    {value: 31, data: ((60*60*8) + (60*45)*1000), simple_text: 'UTC+08:45',  text: 'UTC+08:45 (CWST)'},
                    {value: 32, data: ((60*60*9)*1000), simple_text: 'UTC+09',  text: 'UTC+09 (AWDT, EIT, IRKT, JST, KST, TLT)'},
                    {value: 33, data: ((60*60*9) + (60*30)*1000), simple_text: 'UTC+09:30',  text: 'UTC+09:30 (ACST, CST)'},
                    {value: 34, data: ((60*60*10)*1000), simple_text: 'UTC+10',  text: 'UTC+10 (AEST, CHST, CHUT, DDUT, EST, PGT, VLAT, YAKT)'},
                    {value: 35, data: ((60*60*10) + (60*30)*1000), simple_text: 'UTC+10:30',  text: 'UTC+10:30 (ACDT, CST, LHST)'},
                    {value: 36, data: ((60*60*11)*1000), simple_text: 'UTC+11',  text: 'UTC+11 (AEDT, KOST, LHST, MIST, NCT, PONT, SAKT, SBT)'},
                    {value: 37, data: ((60*60*11) + (60*30)*1000), simple_text: 'UTC+11:30',  text: 'UTC+11:30 (NFT)'},
                    {value: 38, data: ((60*60*12)*1000), simple_text: 'UTC+12',  text: 'UTC+12 (FJT, GILT, MAGT, MHT, NZST, PETT, TVT, WAKT)'},
                    {value: 39, data: ((60*60*12) + (60*45)*1000), simple_text: 'UTC+12:45',  text: 'UTC+12:45 (CHAST)'},
                    {value: 40, data: ((60*60*13)*1000), simple_text: 'UTC+13',  text: 'UTC+13 (NZDT, PHOT, TKT, TOT)'},
                    {value: 41, data: ((60*60*13) + (60*45)*1000), simple_text: 'UTC+13:45',  text: 'UTC+13:45 (CHADT)'},
                    {value: 42, data: ((60*60*14)*1000), simple_text: 'UTC+14',  text: 'UTC+14 (LINT)'}
                ]
            },
            audit_act_name: {
                statuses: [
                    {value: 0, text: "UNKNOWN"},
                    {value: 1, text: "Remote Control"},
                    {value: 2, text: "File Manager"},
                    {value: 10, text: "Device Info"},
                    {value: 11, text: "Device Info - Alias"},
                    {value: 12, text: "Device Info - Memo"},
                    {value: 20, text: "Device Setting"},
                    {value: 21, text: "Device Setting - Group"}
                ]
            },
            audit_act_type: {
                statuses: [
                    {value: 0, text: "UNKNOWN"},
                    {value: 1, text: "Start"},
                    {value: 2, text: "End"},
                    {value: 3, text: "Create"},
                    {value: 4, text: "Touch"},
                    {value: 5, text: "Delete"}
                ]
            },
            user_type: {
                statuses: [
                    {value: 0, text: "Guest"},
                    {value: 1, text: "Member"},
                    {value: 2, text: "Manager"},
                    {value: 3, text: "Admin"},
                    {value: 4, text: "Super"}
                ]
            },
            user_auth_type: {
                statuses: [
                    {value: 1, text: "Device 정보 수정 권한"},
                    {value: 2, text: "Device 컨트롤 권한"},
                    {value: 4, text: "Device Report 볼수 있는 권한"}
                ]
            },
            device: {
                type_statuses: [
                    {value: 1, text: "Device 정보 수정 권한"},
                    {value: 2, text: "Device 컨트롤 권한"},
                    {value: 4, text: "Device Report 볼수 있는 권한"}
                ],
                os_statuses: [
                    {value: 0, text: "windows"},//UNKNOWN
                    {value: 1, text: "windows"},
                    {value: 2, text: "linux"},
                    {value: 3, text: "os_x"},
                    {value: 4, text: "android"},
                    {value: 5, text: "ios"}
                ]
            }
        }
    });