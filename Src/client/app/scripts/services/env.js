/**
 * Created by ksm on 2015-01-28.
 */
'use strict';

angular.module('lmmApp')
    .constant('ENV', {
        // local build -------------------------------------------------------------------------------------------------
        host: 'http://192.168.1.42:8088',
        liveImgUri: "./images/test/live/",
        qrcodeImgUri: "./images/test/qr_code/",
        uploadImgUri: "./images/test/upload/"

        // test server build  ------------------------------------------------------------------------------------------
        //host: "http://linkmemine.com",
        //liveImgUri: "/live_view/",
        //qrcodeImgUri: "/qr_code/",
        //uploadImgUri: "/upload/"

        // local & test server build  ----------------------------------------------------------------------------------

    });