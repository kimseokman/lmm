/**
 * Created by ksm on 2015-03-16.
 */
'use strict';

angular.module('lmmApp')
    .factory('SharedService', function ($rootScope) {
        var sharedService = {};
        // communication data list -------------------------------------------------------------------------------------
        sharedService.user = null;
        sharedService.str = null;
        sharedService.logo_src = null;
        // data: user --------------------------------------------------------------------------------------------------
        sharedService.prepForUserBroadcast = function(user){
            this.user = user;
            this.broadcastRoot("USER");
        };
        sharedService.prepForConfigStringBroadcast = function(str){
            this.str = str;
            this.broadcastRoot("CONFIG");
        };
        sharedService.prepForLogoBroadcast = function(logo_src){
            this.logo_src = logo_src;
            this.broadcastRoot("LOGO");
        };
        // communication -----------------------------------------------------------------------------------------------
        sharedService.broadcastRoot = function(type){
            $rootScope.$broadcast(type);
        };

        return sharedService;
    });