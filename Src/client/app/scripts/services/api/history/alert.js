/**
 * Created by ksm on 2015-04-03.
 */
'use strict';

angular.module('lmmApp')
    .factory('AlertHistory', function ($resource, ENV) {
        return $resource(ENV.host + '/lmm/api/v1.0/history/alert/:pk', null, {'update': {method: 'PUT'}});
    });