/**
 * Created by ksm on 2015-03-23.
 */
'use strict';

angular.module('lmmApp')
    .factory('AuditHistory', function ($resource, ENV) {
        return $resource(ENV.host + '/lmm/api/v1.0/history/audit/:pk', null, {'update': {method: 'PUT'}});
    });
