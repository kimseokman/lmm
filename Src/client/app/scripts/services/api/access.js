/**
 * Created by ksm on 2015-02-05.
 */
'use strict';

angular.module('lmmApp')
    .factory('Access', function ($resource, $location, $http, ENV, Session, Modal, Auth) {
        return {
            auth: function(user){
                $http({
                    method: 'post',
                    url: ENV.host + '/lmm/api/v1.0/access',
                    headers: {'Content-Type': 'application/json'},
                    data: user
                })
                .success(function(data){
                    var auth = data.objects;
                    var resMsg = "";
                    Session.set('isLogin', auth.isLogin);
                    Session.set('step1', auth.step1);

                    if(auth.step1 == "Fail"){
                        var loginInfo = {};
                        loginInfo['step'] = "1";
                        loginInfo['status'] = "Fail";
                        loginInfo['msg'] = "Login Fail";

                        Modal.open(
                            "views/modal/dialogNotify_login.html",
                            "DialogNotifyLoginCtrl",
                            "lg",
                            {
                                info: function(){
                                    return loginInfo;
                                }
                            }
                        );
                    }else if(auth.step1 == "Success"){//pass 1 step
                        Session.set('step2', auth.step2);


                        if(auth.step2 == "Fail"){
                            var loginInfo = {};
                            loginInfo['step'] = "2";
                            loginInfo['mail'] = auth.mail;
                            loginInfo['status'] = "Fail";
                            loginInfo['msg'] = "Please mail verification.";

                            Modal.open(
                                "views/modal/dialogNotify_login.html",
                                "DialogNotifyLoginCtrl",
                                "lg",
                                {
                                    info: function(){
                                        return loginInfo;
                                    }
                                },
                                function(sendMailRes){
                                    if(sendMailRes.sendMailFlag){
                                        var arg = {};
                                        arg['type'] = sendMailRes.type;
                                        arg['mail'] = sendMailRes.mail;

                                        Auth.save(arg, function(data){//mail auth
                                            if(data.status == 200){
                                                alert("Send Mail to complete!");
                                            }
                                        });
                                    }
                                }
                            );
                        }else if(auth.step2 == "Success"){
                            Session.set('token', auth.token);
                            Session.set('useOTP', auth.useOTP);

                            if(auth.useOTP == "Use"){
                                Session.set('step3', auth.step3);
                                $location.path('/access/otp', true);
                            }else if(auth.useOTP == "None"){
                                $location.path('/main/devices/all', true);
                            }
                        }
                    }
                })
            },
            auth_otp: function(user){
                var token = Session.get('token');
                $http.defaults.headers.get = {token: token};
                $http.defaults.headers.put["token"] = token;
                $http.defaults.headers.post["token"] = token;
                $http.defaults.headers.common["token"] = token;

                $http({
                    method: 'post',
                    url: ENV.host + '/lmm/api/v1.0/access',
                    headers: {'Content-Type': 'application/json'},
                    data: user
                })
                .success(function(data){
                    var auth = data.objects;
                    Session.set('isLogin', auth.isLogin);
                    Session.set('step3', auth.step3);

                    if(auth.step3 == "Fail"){
                        alert('OTP Failed');
                    }else if(auth.step3 == "Success"){
                        $location.path('/main/devices/all', true);
                    }
                })
            },
            isAuthenticated: function(){
                var passStep1 = Session.get('step1');
                var passStep2 = Session.get('step2');
                var useOTP = Session.get('useOTP');
                var passStep3 = Session.get('step3');
                var isLogin = Session.get('isLogin');

                var isAuth = false;

                if(passStep1 == "Success" && passStep2 == "Success"){
                    if(useOTP == "Use"){
                        if(passStep3 == "Success" && isLogin == "Success"){
                            isAuth = true;
                        }else{
                            isAuth = false;
                        }
                    }else if(useOTP == "None"){
                        if(isLogin == "Success"){
                            isAuth = true;
                        }else{
                            isAuth = false;
                        }
                    }
                }

                return isAuth;
            }
        };
    });