/**
 * Created by ksm on 2015-02-10.
 */
'use strict';

angular.module('lmmApp')
    .factory('Check', function ($resource, ENV) {
        return $resource(ENV.host + '/lmm/api/v1.0/check', null, {'update': {method: 'PUT'}});
    });

