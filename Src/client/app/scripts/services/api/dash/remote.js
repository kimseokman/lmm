/**
 * Created by ksm on 2015-04-21.
 */
'use strict';

angular.module('lmmApp')
    .factory('RemoteDash', function ($resource, ENV) {
        return $resource(ENV.host + '/lmm/api/v1.0/dash/remote/:pk', null, {'update': {method: 'PUT'}});
    });