/**
 * Created by ksm on 2015-02-11.
 */

'use strict';

angular.module('lmmApp')
    .factory('Auth', function ($resource, ENV) {
        return $resource(ENV.host + '/lmm/api/v1.0/auth', null, {'update': {method: 'PUT'}});
    });