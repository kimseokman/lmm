/**
 * Created by ksm on 2015-02-13.
 */
'use strict';

angular.module('lmmApp')
    .factory('Users', function ($resource, ENV) {
        return $resource(ENV.host + '/lmm/api/v1.0/users/:pk', null, {'update': {method: 'PUT'}});
    });

