/**
 * Created by ksm on 2015-04-22.
 */
'use strict';

angular.module('lmmApp')
    .factory('DeviceGroupsConn', function ($resource, ENV) {
        return $resource(ENV.host + '/lmm/api/v1.0/conn-device-groups/:pk', null, {'update': {method: 'PUT'}});
    });