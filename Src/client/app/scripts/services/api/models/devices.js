/**
 * Created by ksm on 2015-02-23.
 */
'use strict';

angular.module('lmmApp')
    .factory('Devices', function ($resource, ENV) {
        return $resource(ENV.host + '/lmm/api/v1.0/devices/:pk', null, {'update': {method: 'PUT'}});
    });
