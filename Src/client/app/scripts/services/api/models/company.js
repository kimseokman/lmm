/**
 * Created by ksm on 2015-03-17.
 */
'use strict';

angular.module('lmmApp')
    .factory('Company', function ($resource, ENV) {
        return $resource(ENV.host + '/lmm/api/v1.0/company/:pk', null, {'update': {method: 'PUT'}});
    });


