/**
 * Created by ksm on 2015-02-13.
 */
'use strict';

angular.module('lmmApp')
    .factory('DeviceGroups', function ($resource, ENV) {
        return $resource(ENV.host + '/lmm/api/v1.0/device-groups/:pk', null, {'update': {method: 'PUT'}});
    });