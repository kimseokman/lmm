/**
 * Created by ksm on 2015-02-11.
 */
'use strict';

angular.module('lmmApp')
    .factory('ResetPW', function ($resource, ENV) {
        return $resource(ENV.host + '/lmm/api/v1.0/resetPW', null, {'update': {method: 'PUT'}});
    });
