/**
 * Created by ksm on 2015-04-23.
 */
'use strict';

angular.module('lmmApp')
    .factory('RemoteChart', function ($resource, ENV) {
        return $resource(ENV.host + '/lmm/api/v1.0/chart/remote/:pk', null, {'update': {method: 'PUT'}});
    });