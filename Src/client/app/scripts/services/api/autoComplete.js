/**
 * Created by ksm on 2015-02-27.
 */

'use strict';

angular.module('lmmApp')
    .factory('AutoComplete', function ($resource, ENV) {
        return $resource(ENV.host + '/lmm/api/v1.0/auto_complete');
    });
