/**
 * Created by ksm on 2015-02-25.
 */
'use strict';

angular.module('lmmApp')
    .factory('Modal', function ($modal) {
        return {
            open: function (templateUrl, controller, size, resolve, callback) {
                var modalInstance = $modal.open({
                    templateUrl: templateUrl,
                    controller: controller,
                    size: size,//sm or lg
                    resolve: resolve
                });

                modalInstance.result.then(callback);
            }
        };
    });