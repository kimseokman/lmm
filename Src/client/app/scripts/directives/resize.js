/**
 * Created by ksm on 2015-02-16.
 */

'use strict';

angular.module('lmmApp')
    .directive('resize', function($window){
        return {
            restrict: 'A',
            link: function(scope, elm, attrs){
                var w = angular.element($window);
                var header = Number(attrs.header);
                var footer = Number(attrs.footer);
                var obstacle = Number(attrs.obstacle);
                var whiteSpaceH = header + footer + obstacle;

                scope.getWindowDimensions = function () {
                    return {
                        'h': w.height()
                    };
                };

                scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
                    scope.contentHeight = newValue.h - whiteSpaceH;

                    scope.autoStyleHeight = function () {
                        return {
                            'height': scope.contentHeight + 'px'
                        };
                    };

                    scope.autoStyleMinHeight = function () {
                        return {
                            'min-height': scope.contentHeight + 'px'
                        };
                    };

                    scope.isScroll = function (targetContentClassName) {
                        var isScrollFlag = false;
                        var content = elm.find('.' + targetContentClassName);

                        if( content.height() > scope.contentHeight ){
                            isScrollFlag = true;
                        }

                        return isScrollFlag;
                    };

                }, true);

                w.bind('resize', function () {
                    scope.$apply();
                });
            }
        }
    });