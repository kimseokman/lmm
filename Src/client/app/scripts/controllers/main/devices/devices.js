/**
 * Created by ksm on 2015-02-09.
 */
'use strict';

angular.module('lmmApp')
    .controller('DevicesCtrl', function ($scope, $http, $route, $location, $rootScope, $cookieStore, $timeout, $interval, ENV, DeviceGroups, Devices, ngTableParams, Modal, AutoComplete, SharedService) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-devices';

        //inherit data -------------------------------------------------------------------------------------------------
        $scope.$parent.updateInformation();
        $scope.$parent.updatePath();

        $scope.$on("USER", function(){
            $scope.user = SharedService.user;
        });
        $scope.$on("CONFIG", function(){
            $scope.config_str = SharedService.str;
        });
        $scope.$on("LOGO", function(){
            $scope.logo_img_uri = SharedService.logo_src;
        });

        //default Setting ----------------------------------------------------------------------------------------------
        $scope.devicesPage = {//devices status
            view_mode: $cookieStore.get("devices_view_mode") || false  //view mode flag - false:pagination, true:noraml
        };
        $scope.deviceGroupPK = $route.current.params.deviceGroupPK || 'all';
        $scope.deviceImgSize = 1;
        $scope.groups = [//side groups data scaffold
            {
                pk: "Empty", name: "Empty", flag_df_group: true, flag_confirm: false//default group set
            }
        ];

        $scope.deviceTableHeader = [
            {name: "No.", className: "no", sortable: false},
            {name: "Group", className: "group", sortable: true, field: "device_group_fk"},
            //{name: "Type", className: "type", sortable: true, field: "dev_type_id"},
            {name: "OS", className: "os", sortable: true, field: "dev_os_id"},
            {name: "Alias", className: "alias", sortable: true, field: "alias"},
            {name: "EMPTY", className: "act1", sortable: false},
            {name: "EMPTY", className: "act2", sortable: false},
            {name: "EMPTY", className: "act3", sortable: false},
            {name: "EMPTY", className: "act4", sortable: false},
            {name: "EMPTY", className: "act5", sortable: false},
            {name: "EMPTY", className: "act6", sortable: false},
            {name: "EMPTY", className: "act7", sortable: false}
        ];

        $scope.deviceTableCol = $scope.deviceTableHeader.length;//device table head col count
        $scope.deviceTableLimits = [
            {value: 10},
            {value: 25},
            {value: 50},
            {value: 100}
        ];
        $scope.deviceTableLimit = $scope.deviceTableLimits[0];//set default limit line

        $scope.deviceTableFilters = [
            {
                field: 'alias', placeholder: 'Filter Alias', resource: AutoComplete, resourceType: "devices"
            },
            {
                field: 'dev_type_id', placeholder: 'Filter Type',
                data: [
                    {label: "Type", id: 'all', text: 'All'},
                    {label: "Type", id: 0, text: 'UNKNOWN'},
                    {label: "Type", id: 1, text: 'Desktop'},
                    {label: "Type", id: 2, text: 'Tablet'},
                    {label: "Type", id: 3, text: 'Mobile'},
                    {label: "Type", id: 4, text: 'Laptop'}
                ]
            },
            {
                field: 'dev_os_id', placeholder: 'Filter OS',
                data: [
                    {label: "OS", id: 'all', text: 'All'},
                    {label: "OS", id: 0, text: 'UNKNOWN'},
                    {label: "OS", id: 1, text: 'Windows'},
                    {label: "OS", id: 2, text: 'Linux'},
                    {label: "OS", id: 3, text: 'OS X'},
                    {label: "OS", id: 4, text: 'Android'},
                    {label: "OS", id: 5, text: 'IOS'}
                ]
            }
        ];
        $scope.deviceTableFilterIndex = 0;//default set filter index

        $scope.deviceTableFilterSelectors = [
            {index: 0, name: 'Alias'},
            {index: 1, name: 'Type'},
            {index: 2, name: 'OS'}
        ];

        $scope.deviceTableFilterSelector = $scope.deviceTableFilterSelectors[$scope.deviceTableFilterIndex];//default set filter selector
        $scope.deviceTableFilterSelected = [
            null,//index 0
            null,//index 1
            null//index 2
        ];

        //default Data -------------------------------------------------------------------------------------------------
        var deviceGroupsArg = {};
        DeviceGroups.get(deviceGroupsArg, function(data) {//get device group
            if (data.status == 200) {
                $scope.groups = data.objects;
            }
        });

        $scope.$watch('deviceTableFilterSelected', function (newValue, oldValue) {  //get device Table & watch - device Table
            if (($scope.deviceTable == undefined) || newValue != oldValue) {
                if ($scope.deviceTable == undefined) {
                    $scope.deviceTable = new ngTableParams({//get device
                        page: 1,//show first page
                        count: $scope.deviceTableLimit.value,//count per page
                        sorting: {}//initial sorting
                    }, {
                        total: 0,// length of data
                        getData: function ($defer, params) {
                            var deviceArg = {};
                            deviceArg['limit'] = params.count();
                            deviceArg['offset'] = (params.page() - 1) * deviceArg['limit'];
                            deviceArg['device_group_pk'] = $scope.deviceGroupPK;
                            deviceArg['search_mode'] = ($scope.devicesPage.view_mode) ? 'normal' : 'pagination';
                            var sorting_order = 'desc';//default order
                            var sorting_by = 'alias';//default by

                            var sorting = params.sorting();
                            for (var k in sorting) {
                                var v = sorting[k];

                                sorting_order = v;
                                sorting_by = k;
                            }

                            for (var i in $scope.deviceTableFilterSelected) {
                                if ($scope.deviceTableFilterSelected[i]) {
                                    deviceArg[$scope.deviceTableFilters[i].field] = $scope.deviceTableFilterSelected[i].id;
                                }
                            }

                            deviceArg['order'] = sorting_order;
                            deviceArg['by'] = sorting_by;

                            Devices.get(deviceArg, function(data){
                                $timeout(function () {
                                    $scope.devices = data.objects;
                                    angular.forEach($scope.devices, function(v, k){
                                        $scope.devices[k].live_img_src = null;
                                    });
                                    $scope.device_image_load();
                                    params.total(data.meta.total_count);
                                    $defer.resolve($scope.devices);
                                });
                            });
                        }
                    });
                } else {
                    $scope.deviceTable.reload();
                }
            }
        }, true);

        //functions - Side ---------------------------------------------------------------------------------------------
        $scope.onClickDeviceGroup = function(isRootFlag, deviceGroupPK){//click group
            $scope.deviceGroupPK = (isRootFlag == 'all') ? 'all' : deviceGroupPK;

            var change_uri = '/main/devices/' + $scope.deviceGroupPK;
            var full_uri = $location.absUrl();
            var uri_split = full_uri.split('#');
            var current_uri = uri_split[1];

            if($scope.deviceGroupPK != 'all'){
                var deviceGroupsArg = {};
                deviceGroupsArg["flag_confirm"] = true;
                DeviceGroups.update({pk: $scope.deviceGroupPK}, deviceGroupsArg, function(){
                    angular.forEach($scope.groups, function(v){//update DeviceGroups
                        if(v.pk == $scope.deviceGroupPK ){
                            v.flag_confirm = true;
                        }
                    });
                });
            }

            if(current_uri != change_uri)
                $location.path(change_uri, false);
        };

        $scope.onActiveDeviceGroup = function(isRootFlag, deviceGroupPK){//active group
            $scope.deviceGroupTag = (isRootFlag == 'all') ? 'all' : deviceGroupPK;

            var full_uri = $location.absUrl();
            var uri_split = full_uri.split('#');
            var current_uri = uri_split[1];
            var current_flag = false;

            if(current_uri.indexOf($scope.deviceGroupTag) != -1){
                current_flag = true;
            }

            return current_flag;
        };

        //functions - Content ------------------------------------------------------------------------------------------
        $scope.setFilterIndex = function(index){
            $scope.deviceTableFilterIndex = index;
        };

        $scope.getFilterOptions = function (filter) {//get filter options
            var options = {
                placeholder: filter.placeholder,
                allowClear: true
            };

            if (filter.data) {
                options['minimumResultsForSearch'] = -1;
                options['data'] = filter.data;
            }
            else if (filter.resource) {
                options['minimumInputLength'] = 2;
                options['initSelection'] = function(el, fn) {};
                options['ajax'] = {
                    quietMillis: 500,
                    data: function (term, page) {
                        return {
                            field: filter.field,
                            type: filter.resourceType,
                            text: term
                        };
                    },
                    transport: function (queryParams) {
                        return filter.resource.query(queryParams.data).$promise.then(queryParams.success);
                    },
                    results: function (data, page, query) {
                        return { results: data }
                    }
                };
            }

            return options;
        };

        $scope.getDeviceTypeIcon = function(type_id){// [ disabled ] get type icon
            var deviceTypeIcon = '';
            switch(type_id){
                case 0://UNKNOWN
                    deviceTypeIcon = "";
                    break;
                case 1://Desktop
                    deviceTypeIcon = "<i class='fa fa-desktop'></i>";
                    break;
                case 1://Tablet
                    deviceTypeIcon = "<i class='fa fa-tablet'></i>";
                    break;
                case 3://Mobile
                    deviceTypeIcon = "<i class='fa fa-mobile'></i>";
                    break;
                case 4://Laptop
                    deviceTypeIcon = "<i class='fa fa-laptop'></i>";
                    break;
            }

            return deviceTypeIcon;
        };

        $scope.getDeviceOSIcon = function(os_id){//get os icon
            var deviceOSIcon = '';

            switch(os_id){
                case 0://UNKNOWN
                    deviceOSIcon = "<span class='brand-NA'>UNKNOWN</span>";
                    break;
                case 1://Windows
                    deviceOSIcon = "<i class='fa fa-windows'></i>";
                    break;
                case 2://Linux
                    deviceOSIcon = "<i class='fa fa-linux'></i>";
                    break;
                case 3://OS_X
                    deviceOSIcon = "<i class='fa fa-apple'></i>";
                    break;
                case 4://Android
                    deviceOSIcon = "<i class='fa fa-android'></i>";
                    break;
                case 5://IOS
                    deviceOSIcon = "<i class='fa fa-apple'></i>";
                    break;
            }

            return deviceOSIcon;
        };

        $scope.deviceImgStyle = function(){
            var baseWidth = 107;
            var baseHeight = 80;

            return {
                width: (baseWidth * $scope.deviceImgSize) + "px",
                height: (baseHeight * $scope.deviceImgSize) + "px"
            }
        };

        $scope.devicesCookieSync = function(){
            if($cookieStore.get("devices_view_mode") == undefined){//window controll resource
                $scope.devicesPage.view_mode = false;
                $cookieStore.put("devices_view_mode", false);
            }else{
                $scope.devicesPage.view_mode = $cookieStore.get("devices_view_mode");
            }
        };

        $scope.setSwitchMode = function(mode_flag){
            $cookieStore.put("devices_view_mode", mode_flag);
            $scope.devicesCookieSync();
        };

        $scope.updateDeviceGroupByDB = function(field, data, pk){//update
            var updateDeviceArg = {};
            updateDeviceArg[field] = data;

            Devices.update({pk: pk}, updateDeviceArg, function(data){
                if(data.status == 200){
                    $timeout(function(){
                        $scope.deviceTable.reload();
                    }, 500);
                }
            });
        };

        //functions - If -----------------------------------------------------------------------------------------------
        $scope.if_showGroup = function(item){//if show default group
            var show_flag = false;

            if(item.pk != 'EMPTY'){
                show_flag = true;
            }else{
                show_flag = false;
            }

            return show_flag;
        };

        $scope.if_showDeviceContentMode = function(flag){//if show change mode
            var show_flag = false;

            if(flag){
                show_flag = true;
            }else{
                show_flag = false;
            }

            return show_flag;
        };

        $scope.if_showDeviceData = function(pk){//if show device table
            var show_flag = true;

            if(pk != "EMPTY"){
                show_flag = true;
            }else{
                show_flag = false;
            }

            return show_flag;
        };

        $scope.if_showBtns = function(btn_type){//if show - menu
            var showFlag = false;

            if(btn_type == 'Add Group'){
                if($scope.$parent.userType == "Manager"){
                    showFlag = true;
                }
            }else if(btn_type == 'Group Setting'){
                if($scope.deviceGroupPK != "all"){
                    showFlag = true;
                }
            }

            return showFlag;
        };

        //functions - Watch --------------------------------------------------------------------------------------------
        $scope.$watch('devicesPage.view_mode', function(newValue, oldValue){
            if($scope.deviceTable != undefined){//get device info reload
                $timeout(function(){
                    $scope.deviceTable.reload();
                }, 500);
            }
        });

        $scope.$watch('deviceGroupPK', function(newValue, oldValue){//watch - $scope.deviceGroupPK
            $scope.deviceGroupPK = newValue;

            var deviceGroupsArg = {};
            DeviceGroups.get(deviceGroupsArg, function(data) {//get device group info reload
                if (data.status == 200) {
                    $scope.groups = data.objects;
                }
            });

            if($scope.deviceTable != undefined){//get device info reload
                $timeout(function(){
                    $scope.deviceTable.reload();
                }, 500);
            }

            $scope.$parent.updatePath();
        });

        var device_image_interval_promise;
        $scope.device_image_load = function(){
            var expander = ".png";
            angular.forEach($scope.devices, function(v, k){//first img set
                $scope.devices[k].live_img_src = ENV.liveImgUri + $scope.devices[k].guid + expander; //img init
            });

            if($scope.user.flag_img_refresh){
                var delay = $scope.user.img_refresh_interval;

                angular.forEach($scope.devices, function(v, k){//first img set
                    $scope.devices[k].live_img_src = ENV.liveImgUri + $scope.devices[k].guid + expander + "?cache=" + new Date().getTime();
                });

                if ( angular.isDefined(device_image_interval_promise) ) return;

                device_image_interval_promise = $interval(function() {
                    if(delay <= 0){
                        delay = $scope.user.img_refresh_interval;
                        angular.forEach($scope.devices, function(v, k){
                            $scope.devices[k].live_img_src = ENV.liveImgUri + $scope.devices[k].guid + expander + "?cache=" + new Date().getTime();
                        });
                    }else{
                        delay = delay - 1;
                    }
                }, 1000);
            }else{
                angular.forEach($scope.devices, function(v, k){
                    $scope.devices[k].live_img_src = ENV.liveImgUri + $scope.devices[k].guid + expander + "?cache=" + new Date().getTime();
                });
            }
        };

        $scope.device_image_reload_clear = function(){
            if (angular.isDefined(device_image_interval_promise)) {
                $interval.cancel(device_image_interval_promise);
                device_image_interval_promise = undefined;
            }
        };

        //functions - Modal --------------------------------------------------------------------------------------------
        $scope.openLiveDeviceModal = function(live_device){//modal - live device
            Modal.open(
                "views/modal/liveDevice.html",
                "LiveDeviceCtrl",
                "lg",
                {
                    live_device: function(){
                        return live_device;
                    },
                    deviceImg_uri: function(){
                        return ENV.liveImgUri;
                    },
                    user: function(){
                        return $scope.user;
                    }
                }
            );
        };

        $scope.openAddDeviceModal = function(agents){//modal - add device
            Modal.open(
                "views/modal/addDevice.html",
                "AddDeviceCtrl",
                "lg",
                {
                    user: function(){
                        return $scope.user;
                    },
                    agents: function(){
                        return agents;
                    }
                }
            );
        };

        $scope.openAddDeviceGroupModal = function(groups){//modal - add device group
            Modal.open(
                "views/modal/addDeviceGroup.html",
                "AddDeviceGroupCtrl",
                "lg",
                {
                    user: function(){
                        return $scope.user;
                    },
                    groups: function(){
                        return groups;
                    }
                }
            );
        };

        $scope.openDeviceGroupSettingModal = function(){//modal - device group setting
            Modal.open(
                "views/modal/deviceGroupSetting.html",
                "DeviceGroupSettingCtrl",
                "lg",
                {
                    user: function(){
                        return $scope.user;
                    }
                }
            );
        };

        $scope.openDevicePropertyModal = function(device){//modal - device property
            Modal.open(
                "views/modal/deviceProperty.html",
                "DevicePropertyCtrl",
                "lg",
                {
                    device: function(){
                        return device;
                    }
                }
            );
        };

        $scope.openDeviceFileManagerModal = function(device){//modal - device File Manager
            Modal.open(
                "views/modal/fileManager.html",
                "FileManagerCtrl",
                "lg",
                {
                    device: function(){
                        return device;
                    }
                }
            );
        };

        $scope.openDeviceMemoModal = function(device){//modal - device memo
            Modal.open(
                "views/modal/deviceMemo.html",
                "DeviceMemoCtrl",
                "lg",
                {
                    device: function(){
                        return device;
                    }
                }
            );
        };

        //destroy ------------------------------------------------------------------------------------------------------
        $scope.$on('$destroy', function() {
            $scope.device_image_reload_clear();
        });
    });
