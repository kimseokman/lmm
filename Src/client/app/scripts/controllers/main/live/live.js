/**
 * Created by ksm on 2015-02-09.
 */
'use strict';

angular.module('lmmApp')
    .controller('LiveCtrl', function ($scope, $rootScope, $route, $location, $cookieStore, $filter, $interval, $timeout, Session, COM, ENV, Modal, ngTableParams, DeviceGroups, Devices, Company, AuditHistory, SharedService) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-live';

        //inherit data -------------------------------------------------------------------------------------------------
        $scope.$parent.updateInformation();
        $scope.$parent.updatePath();

        $scope.$on("USER", function(){
            $scope.user = SharedService.user;
        });
        $scope.$on("CONFIG", function(){
            $scope.config_str = SharedService.str;
        });
        $scope.$on("LOGO", function(){
            $scope.logo_img_uri = SharedService.logo_src;
        });

        $scope.setting = COM.setting;

        //default Setting ----------------------------------------------------------------------------------------------
        $scope.liveTreeRoot = {field: "mail", empty_field: "name"};
        $scope.liveRootGroupActive = false;
        $scope.liveStatusPage = {//liveStatus status
            live_img_uri: null,
            window_opened:{
                resource: null,
                sysInfo: null,
                process: null,
                software: null,
                hardware: null,
                audit: null
            }
        };
        $scope.devicePK = $route.current.params.devicePK;
        $scope.deviceVer = $route.current.params.deviceVer;

        $scope.auditHistoryTableHeader = [
            {name: "No.", className: "no", sortable: false},
            {name: "E-Mail", className: "mail", sortable: false},
            {name: "Date", className: "date", sortable: false},
            {name: "Action", className: "act", sortable: false}
        ];

        $scope.auditHistoryTableCol = $scope.auditHistoryTableHeader.length;//device table head col count
        $scope.auditHistoryTableLimits = [
            {value: 10},
            {value: 25},
            {value: 50},
            {value: 100}
        ];
        $scope.auditHistoryTableLimit = $scope.auditHistoryTableLimits[0];//set default limit line

        $scope.windows = {
            resource: 'windows',
            usageStatus: [
                {type: 'text_readOnly', name:'CPU Usage', field:'cpu_usage', empty: 'N/A', unit: '%'},
                {type: 'text_readOnly', name:'Fan Speed', field:'fan_speed_id', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'HDD Usage', field:'hdd_usage', empty: 'N/A', unit: '%'},
                {type: 'date_readOnly', name:'Running Time', field:'running_time', value: null, empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Memory Usage', field:'memory_usage', empty: 'N/A', unit: '%'},
                {type: 'date_readOnly', name:'System Time', field:'system_time', empty: 'N/A', unit: null},
                {type: 'text_readOnly_Temp', name:'CPU Temp.', field:'cpu_temp', empty: 'N/A', unit: '℃', split_str: "/", secUnit: '℉'},
                {type: 'text_readOnly', name:'Timezone', field:'time_zone_id', empty: 'N/A', unit: null},
                {type: 'text_readOnly_Temp', name:'HDD Temp.', field:'hdd_temp', empty: 'N/A', unit: '℃', split_str: "/", secUnit: '℉'}
            ],
            sysInfo: [
                {type: 'text_readOnly', name:'Processor', field:'processor', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'MAC Address', field:'mac_addr', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'BIOS Version/Date', field:'bios', empty: 'N/A', unit: null, secField:'', secValue: 'test2', secEmpty: 'N/A', secUnit: null},
                {type: 'text_readOnly', name:'Netmask', field:'netmask', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Physical Memory', field:'memory_capacity', empty: 'N/A', unit: "MB"},
                {type: 'text_readOnly', name:'DNS', field:'dns', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Physical HDD', field:'hdd_capacity', empty: 'N/A', unit: "MB"},
                {type: 'text_readOnly', name:'IP Address', field:'ip_addr', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Gateway', field:'gateway', empty: 'N/A', unit: null}
            ],
            processTableHeader: [
                {name: "No.", className: "no"},
                {name: "Name", className: "name"},
                {name: "CPU Usage", className: "cpu"},
                {name: "Memory Usage", className: "memory"}
            ],
            processTableCol: 0,
            processTable: [
                {pk: 'EMPTY'}
            ],
            softwareTableHeader: [
                {name: "Name", className: "name"},
                {name: "Corp.", className: "corp"},
                {name: "Installed Date", className: "date"},
                {name: "Physical", className: "physical"},
                {name: "Ver.", className: "version"}
            ],
            softwareTableCol: 0,
            softwareTable: [
                {pk: 'EMPTY'}
            ],
            hardwareInfo: [
                {pk: 'EMPTY'}
            ]
        };
        $scope.windows.processTableCol = $scope.windows.processTableHeader.length;
        $scope.windows.softwareTableCol = $scope.windows.softwareTableHeader.length;

        $scope.linux = {
            resource: 'linux',
            usageStatus: [
                {type: 'text_readOnly', name:'CPU Usage', field:'cpu_usage', empty: 'N/A', unit: '%'},
                {type: 'text_readOnly', name:'Fan Speed', field:'fan_speed_id', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'HDD Usage', field:'hdd_usage', empty: 'N/A', unit: '%'},
                {type: 'date_readOnly', name:'Running Time', field:'running_time', value: null, empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Memory Usage', field:'memory_usage', empty: 'N/A', unit: '%'},
                {type: 'date_readOnly', name:'System Time', field:'system_time', empty: 'N/A', unit: null},
                {type: 'text_readOnly_Temp', name:'CPU Temp.', field:'cpu_temp', empty: 'N/A', unit: '℃', split_str: "/", secUnit: '℉'},
                {type: 'text_readOnly', name:'Timezone', field:'time_zone_id', empty: 'N/A', unit: null},
                {type: 'text_readOnly_Temp', name:'HDD Temp.', field:'hdd_temp', empty: 'N/A', unit: '℃', split_str: "/", secUnit: '℉'}
            ],
            sysInfo: [
                {type: 'text_readOnly', name:'Processor', field:'processor', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'MAC Address', field:'mac_addr', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'BIOS Version/Date', field:'bios', empty: 'N/A', unit: null, secField:'', secValue: 'test2', secEmpty: 'N/A', secUnit: null},
                {type: 'text_readOnly', name:'Netmask', field:'netmask', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Physical Memory', field:'memory_capacity', empty: 'N/A', unit: "MB"},
                {type: 'text_readOnly', name:'DNS', field:'dns', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Physical HDD', field:'hdd_capacity', empty: 'N/A', unit: "MB"},
                {type: 'text_readOnly', name:'IP Address', field:'ip_addr', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Gateway', field:'gateway', empty: 'N/A', unit: null}
            ],
            processTableHeader: [
                {name: "No.", className: "no"},
                {name: "Name", className: "name"},
                {name: "CPU Usage", className: "cpu"},
                {name: "Memory Usage", className: "memory"}
            ],
            processTableCol: 0,
            processTable: [
                {pk: 'EMPTY'}
            ],
            softwareTableHeader: [
                {name: "Name", className: "name"},
                {name: "Corp.", className: "corp"},
                {name: "Installed Date", className: "date"},
                {name: "Physical", className: "physical"},
                {name: "Ver.", className: "version"}
            ],
            softwareTableCol: 0,
            softwareTable: [
                {pk: 'EMPTY'}
            ],
            hardwareInfo: [
                {pk: 'EMPTY'}
            ]
        };
        $scope.linux.processTableCol = $scope.linux.processTableHeader.length;
        $scope.linux.softwareTableCol = $scope.linux.softwareTableHeader.length;

        $scope.os_x = {
            resource: 'os_x',
            usageStatus: [
                {type: 'text_readOnly', name:'CPU Usage', field:'cpu_usage', empty: 'N/A', unit: '%'},
                {type: 'text_readOnly', name:'Fan Speed', field:'fan_speed_id', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'HDD Usage', field:'hdd_usage', empty: 'N/A', unit: '%'},
                {type: 'date_readOnly', name:'Running Time', field:'running_time', value: null, empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Memory Usage', field:'memory_usage', empty: 'N/A', unit: '%'},
                {type: 'date_readOnly', name:'System Time', field:'system_time', empty: 'N/A', unit: null},
                {type: 'text_readOnly_Temp', name:'CPU Temp.', field:'cpu_temp', empty: 'N/A', unit: '℃', split_str: "/", secUnit: '℉'},
                {type: 'text_readOnly', name:'Timezone', field:'time_zone_id', empty: 'N/A', unit: null},
                {type: 'text_readOnly_Temp', name:'HDD Temp.', field:'hdd_temp', empty: 'N/A', unit: '℃', split_str: "/", secUnit: '℉'}
            ],
            sysInfo: [
                {type: 'text_readOnly', name:'Processor', field:'processor', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'MAC Address', field:'mac_addr', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'BIOS Version/Date', field:'bios', empty: 'N/A', unit: null, secField:'', secValue: 'test2', secEmpty: 'N/A', secUnit: null},
                {type: 'text_readOnly', name:'Netmask', field:'netmask', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Physical Memory', field:'memory_capacity', empty: 'N/A', unit: "MB"},
                {type: 'text_readOnly', name:'DNS', field:'dns', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Physical HDD', field:'hdd_capacity', empty: 'N/A', unit: "MB"},
                {type: 'text_readOnly', name:'IP Address', field:'ip_addr', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Gateway', field:'gateway', empty: 'N/A', unit: null}
            ],
            processTableHeader: [
                {name: "No.", className: "no"},
                {name: "Name", className: "name"},
                {name: "CPU Usage", className: "cpu"},
                {name: "Memory Usage", className: "memory"}
            ],
            processTableCol: 0,
            processTable: [
                {pk: 'EMPTY'}
            ],
            softwareTableHeader: [
                {name: "Name", className: "name"},
                {name: "Corp.", className: "corp"},
                {name: "Installed Date", className: "date"},
                {name: "Physical", className: "physical"},
                {name: "Ver.", className: "version"}
            ],
            softwareTableCol: 0,
            softwareTable: [
                {pk: 'EMPTY'}
            ],
            hardwareInfo: [
                {pk: 'EMPTY'}
            ]
        };
        $scope.os_x.processTableCol = $scope.os_x.processTableHeader.length;
        $scope.os_x.softwareTableCol = $scope.os_x.softwareTableHeader.length;

        $scope.android = {
            resource: 'android',
            usageStatus: [
                {type: 'text_readOnly', name:'CPU Usage', field:'cpu_usage', empty: 'N/A', unit: '%'},
                {type: 'text_readOnly', name:'Fan Speed', field:'fan_speed_id', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'HDD Usage', field:'hdd_usage', empty: 'N/A', unit: '%'},
                {type: 'date_readOnly', name:'Running Time', field:'running_time', value: null, empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Memory Usage', field:'memory_usage', empty: 'N/A', unit: '%'},
                {type: 'date_readOnly', name:'System Time', field:'system_time', empty: 'N/A', unit: null},
                {type: 'text_readOnly_Temp', name:'CPU Temp.', field:'cpu_temp', empty: 'N/A', unit: '℃', split_str: "/", secUnit: '℉'},
                {type: 'text_readOnly', name:'Timezone', field:'time_zone_id', empty: 'N/A', unit: null},
                {type: 'text_readOnly_Temp', name:'HDD Temp.', field:'hdd_temp', empty: 'N/A', unit: '℃', split_str: "/", secUnit: '℉'}
            ],
            sysInfo: [
                {type: 'text_readOnly', name:'Processor', field:'processor', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'MAC Address', field:'mac_addr', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'BIOS Version/Date', field:'bios', empty: 'N/A', unit: null, secField:'', secValue: 'test2', secEmpty: 'N/A', secUnit: null},
                {type: 'text_readOnly', name:'Netmask', field:'netmask', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Physical Memory', field:'memory_capacity', empty: 'N/A', unit: "MB"},
                {type: 'text_readOnly', name:'DNS', field:'dns', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Physical HDD', field:'hdd_capacity', empty: 'N/A', unit: "MB"},
                {type: 'text_readOnly', name:'IP Address', field:'ip_addr', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Gateway', field:'gateway', empty: 'N/A', unit: null}
            ],
            processTableHeader: [
                {name: "No.", className: "no"},
                {name: "Name", className: "name"},
                {name: "CPU Usage", className: "cpu"},
                {name: "Memory Usage", className: "memory"}
            ],
            processTableCol: 0,
            processTable: [
                {pk: 'EMPTY'}
            ],
            softwareTableHeader: [
                {name: "Name", className: "name"},
                {name: "Corp.", className: "corp"},
                {name: "Installed Date", className: "date"},
                {name: "Physical", className: "physical"},
                {name: "Ver.", className: "version"}
            ],
            softwareTableCol: 0,
            softwareTable: [
                {pk: 'EMPTY'}
            ],
            hardwareInfo: [
                {pk: 'EMPTY'}
            ]
        };
        $scope.android.processTableCol = $scope.android.processTableHeader.length;
        $scope.android.softwareTableCol = $scope.android.softwareTableHeader.length;

        $scope.ios = {
            resource: 'ios',
            usageStatus: [
                {type: 'text_readOnly', name:'CPU Usage', field:'cpu_usage', empty: 'N/A', unit: '%'},
                {type: 'text_readOnly', name:'Fan Speed', field:'fan_speed_id', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'HDD Usage', field:'hdd_usage', empty: 'N/A', unit: '%'},
                {type: 'date_readOnly', name:'Running Time', field:'running_time', value: null, empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Memory Usage', field:'memory_usage', empty: 'N/A', unit: '%'},
                {type: 'date_readOnly', name:'System Time', field:'system_time', empty: 'N/A', unit: null},
                {type: 'text_readOnly_Temp', name:'CPU Temp.', field:'cpu_temp', empty: 'N/A', unit: '℃', split_str: "/", secUnit: '℉'},
                {type: 'text_readOnly', name:'Timezone', field:'time_zone_id', empty: 'N/A', unit: null},
                {type: 'text_readOnly_Temp', name:'HDD Temp.', field:'hdd_temp', empty: 'N/A', unit: '℃', split_str: "/", secUnit: '℉'}
            ],
            sysInfo: [
                {type: 'text_readOnly', name:'Processor', field:'processor', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'MAC Address', field:'mac_addr', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'BIOS Version/Date', field:'bios', empty: 'N/A', unit: null, secField:'', secValue: 'test2', secEmpty: 'N/A', secUnit: null},
                {type: 'text_readOnly', name:'Netmask', field:'netmask', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Physical Memory', field:'memory_capacity', empty: 'N/A', unit: "MB"},
                {type: 'text_readOnly', name:'DNS', field:'dns', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Physical HDD', field:'hdd_capacity', empty: 'N/A', unit: "MB"},
                {type: 'text_readOnly', name:'IP Address', field:'ip_addr', empty: 'N/A', unit: null},
                {type: 'text_readOnly', name:'Gateway', field:'gateway', empty: 'N/A', unit: null}
            ],
            processTableHeader: [
                {name: "No.", className: "no"},
                {name: "Name", className: "name"},
                {name: "CPU Usage", className: "cpu"},
                {name: "Memory Usage", className: "memory"}
            ],
            processTableCol: 0,
            processTable: [
                {pk: 'EMPTY'}
            ],
            softwareTableHeader: [
                {name: "Name", className: "name"},
                {name: "Corp.", className: "corp"},
                {name: "Installed Date", className: "date"},
                {name: "Physical", className: "physical"},
                {name: "Ver.", className: "version"}
            ],
            softwareTableCol: 0,
            softwareTable: [
                {pk: 'EMPTY'}
            ],
            hardwareInfo: [
                {pk: 'EMPTY'}
            ]
        };
        $scope.ios.processTableCol = $scope.ios.processTableHeader.length;
        $scope.ios.softwareTableCol = $scope.ios.softwareTableHeader.length;

        //default Data -------------------------------------------------------------------------------------------------
        var com_arg = {};
        Company.get({pk: COM.self_pk}, com_arg, function(data){
            if(data.status == 200){
                $scope.company = data.objects;
            }
        });

        $scope.$watch('devicePK', function (newVal, oldVal){//get tree
            $scope.devicePK = newVal;

            $scope.$parent.updatePath();
            //$scope.devicePK = $route.current.params.devicePK;
            //$scope.deviceVer = $route.current.params.deviceVer;

            Devices.get({pk: $scope.devicePK}, function(data){//get current device
                if(data.status == 200){
                    $scope.device = data.objects;
                    $scope.live_status_image_load();
                    //console.log(JSON.stringify($scope.device));
                    Session.set('current_device_pk', $scope.device.pk);
                    $scope.deviceVer = $scope.showDeviceOSStatus_atLiveStatus($scope.device.dev_os_id);

                    //parse process
                    var process_name_str = "" + $scope.device.prcss_name;
                    var process_name = process_name_str.split(";");
                    var process_cpu_usage_str = "" + $scope.device.prcss_cpu_usage;
                    var process_cpu_usage = process_cpu_usage_str.split(";");
                    var process_memory_usage_str = "" + $scope.device.prcss_memory_usage;
                    var process_memory_usage = process_memory_usage_str.split(";");
                    //parse software
                    var software_name_str = "" + $scope.device.software_name;
                    var software_name = software_name_str.split(";");
                    var software_list_str = "" + $scope.device.software_list;
                    var software_list = software_list_str.split(";");
                    //parse hardware
                    var hardware_name_str = "" + $scope.device.hardware_name;
                    var hardware_name = hardware_name_str.split(";");
                    var hardware_txt_str = "" + $scope.device.hardware_txt;
                    var hardware_txt = hardware_txt_str.split(";");

                    if($scope.deviceVer == 'windows'){
                        //get process
                        $scope.windows.processTable = [];
                        if(process_name[0] == "" || process_name[0] == $scope.config_str.undefined){
                            $scope.windows.processTable.push({pk: 'EMPTY'});
                        }else{
                            angular.forEach(process_name, function(v, k){
                                $scope.windows.processTable.push({
                                    icon: null,
                                    name: process_name[k],
                                    cpu_usage: process_cpu_usage[k],
                                    cpu_usage_unit:'%',
                                    memory_usage: process_memory_usage[k],
                                    memory_usage_unit:'%',
                                    empty: 'N/A'
                                });
                            });
                        }
                        //get software
                        $scope.windows.softwareTable = [];
                        if(software_name[0] == "" || software_name[0] == $scope.config_str.undefined){
                            $scope.windows.softwareTable.push({pk: 'EMPTY'});
                        }else{
                            var software_list_sq = 0;
                            angular.forEach(software_name, function(v, k){
                                $scope.windows.softwareTable.push({
                                    icon: null,
                                    name: software_name[k],
                                    corp: software_list[software_list_sq],
                                    install_date: software_list[software_list_sq+1],
                                    physical: software_list[software_list_sq+2],
                                    physical_unit: 'MB',
                                    version: software_list[software_list_sq+3],
                                    empty: 'N/A'
                                });
                                software_list_sq = software_list_sq+4;
                            });
                        }
                        //get hardware
                        $scope.windows.hardwareInfo = [];
                        if(hardware_name[0] == "" || hardware_name[0] == $scope.config_str.undefined){
                            $scope.windows.hardwareInfo.push({pk: 'EMPTY'});
                        }else{
                            angular.forEach(hardware_name, function(v, k){
                                $scope.windows.hardwareInfo.push({
                                    type: 'text_readOnly',
                                    name: hardware_name[k],
                                    value: hardware_txt[k],
                                    empty: 'N/A',
                                    unit: null
                                });
                            });
                        }
                    }else if($scope.deviceVer == 'linux'){
                        //get process
                        $scope.linux.processTable = [];
                        if(process_name[0] == "" || process_name[0] == $scope.config_str.undefined){
                            $scope.linux.processTable.push({pk: 'EMPTY'});
                        }else{
                            angular.forEach(process_name, function(v, k){
                                $scope.linux.processTable.push({
                                    icon: null,
                                    name: process_name[k],
                                    cpu_usage: process_cpu_usage[k],
                                    cpu_usage_unit:'%',
                                    memory_usage: process_memory_usage[k],
                                    memory_usage_unit:'%',
                                    empty: 'N/A'
                                });
                            });
                        }
                        //get software
                        $scope.linux.softwareTable = [];
                        if(software_name[0] == "" || software_name[0] == $scope.config_str.undefined){
                            $scope.linux.softwareTable.push({pk: 'EMPTY'});
                        }else{
                            var software_list_sq = 0;
                            angular.forEach(software_name, function(v, k){
                                $scope.linux.softwareTable.push({
                                    icon: null,
                                    name: software_name[k],
                                    corp: software_list[software_list_sq],
                                    install_date: software_list[software_list_sq+1],
                                    physical: software_list[software_list_sq+2],
                                    physical_unit: 'MB',
                                    version: software_list[software_list_sq+3],
                                    empty: 'N/A'
                                });
                                software_list_sq = software_list_sq+4;
                            });
                        }
                        //get hardware
                        $scope.linux.hardwareInfo = [];
                        if(hardware_name[0] == "" || hardware_name[0] == $scope.config_str.undefined){
                            $scope.linux.hardwareInfo.push({pk: 'EMPTY'});
                        }else{
                            angular.forEach(hardware_name, function(v, k){
                                $scope.linux.hardwareInfo.push({
                                    type: 'text_readOnly',
                                    name: hardware_name[k],
                                    value: hardware_txt[k],
                                    empty: 'N/A',
                                    unit: null
                                });
                            });
                        }
                    }else if($scope.deviceVer == 'os_x'){
                        //get process
                        $scope.os_x.processTable = [];
                        if(process_name[0] == "" || process_name[0] == $scope.config_str.undefined){
                            $scope.os_x.processTable.push({pk: 'EMPTY'});
                        }else{
                            angular.forEach(process_name, function(v, k){
                                $scope.os_x.processTable.push({
                                    icon: null,
                                    name: process_name[k],
                                    cpu_usage: process_cpu_usage[k],
                                    cpu_usage_unit:'%',
                                    memory_usage: process_memory_usage[k],
                                    memory_usage_unit:'%',
                                    empty: 'N/A'
                                });
                            });
                        }
                        //get software
                        $scope.os_x.softwareTable = [];
                        if(software_name[0] == "" || software_name[0] == $scope.config_str.undefined){
                            $scope.os_x.softwareTable.push({pk: 'EMPTY'});
                        }else{
                            var software_list_sq = 0;
                            angular.forEach(software_name, function(v, k){
                                $scope.os_x.softwareTable.push({
                                    icon: null,
                                    name: software_name[k],
                                    corp: software_list[software_list_sq],
                                    install_date: software_list[software_list_sq+1],
                                    physical: software_list[software_list_sq+2],
                                    physical_unit: 'MB',
                                    version: software_list[software_list_sq+3],
                                    empty: 'N/A'
                                });
                                software_list_sq = software_list_sq+4;
                            });
                        }
                        //get hardware
                        $scope.os_x.hardwareInfo = [];
                        if(hardware_name[0] == "" || hardware_name[0] == $scope.config_str.undefined){
                            $scope.os_x.hardwareInfo.push({pk: 'EMPTY'});
                        }else{
                            angular.forEach(hardware_name, function(v, k){
                                $scope.os_x.hardwareInfo.push({
                                    type: 'text_readOnly',
                                    name: hardware_name[k],
                                    value: hardware_txt[k],
                                    empty: 'N/A',
                                    unit: null
                                });
                            });
                        }
                    }else if($scope.deviceVer == 'android'){
                        //get process
                        $scope.android.processTable = [];
                        if(process_name[0] == "" || process_name[0] == $scope.config_str.undefined){
                            $scope.android.processTable.push({pk: 'EMPTY'});
                        }else{
                            angular.forEach(process_name, function(v, k){
                                $scope.android.processTable.push({
                                    icon: null,
                                    name: process_name[k],
                                    cpu_usage: process_cpu_usage[k],
                                    cpu_usage_unit:'%',
                                    memory_usage: process_memory_usage[k],
                                    memory_usage_unit:'%',
                                    empty: 'N/A'
                                });
                            });
                        }
                        //get software
                        $scope.android.softwareTable = [];
                        if(software_name[0] == "" || software_name[0] == $scope.config_str.undefined){
                            $scope.android.softwareTable.push({pk: 'EMPTY'});
                        }else{
                            var software_list_sq = 0;
                            angular.forEach(software_name, function(v, k){
                                $scope.android.softwareTable.push({
                                    icon: null,
                                    name: software_name[k],
                                    corp: software_list[software_list_sq],
                                    install_date: software_list[software_list_sq+1],
                                    physical: software_list[software_list_sq+2],
                                    physical_unit: 'MB',
                                    version: software_list[software_list_sq+3],
                                    empty: 'N/A'
                                });
                                software_list_sq = software_list_sq+4;
                            });
                        }
                        //get hardware
                        $scope.android.hardwareInfo = [];
                        if(hardware_name[0] == "" || hardware_name[0] == $scope.config_str.undefined){
                            $scope.android.hardwareInfo.push({pk: 'EMPTY'});
                        }else{
                            angular.forEach(hardware_name, function(v, k){
                                $scope.android.hardwareInfo.push({
                                    type: 'text_readOnly',
                                    name: hardware_name[k],
                                    value: hardware_txt[k],
                                    empty: 'N/A',
                                    unit: null
                                });
                            });
                        }
                    }else if($scope.deviceVer == 'ios'){
                        //get process
                        $scope.ios.processTable = [];
                        if(process_name[0] == "" || process_name[0] == $scope.config_str.undefined){
                            $scope.ios.processTable.push({pk: 'EMPTY'});
                        }else{
                            angular.forEach(process_name, function(v, k){
                                $scope.ios.processTable.push({
                                    icon: null,
                                    name: process_name[k],
                                    cpu_usage: process_cpu_usage[k],
                                    cpu_usage_unit:'%',
                                    memory_usage: process_memory_usage[k],
                                    memory_usage_unit:'%',
                                    empty: 'N/A'
                                });
                            });
                        }
                        //get software
                        $scope.ios.softwareTable = [];
                        if(software_name[0] == "" || software_name[0] == $scope.config_str.undefined){
                            $scope.ios.softwareTable.push({pk: 'EMPTY'});
                        }else{
                            var software_list_sq = 0;
                            angular.forEach(software_name, function(v, k){
                                $scope.ios.softwareTable.push({
                                    icon: null,
                                    name: software_name[k],
                                    corp: software_list[software_list_sq],
                                    install_date: software_list[software_list_sq+1],
                                    physical: software_list[software_list_sq+2],
                                    physical_unit: 'MB',
                                    version: software_list[software_list_sq+3],
                                    empty: 'N/A'
                                });
                                software_list_sq = software_list_sq+4;
                            });
                        }
                        //get hardware
                        $scope.ios.hardwareInfo = [];
                        if(hardware_name[0] == "" || hardware_name[0] == $scope.config_str.undefined){
                            $scope.ios.hardwareInfo.push({pk: 'EMPTY'});
                        }else{
                            angular.forEach(hardware_name, function(v, k){
                                $scope.ios.hardwareInfo.push({
                                    type: 'text_readOnly',
                                    name: hardware_name[k],
                                    value: hardware_txt[k],
                                    empty: 'N/A',
                                    unit: null
                                });
                            });
                        }
                    }

                    DeviceGroups.get({pk: $scope.device.device_group_fk}, function(data){//get current device group
                        if(data.status == 200){
                            $scope.device_group = data.objects;
                            Session.set('current_device_group_pk', $scope.device_group.pk);

                            var deviceGroupsArg = {};
                            var devicePK = Number(Session.get('current_device_pk'));
                            var deviceGroupPK = Number(Session.get('current_device_group_pk'));

                            DeviceGroups.get(deviceGroupsArg, function(data) {//get device group info & reload
                                if (data.status == 200) {
                                    $scope.groups = data.objects;
                                    angular.forEach($scope.groups, function(v, k){//tree require values
                                        if($scope.groups[k].pk == deviceGroupPK){
                                            $scope.groups[k].active = true;
                                        }else{
                                            $scope.groups[k].active = false;
                                        }
                                    });

                                    var deviceArg = {};
                                    deviceArg['device_group_pk'] = deviceGroupPK;
                                    deviceArg['search_mode'] = 'normal';
                                    var sorting_order = 'desc';//default order
                                    var sorting_by = 'alias';//default by
                                    deviceArg['order'] = sorting_order;
                                    deviceArg['by'] = sorting_by;

                                    Devices.get(deviceArg, function(data){
                                        if(data.status == 200){
                                            $scope.devices = data.objects;
                                            angular.forEach($scope.devices, function(v, k){//tree require values
                                                if($scope.devices[k].pk == devicePK){
                                                    $scope.devices[k].active = true;
                                                }else{
                                                    $scope.devices[k].active = false;
                                                }
                                            });
                                        }
                                    });

                                    $scope.liveRootGroupActive = true;//root device group set active
                                }
                            });
                        }
                    });
                }
            });

            if ($scope.auditHistoryTable == undefined) {
                $scope.auditHistoryTable = new ngTableParams({//get device
                    page: 1,//show first page
                    count: $scope.auditHistoryTableLimit.value,//count per page
                    sorting: {}//initial sorting
                }, {
                    total: 0,// length of data
                    getData: function ($defer, params) {
                        var audit_arg = {};
                        audit_arg['limit'] = params.count();
                        audit_arg['offset'] = (params.page() - 1) * audit_arg['limit'];
                        audit_arg['device_pk'] = $scope.devicePK;
                        var sorting_order = 'desc';//default order
                        var sorting_by = 'act_time';//default by

                        var sorting = params.sorting();
                        for (var k in sorting) {
                            var v = sorting[k];

                            sorting_order = v;
                            sorting_by = k;
                        }

                        audit_arg['order'] = sorting_order;
                        audit_arg['by'] = sorting_by;

                        AuditHistory.get(audit_arg, function(data){
                            $timeout(function () {
                                $scope.dataSet = data.objects;
                                params.total(data.meta.total_count);
                                $defer.resolve($scope.dataSet);
                            });
                        });
                    }
                });
            } else {
                $scope.auditHistoryTable.reload();
            }
        });

        var live_status_image_interval_promise;
        $scope.live_status_image_load = function(){
            var expander = ".png";
            $scope.liveStatusPage.live_img_uri = ENV.liveImgUri + $scope.device.guid + expander;

            if($scope.user.flag_img_refresh){
                var delay = $scope.user.img_refresh_interval;

                $scope.liveStatusPage.live_img_uri = ENV.liveImgUri + $scope.device.guid + expander + "?cache=" + new Date().getTime();//first img load

                if ( angular.isDefined(live_status_image_interval_promise) ) return;

                live_status_image_interval_promise = $interval(function() {
                    if(delay <= 0){
                        delay = $scope.user.img_refresh_interval;

                        $scope.liveStatusPage.live_img_uri = ENV.liveImgUri + $scope.device.guid + expander + "?cache=" + new Date().getTime();
                    }else{
                        delay = delay - 1;
                    }
                }, 1000);
            }else{
                $scope.liveStatusPage.live_img_uri = ENV.liveImgUri + $scope.device.guid + expander + "?cache=" + new Date().getTime();
            }
        };

        $scope.live_status_image_reload_clear = function(){
            if (angular.isDefined(live_status_image_interval_promise)) {
                $interval.cancel(live_status_image_interval_promise);
                live_status_image_interval_promise = undefined;
            }
        };

        //functions - Side ---------------------------------------------------------------------------------------------
        $scope.onClickLiveRootGroup = function(act_flag){
            $scope.liveRootGroupActive = !act_flag;

            var deviceGroupsArg = {};
            DeviceGroups.get(deviceGroupsArg, function(data) {//device group info reload
                if (data.status == 200) {
                    $scope.groups = data.objects;
                    angular.forEach($scope.groups, function(v, k){//tree require values
                        $scope.groups[k].active = false;
                    });
                }
            });
        };

        $scope.onClickLiveGroup = function(group){
            var group = group;
            Session.set('current_device_group_pk', group.pk);
            var devicePK = Number(Session.get('current_device_pk'));
            var deviceGroupPK = Number(Session.get('current_device_group_pk'));

            angular.forEach($scope.groups, function(v, k){//group active status set
                if(v.pk == deviceGroupPK){
                    $scope.groups[k].active = !group.active;
                }else{
                    $scope.groups[k].active = false;
                }
            });

            var deviceArg = {};
            deviceArg['device_group_pk'] = group.pk;
            deviceArg['search_mode'] = 'normal';
            var sorting_order = 'desc';//default order
            var sorting_by = 'alias';//default by
            deviceArg['order'] = sorting_order;
            deviceArg['by'] = sorting_by;

            $scope.devices = null;//resolve - Folder is opened and closed while developing

            Devices.get(deviceArg, function(data){
                if(data.status == 200){
                    $scope.devices = data.objects;
                    angular.forEach($scope.devices, function(v, k){//tree require values
                        if(v.pk == devicePK){
                            $scope.devices[k].active = true;
                        }else{
                            $scope.devices[k].active = false;
                        }
                    });
                }
            });
        };

        $scope.onClickLiveDevice = function(device){
            $scope.device = device;
            $scope.devicePK = $scope.device.pk;
            $scope.deviceVer = $scope.showDeviceOSStatus_atLiveStatus($scope.device.dev_os_id);
            Session.set('current_device_pk', $scope.device.pk);
            var devicePK = Number(Session.get('current_device_pk'));

            angular.forEach($scope.devices, function(v, k){//group active status set
                if(v.pk == devicePK){
                    $scope.devices[k].active = !$scope.device.active;
                }else{
                    $scope.devices[k].active = false;
                }
            });

            var change_uri = '/main/live/' + $scope.deviceVer + '/' + $scope.devicePK;
            var full_uri = $location.absUrl();
            var uri_split = full_uri.split('#');
            var current_uri = uri_split[1];

            if(current_uri != change_uri)
                $location.path(change_uri, false);
        };

        //functions - Content ------------------------------------------------------------------------------------------
        $scope.showDeviceOSStatus_atLiveStatus = function(value){
            var selected = $filter('filter')($scope.setting.device.os_statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                return selected[0].text;
            }
        };

        $scope.liveStatusCookieSync = function(){
            if($cookieStore.get("wc_resource") == undefined){//window controll resource
                $scope.liveStatusPage.window_opened.resource = true;
                $cookieStore.put("wc_resource", true);
            }else{
                $scope.liveStatusPage.window_opened.resource = $cookieStore.get("wc_resource");
            }

            if($cookieStore.get("wc_sysInfo") == undefined){//window controll system information
                $scope.liveStatusPage.window_opened.sysInfo = true;
                $cookieStore.put("wc_sysInfo", true);
            }else{
                $scope.liveStatusPage.window_opened.sysInfo = $cookieStore.get("wc_sysInfo");
            }

            if($cookieStore.get("wc_process") == undefined){//window controll system information
                $scope.liveStatusPage.window_opened.process = true;
                $cookieStore.put("wc_process", true);
            }else{
                $scope.liveStatusPage.window_opened.process = $cookieStore.get("wc_process");
            }

            if($cookieStore.get("wc_software") == undefined){//window controll software
                $scope.liveStatusPage.window_opened.software = true;
                $cookieStore.put("wc_software", true);
            }else{
                $scope.liveStatusPage.window_opened.software = $cookieStore.get("wc_software");
            }

            if($cookieStore.get("wc_hardware") == undefined){
                $scope.liveStatusPage.window_opened.hardware = true;
                $cookieStore.put("wc_hardware", true);
            }else{
                $scope.liveStatusPage.window_opened.hardware = $cookieStore.get("wc_hardware");
            }

            if($cookieStore.get("wc_audit") == undefined){
                $scope.liveStatusPage.window_opened.audit = true;
                $cookieStore.put("wc_audit", true);
            }else{
                $scope.liveStatusPage.window_opened.audit = $cookieStore.get("wc_audit");
            }
        };

        $scope.onClickPanelToggle = function (window_tag) {
            $scope.changeCookie = !$cookieStore.get(window_tag);
            $cookieStore.put(window_tag, $scope.changeCookie);

            $scope.liveStatusCookieSync();
        };

        //functions - If -----------------------------------------------------------------------------------------------
        $scope.if_showAuditData = function(pk){//if show audit table
            var show_flag = true;

            if(pk != "EMPTY"){
                show_flag = true;
            }else{
                show_flag = false;
            }

            return show_flag;
        };

        $scope.if_showDevice = function(pk){
            var show_flag = false;

            if(pk != 'EMPTY'){
                show_flag = true;
            }

            return show_flag;
        };

        $scope.if_showProcessData = function(pk){
            var show_flag = false;

            if(pk != 'EMPTY'){
                show_flag = true;
            }

            return show_flag;
        };

        $scope.if_showSoftwareData = function(pk){
            var show_flag = false;

            if(pk != 'EMPTY'){
                show_flag = true;
            }

            return show_flag;
        };

        $scope.if_showHardwareData = function(pk){
            var show_flag = false;

            if(pk != 'EMPTY'){
                show_flag = true;
            }

            return show_flag;
        };

        $scope.if_showDeviceRealData = function(data){
            var show_flag = false;
            if(data != undefined){
                show_flag = true;
            }
            return show_flag;
        };

        $scope.if_showDeviceRealSecondData = function(first_data_flag, data){
            var show_flag = false;
            if(first_data_flag && data != undefined){
                show_flag = true;
            }
            return show_flag;
        };
        //functions - Watch --------------------------------------------------------------------------------------------
        $scope.$watch('device', function(newValue, oldValue){
            $scope.device = newValue;
        });

        //functions - Modal --------------------------------------------------------------------------------------------
        $scope.openLiveDeviceModal = function(live_device){
            Modal.open(
                "views/modal/liveDevice.html",
                "LiveDeviceCtrl",
                "lg",
                {
                    live_device: function(){
                        return live_device;
                    },
                    deviceImg_uri: function(){
                        return ENV.liveImgUri;
                    },
                    user: function(){
                        return $scope.user;
                    }
                }
            );
        };

        //destroy ------------------------------------------------------------------------------------------------------
        $scope.$on('$destroy', function() {
            $scope.live_status_image_reload_clear();
        });
    });
