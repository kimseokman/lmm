/**
 * Created by ksm on 2015-04-02.
 */
'use strict';

angular.module('lmmApp')
    .controller('RemoteHistoryCtrl', function ($scope, $rootScope, $route, SharedService, RemoteHistory, ngTableParams, $timeout) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-remote-remoteHistory';

        //inherit data -------------------------------------------------------------------------------------------------
        $scope.$on("USER", function(){
            $scope.user = SharedService.user
        });
        $scope.$on("CONFIG", function(){
            $scope.config_str = SharedService.str;
        });
        $scope.$on("LOGO", function(){
            $scope.logo_img_uri = SharedService.logo_src;
        });

        //default Setting ----------------------------------------------------------------------------------------------
        $scope.reportPageName = $route.current.params.reportPageName;// 'remote'
        $scope.detailPageName = $route.current.params.detailPageName;// 'history'

        $scope.remoteHistoryTableHeader = [
            {name: "No.", className: "no", sortable: false},
            {name: "E-Mail", className: "mail", sortable: false},
            {name: "Name", className: "name", sortable: false},
            {name: "Device", className: "device", sortable: false},
            {name: "Alias", className: "alias", sortable: false},
            {name: "Host Public IP", className: "host_public_ip", sortable: false},
            {name: "Host Private IP", className: "host_public_ip", sortable: false},
            {name: "Viewer Public IP", className: "host_public_ip", sortable: false},
            {name: "Viewer Private IP", className: "host_public_ip", sortable: false},
            {name: "Start Date", className: "start_time", sortable: false},
            {name: "End Date", className: "end_time", sortable: false}
        ];

        $scope.remoteHistoryTableCol = $scope.remoteHistoryTableHeader.length;//device table head col count
        $scope.remoteHistoryTableLimits = [
            {value: 10},
            {value: 25},
            {value: 50},
            {value: 100}
        ];
        $scope.remoteHistoryTableLimit = $scope.remoteHistoryTableLimits[0];//set default limit line

        //default data -------------------------------------------------------------------------------------------------
        if ($scope.remoteHistoryTable == undefined) {
            $scope.remoteHistoryTable = new ngTableParams({//get device
                page: 1,//show first page
                count: $scope.remoteHistoryTableLimit.value,//count per page
                sorting: {}//initial sorting
            }, {
                total: 0,// length of data
                getData: function ($defer, params) {
                    var hs_remote_arg = {};
                    hs_remote_arg['limit'] = params.count();
                    hs_remote_arg['offset'] = (params.page() - 1) * hs_remote_arg['limit'];

                    var sorting_order = 'desc';//default order
                    var sorting_by = 'start_time';//default by

                    var sorting = params.sorting();
                    for (var k in sorting) {
                        var v = sorting[k];

                        sorting_order = v;
                        sorting_by = k;
                    }

                    hs_remote_arg['order'] = sorting_order;
                    hs_remote_arg['by'] = sorting_by;

                    RemoteHistory.get(hs_remote_arg, function(data){
                        $timeout(function () {
                            $scope.dataSet = data.objects;
                            params.total(data.meta.total_count);
                            $defer.resolve($scope.dataSet);
                        });
                    });
                }
            });
        } else {
            $scope.remoteHistoryTable.reload();
        }

        //functions - If -----------------------------------------------------------------------------------------------
        $scope.if_showRemoteHistoryData = function(pk){//if show remote history table
            var show_flag = true;

            if(pk != "EMPTY"){
                show_flag = true;
            }else{
                show_flag = false;
            }

            return show_flag;
        };
    });
