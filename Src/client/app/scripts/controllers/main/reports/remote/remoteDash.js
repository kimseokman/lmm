/**
 * Created by ksm on 2015-04-02.
 */
'use strict';

angular.module('lmmApp')
    .controller('RemoteDashCtrl', function ($scope, $rootScope, $filter, $route, $timeout, SharedService, RemoteDash, RemoteChart, ngTableParams) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-remote-remoteDash';

        //inherit data -------------------------------------------------------------------------------------------------
        $scope.$on("USER", function(){
            $scope.user = SharedService.user
        });
        $scope.$on("CONFIG", function(){
            $scope.config_str = SharedService.str;
        });
        $scope.$on("LOGO", function(){
            $scope.logo_img_uri = SharedService.logo_src;
        });

        //default Setting ----------------------------------------------------------------------------------------------
        $scope.reportPageName = $route.current.params.reportPageName;// 'remote'
        $scope.detailPageName = $route.current.params.detailPageName;// 'dash'
        $scope.searchTag = $route.current.params.searchTag;// default 'summary'

        $scope.daily_period = "1_day";

        $scope.searchTags = [
            {name: "Summary", class_name: "summary_btn", id: "summary", uri: "/main/reports/remote/dash/summary"},
            {name: "Monthly", class_name: "monthly_btn", id: "monthly", uri: "/main/reports/remote/dash/monthly"},
            {name: "Daily", class_name: "daily_btn", id: "daily", uri: "/main/reports/remote/dash/daily"},
            {name: "Hourly", class_name: "hourly_btn", id: "hourly", uri: "/main/reports/remote/dash/hourly"}
        ];

        $scope.summary_periods = [
            {name: "1 days", id: "1_day", class_name: "day_1_btn"},
            {name: "3 days", id: "3_day", class_name: "day_3_btn"},
            {name: "1 weeks", id: "1_week", class_name: "week_1_btn"},
            {name: "1 months", id: "1_month", class_name: "month_1_btn"},
            {name: "3 months", id: "3_month", class_name: "month_3_btn"},
            {name: "6 months", id: "6_month", class_name: "month_6_btn"},
            {name: "1 years", id: "1_year", class_name: "year_1_btn"},
            {name: "Custom", id: "0_etc", class_name: "etc_0_btn"}
        ];

        $scope.today = new Date();
        $scope.search_date = {
            start_date: $scope.today,
            end_date: $scope.today
        };

        $scope.param = {
            start_date: $scope.today,
            end_date: $scope.today
        };

        $scope.remoteDashUserTableHeader = [
            {name: "No.", className: "no", sortable: false},
            {name: "User", className: "mail", sortable: true, field: "mail"},
            {name: "Usage", className: "usage", sortable: false},
            {name: "Count", className: "cnt", sortable: false}
        ];

        $scope.remoteDashUserTableCol = $scope.remoteDashUserTableHeader.length;//device table head col count
        $scope.remoteDashUserTableLimits = [
            {value: 10},
            {value: 25},
            {value: 50},
            {value: 100}
        ];
        $scope.remoteDashUserTableLimit = $scope.remoteDashUserTableLimits[0];//set default limit line

        $scope.remoteDashDeviceTableHeader = [
            {name: "No.", className: "no", sortable: false},
            {name: "User", className: "mail", sortable: true, field: "mail"},
            {name: "Usage", className: "usage", sortable: false},
            {name: "Count", className: "cnt", sortable: false}
        ];

        $scope.remoteDashDeviceTableCol = $scope.remoteDashDeviceTableHeader.length;//device table head col count
        $scope.remoteDashDeviceTableLimits = [
            {value: 10},
            {value: 25},
            {value: 50},
            {value: 100}
        ];
        $scope.remoteDashDeviceTableLimit = $scope.remoteDashDeviceTableLimits[0];//set default limit line

        $scope.year_periods = [];
        $scope.month_periods = [];
        $scope.period = {};

        $scope.remoteLogTableHeader = [
            {name: "Date", className: "date"},
            {name: "Count", className: "cnt"},
            {name: "Ratio", className: "ratio"}
        ];
        $scope.remoteLogTableCol = $scope.remoteLogTableHeader.length;//device table head col count
        $scope.remoteLogTableLimits = [
            {value: 10},
            {value: 25},
            {value: 50},
            {value: 100}
        ];
        $scope.remoteLogTableLimit = $scope.remoteLogTableLimits[0];//set default limit line

        $scope.remoteLogTableVer2Header = [
            {name: "Time", className: "date"},
            {name: "Count", className: "cnt"},
            {name: "Total Time", className: "t_date"},
            {name: "Average Time", className: "a_date"},
            {name: "Ratio", className: "ratio"}
        ];
        $scope.remoteLogTableVer2Col = $scope.remoteLogTableVer2Header.length;//device table head col count
        $scope.remoteLogTableVer2Limits = [
            {value: 10},
            {value: 25},
            {value: 50},
            {value: 100}
        ];
        $scope.remoteLogTableVer2Limit = $scope.remoteLogTableVer2Limits[0];//set default limit line

        // data --------------------------------------------------------------------------------------------------------
        $scope.$watch('searchTag', function(newVal, oldVal){
            $scope.searchTag = newVal;

            $scope.remoteChart.loading = true;

            $scope.remoteChart.xAxis.categories = [];
            $scope.remoteChart.series[0].data = [];

            if($scope.searchTag == "summary"){
                if($scope.remoteDashUserTable == undefined){
                    $scope.remoteDashUserTable = new ngTableParams({//get device
                        page: 1,//show first page
                        count: $scope.remoteDashUserTableLimit.value,//count per page
                        sorting: {}//initial sorting
                    }, {
                        total: 0,// length of data
                        getData: function ($defer, params) {
                            var top_arg = {};
                            top_arg['limit'] = params.count();
                            top_arg['offset'] = (params.page() - 1) * top_arg['limit'];
                            top_arg["search_mode"] = "pagination";
                            top_arg["type"] = "user";
                            top_arg["start_date"] = $scope.param.start_date;
                            top_arg["end_date"] = $scope.param.end_date;
                            var sorting_order = 'desc';//default order
                            var sorting_by = 'reg_mail';//default by

                            var sorting = params.sorting();
                            for (var k in sorting) {
                                var v = sorting[k];

                                sorting_order = v;
                                sorting_by = k;
                            }

                            top_arg['order'] = sorting_order;
                            top_arg['by'] = sorting_by;

                            RemoteDash.get(top_arg, function(data){
                                $timeout(function () {
                                    $scope.remoteDashUser = data.objects;
                                    params.total(data.meta.total_count);
                                    $defer.resolve($scope.remoteDashUser);
                                });
                            });
                        }
                    });
                }else{
                    $scope.remoteDashUserTable.reload();
                }

                if($scope.remoteDashDeviceTable == undefined){
                    $scope.remoteDashDeviceTable = new ngTableParams({//get device
                        page: 1,//show first page
                        count: $scope.remoteDashDeviceTableLimit.value,//count per page
                        sorting: {}//initial sorting
                    }, {
                        total: 0,// length of data
                        getData: function ($defer, params) {
                            var top_arg = {};
                            top_arg['limit'] = params.count();
                            top_arg['offset'] = (params.page() - 1) * top_arg['limit'];
                            top_arg["search_mode"] = "pagination";
                            top_arg["type"] = "device";
                            top_arg["start_date"] = $scope.param.start_date;
                            top_arg["end_date"] = $scope.param.end_date;
                            var sorting_order = 'desc';//default order
                            var sorting_by = 'reg_mail';//default by

                            var sorting = params.sorting();
                            for (var k in sorting) {
                                var v = sorting[k];

                                sorting_order = v;
                                sorting_by = k;
                            }

                            top_arg['order'] = sorting_order;
                            top_arg['by'] = sorting_by;

                            RemoteDash.get(top_arg, function(data){
                                $timeout(function () {
                                    $scope.remoteDashDevice = data.objects;
                                    params.total(data.meta.total_count);
                                    $defer.resolve($scope.remoteDashDevice);
                                });
                            });
                        }
                    });
                }else{
                    $scope.remoteDashDeviceTable.reload();
                }
            }else if($scope.searchTag == "hourly"){
                //empty
            }else{
                $scope.year_periods = [];
                var log_range = 3;
                for(var i=log_range; i>=0; i--){//logging range 3 year
                    $scope.year_periods.push({text: $scope.today.getFullYear() - i});

                    if(i==0){
                        $scope.period.year = $scope.year_periods[log_range];
                    }
                }
            }
        });

        $scope.$watch('period.year', function(newVal, oldVal){
            $scope.period.year = newVal;

            $scope.remoteChart.loading = true;
            $scope.remoteChart.xAxis.categories = [];
            $scope.remoteChart.series[0].data = [];

            if($scope.searchTag == "monthly"){
                var chart_arg = {};
                chart_arg["type"] = $scope.searchTag;
                chart_arg["year"] = $scope.period.year.text;
                RemoteChart.get(chart_arg, function(data){
                    if(data.status == 200){
                        $scope.monthly_logs = data.objects;

                        angular.forEach($scope.monthly_logs, function(v, k){
                            $scope.remoteChart.title.text = v.title_date + " " + "Ratio";
                            $scope.remoteChart.xAxis.categories.push(v.value_date);
                            $scope.remoteChart.series[0].data.push(v.access_ratio);
                        });

                        $scope.remoteChart.loading = false;
                    }
                });
            }else if($scope.searchTag == "daily" && $scope.period.year != undefined){
                $scope.updatePeriodYear($scope.period.year);
            }
        });

        $scope.$watch('period.month', function(newVal, oldVal){
            $scope.period.month = newVal;

            $scope.remoteChart.loading = true;
            $scope.remoteChart.xAxis.categories = [];
            $scope.remoteChart.series[0].data = [];

            if($scope.searchTag == "daily" && $scope.period.month != undefined){
                var chart_arg = {};
                chart_arg["type"] = $scope.searchTag;
                chart_arg["year"] = $scope.period.year.text;
                chart_arg["month"] = $scope.period.month.text;
                RemoteChart.get(chart_arg, function(data){
                    if(data.status == 200){
                        $scope.daily_logs = data.objects;

                        angular.forEach($scope.daily_logs, function(v, k){
                            $scope.remoteChart.title.text = v.title_date + " " + "Ratio";
                            $scope.remoteChart.xAxis.categories.push(v.value_date);
                            $scope.remoteChart.series[0].data.push(v.access_ratio);
                        });

                        $scope.remoteChart.loading = false;
                    }
                });
            }
        });

        $scope.updatePeriodYear = function(period_item){
            $scope.period.year = period_item;
            $scope.month_periods = [];
            var month_range = 12;
            var start_month = 1;
            if($scope.period.year.text == $scope.today.getFullYear()){
                month_range = $scope.today.getMonth() + 1;
            }

            for(var i=start_month; i<=month_range; i++){//month range 12 months
                $scope.month_periods.push({text: i});

                if(i == month_range){
                    if($scope.period.year.text == $scope.today.getFullYear()){
                        $scope.period.month = $scope.month_periods[i-1];
                    }else{
                        $scope.period.month = $scope.month_periods[month_range-1];
                    }
                }
            }
        };

        $scope.updateSummaryPeriod = function(period_item){
            $scope.daily_period = period_item.id;
        };

        $scope.$watch('daily_period', function(newVal, oldVal){
            $scope.daily_period = newVal;

            if($scope.searchTag == "summary" || $scope.searchTag == "hourly"){
                if($scope.daily_period != undefined){
                    $scope.updateDate();
                }else{
                    $scope.daily_period = "1_day";//defualt set
                }
            }
        });

        $scope.$watch('search_date', function(newVal, oldVal){
            $scope.search_date.start_date = newVal.start_date;
            $scope.search_date.end_date = newVal.end_date;
            var start_date = new Date(newVal.start_date);
            var end_date = new Date(newVal.end_date);

            $scope.param.start_date = start_date.date_yyyy() + "-" + start_date.date_mm() + "-" + start_date.date_dd();
            $scope.param.end_date = end_date.date_yyyy() + "-" + end_date.date_mm() + "-" + end_date.date_dd();

            if($scope.searchTag == "summary"){
                if($scope.remoteDashUserTable == undefined){
                    $scope.remoteDashUserTable = new ngTableParams({//get device
                        page: 1,//show first page
                        count: $scope.remoteDashUserTableLimit.value,//count per page
                        sorting: {}//initial sorting
                    }, {
                        total: 0,// length of data
                        getData: function ($defer, params) {
                            var top_arg = {};
                            top_arg['limit'] = params.count();
                            top_arg['offset'] = (params.page() - 1) * top_arg['limit'];
                            top_arg["search_mode"] = "pagination";
                            top_arg["type"] = "user";
                            top_arg["start_date"] = $scope.param.start_date;
                            top_arg["end_date"] = $scope.param.end_date;
                            var sorting_order = 'desc';//default order
                            var sorting_by = 'reg_mail';//default by

                            var sorting = params.sorting();
                            for (var k in sorting) {
                                var v = sorting[k];

                                sorting_order = v;
                                sorting_by = k;
                            }

                            top_arg['order'] = sorting_order;
                            top_arg['by'] = sorting_by;

                            RemoteDash.get(top_arg, function(data){
                                $timeout(function () {
                                    $scope.remoteDashUser = data.objects;
                                    params.total(data.meta.total_count);
                                    $defer.resolve($scope.remoteDashUser);
                                });
                            });
                        }
                    });
                }else{
                    $scope.remoteDashUserTable.reload();
                }

                if($scope.remoteDashDeviceTable == undefined){
                    $scope.remoteDashDeviceTable = new ngTableParams({//get device
                        page: 1,//show first page
                        count: $scope.remoteDashDeviceTableLimit.value,//count per page
                        sorting: {}//initial sorting
                    }, {
                        total: 0,// length of data
                        getData: function ($defer, params) {
                            var top_arg = {};
                            top_arg['limit'] = params.count();
                            top_arg['offset'] = (params.page() - 1) * top_arg['limit'];
                            top_arg["search_mode"] = "pagination";
                            top_arg["type"] = "device";
                            top_arg["start_date"] = $scope.param.start_date;
                            top_arg["end_date"] = $scope.param.end_date;
                            var sorting_order = 'desc';//default order
                            var sorting_by = 'reg_mail';//default by

                            var sorting = params.sorting();
                            for (var k in sorting) {
                                var v = sorting[k];

                                sorting_order = v;
                                sorting_by = k;
                            }

                            top_arg['order'] = sorting_order;
                            top_arg['by'] = sorting_by;

                            RemoteDash.get(top_arg, function(data){
                                $timeout(function () {
                                    $scope.remoteDashDevice = data.objects;
                                    params.total(data.meta.total_count);
                                    $defer.resolve($scope.remoteDashDevice);
                                });
                            });
                        }
                    });
                }else{
                    $scope.remoteDashDeviceTable.reload();
                }
            } else if($scope.searchTag == "hourly"){
                $scope.remoteChart.loading = true;

                $scope.remoteChart.xAxis.categories = [];
                $scope.remoteChart.series[0].data = [];

                var chart_arg = {};
                chart_arg["type"] = $scope.searchTag;
                chart_arg["start_date"] = $scope.param.start_date;
                chart_arg["end_date"] = $scope.param.end_date;
                RemoteChart.get(chart_arg, function(data){
                    if(data.status == 200){
                        if($scope.searchTag == "hourly"){
                            $scope.hourly_logs = data.objects;
                            angular.forEach($scope.hourly_logs, function(v, k){
                                $scope.remoteChart.title.text = v.title_date + " " + "Ratio";
                                $scope.remoteChart.xAxis.categories.push(v.value_date);
                                $scope.remoteChart.series[0].data.push(v.access_ratio);
                            });
                        }

                        $scope.remoteChart.loading = false;
                    }
                });
            }
        }, true);

        $scope.updateDate = function(){
            $scope.period_list = $scope.daily_period.split("_");
            var interval = Number($scope.period_list[0]);
            var interval_tag = $scope.period_list[1];

            var apply_date = new Date();
            $scope.search_date.end_date = apply_date;

            if(interval_tag == "year"){
                $scope.search_date.start_date = $scope.getIntervalYear(apply_date, interval);
            }else if(interval_tag == "month"){
                $scope.search_date.start_date = $scope.getIntervalMonth(apply_date, interval);
            }else if(interval_tag == "week"){
                $scope.search_date.start_date = $scope.getIntervalWeek(apply_date);
            }else if(interval_tag == "day"){
                $scope.search_date.start_date = $scope.getIntervalDay(apply_date, interval);
            }else if(interval_tag == "etc"){

            }
        };

        //functions - IF -----------------------------------------------------------------------------------------------
        $scope.if_searchPeriod = function(period_id){
            var show_flag = false;
            if(period_id == $scope.daily_period){
                show_flag = true;
            }
            return show_flag;
        };

        $scope.if_searchTag = function(tag_id){
            var show_flag = false;
            if(tag_id == $scope.searchTag){
                show_flag = true;
            }
            return show_flag;
        };

        $scope.if_showIsSummary = function(){
            var show_flag = false;
            if($scope.searchTag == "summary"){
                show_flag = true;
            }
            return show_flag;
        };

        $scope.if_showIsMonthly = function(){
            var show_flag = false;
            if($scope.searchTag == "monthly"){
                show_flag = true;
            }
            return show_flag;
        };

        $scope.if_showIsDaily = function(){
            var show_flag = false;
            if($scope.searchTag == "daily"){
                show_flag = true;
            }
            return show_flag;
        };

        $scope.if_showIsHourly = function(){
            var show_flag = false;
            if($scope.searchTag == "hourly"){
                show_flag = true;
            }
            return show_flag;
        };

        $scope.if_showRemoteDashTable = function(pk){
            var show_flag = true;

            if(pk != "EMPTY"){
                show_flag = true;
            }else{
                show_flag = false;
            }

            return show_flag;
        };

        $scope.if_showRemoteDashTableColumn = function(pk){
            var show_flag = true;
            var pk_str = "" + pk;
            if(pk_str.indexOf("vertual_key") == -1){
                show_flag = true;
            }else{
                show_flag = false;
            }

            return show_flag;
        };

        //functions - module -------------------------------------------------------------------------------------------


        $scope.getIntervalDay = function (d, interval){
            var d = new Date(d);
            var interval = interval;
            var diff = d.getDate() - interval;
            var resDate = new Date(d.setDate(diff));

            return resDate;
        };

        $scope.getIntervalEndDay = function (d, interval){
            var d = new Date(d);
            var interval = interval;
            var diff = d.getDate() + interval;
            var resDate = new Date(d.setDate(diff));

            return resDate;
        };

        $scope.getIntervalWeek = function (d){
            var d = new Date(d);
            var interval = 6;
            var diff = d.getDate() - interval;
            var resDate = new Date(d.setDate(diff));

            return resDate;
        };

        $scope.getIntervalEndWeek = function (d){
            var d = new Date(d);
            var interval = 6;
            var diff = d.getDate() + interval;
            var resDate = new Date(d.setDate(diff));

            return resDate;
        };

        $scope.getIntervalMonth = function (d, interval){
            var d = new Date(d);
            var interval = interval;

            var resDate = new Date(d.getFullYear(), d.getMonth()-interval, d.getDate());

            return resDate;
        };

        $scope.getIntervalEndMonth = function (d, interval){
            var d = new Date(d);
            var interval = interval;

            var resDate = new Date(d.getFullYear(), d.getMonth()+interval, d.getDate());

            return resDate;
        };

        $scope.getIntervalYear = function (d, interval){
            var d = new Date(d);
            var interval = interval;

            var resDate = new Date(d.getFullYear()-interval, d.getMonth(), d.getDate());

            return resDate;
        };

        $scope.getIntervalEndYear = function (d, interval){
            var d = new Date(d);
            var interval = interval;

            var resDate = new Date(d.getFullYear()+interval, d.getMonth(), d.getDate());

            return resDate;
        };

        Date.prototype.date_yyyy = function() {
            var yyyy = this.getFullYear().toString();
            return yyyy;
        };

        Date.prototype.date_mm = function() {
            var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
            return (mm[1]?mm:"0"+mm[0]);
        };

        Date.prototype.date_dd = function() {
            var dd  = this.getDate().toString();
            return (dd[1]?dd:"0"+dd[0]);
        };

        //functions chart ----------------------------------------------------------------------------------------------
        $scope.remoteChart = {
            options: {
                chart: {
                    type: 'column'
                }
            },
            title: {
                text: "Total Ratio"
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: 0
                }
            },
            yAxis: {
                min: 0,
                max: 100,
                title: {
                    text: 'Ratio (%)'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}%'
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },
            series: [{
                name: 'Percent',
                colorByPoint: true,
                data: []
            }],
            loading: true
        };
    });