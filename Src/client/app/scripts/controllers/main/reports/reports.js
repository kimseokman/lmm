/**
 * Created by ksm on 2015-02-09.
 */
'use strict';

angular.module('lmmApp')
    .controller('ReportsCtrl', function ($scope, $rootScope, $location, SharedService) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-reports';

        //inherit data -------------------------------------------------------------------------------------------------
        $scope.$parent.updateInformation();
        $scope.$parent.updatePath();

        $scope.$on("USER", function(){
            $scope.user = SharedService.user;
        });
        $scope.$on("CONFIG", function(){
            $scope.config_str = SharedService.str;
        });
        $scope.$on("LOGO", function(){
            $scope.logo_img_uri = SharedService.logo_src;
        });

        //default Setting ----------------------------------------------------------------------------------------------
        $scope.reportPageName = 'remote';
        $scope.detailPageName = 'dash';

        $scope.report_side_menu = [
            {
                name: "Remote Control", active_tag: 'main/reports/remote/',
                sub: [
                    {name: "Chart Dashboard", active_tag: 'main/reports/remote/dash', uri: '/main/reports/remote/dash/summary'},
                    {name: "History", active_tag: 'main/reports/remote/history', uri: '/main/reports/remote/history'}
                ]
            },
            {
                name: "Alert", active_tag: 'main/reports/alert/',
                sub: [
                    {name: "Chart Dashboard", active_tag: 'main/reports/alert/dash', uri: '/main/reports/alert/dash'},
                    {name: "History", active_tag: 'main/reports/alert/history', uri: '/main/reports/alert/history'}
                ]
            }
        ];

        //function - side ----------------------------------------------------------------------------------------------
        $scope.onClickReportMenu = function(menu_item){
            var change_uri = menu_item.uri;
            var full_uri = $location.absUrl();
            var uri_split = full_uri.split('#');
            var current_uri = uri_split[1];

            if(current_uri != change_uri)
                $location.path(change_uri, true);
        };
    });
