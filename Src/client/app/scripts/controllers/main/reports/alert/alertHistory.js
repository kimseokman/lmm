/**
 * Created by ksm on 2015-04-02.
 */
'use strict';

angular.module('lmmApp')
    .controller('AlertHistoryCtrl', function ($scope, $rootScope, $route, $timeout, SharedService, ngTableParams, AlertHistory) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-reports-alertHistory';

        //inherit data -------------------------------------------------------------------------------------------------
        $scope.$on("USER", function(){
            $scope.user = SharedService.user
        });
        $scope.$on("CONFIG", function(){
            $scope.config_str = SharedService.str;
        });
        $scope.$on("LOGO", function(){
            $scope.logo_img_uri = SharedService.logo_src;
        });

        //default Setting ----------------------------------------------------------------------------------------------
        $scope.reportPageName = $route.current.params.reportPageName;// 'alert'
        $scope.detailPageName = $route.current.params.detailPageName;// 'history'

        $scope.alertHistoryTableHeader = [
            {name: "No.", className: "no", sortable: false},
            {name: "Status", className: "status", sortable: false},
            {name: "Category", className: "category", sortable: false},
            {name: "Description", className: "description", sortable: false},
            {name: "Device", className: "device", sortable: false},
            {name: "Start Date", className: "start_time", sortable: false},
            {name: "End Date", className: "end_time", sortable: false},
            {name: "Interval", className: "interval", sortable: false}
        ];

        $scope.alertHistoryTableCol = $scope.alertHistoryTableHeader.length;//device table head col count
        $scope.alertHistoryTableLimits = [
            {value: 10},
            {value: 25},
            {value: 50},
            {value: 100}
        ];
        $scope.alertHistoryTableLimit = $scope.alertHistoryTableLimits[0];//set default limit line

        //default data -------------------------------------------------------------------------------------------------
        if ($scope.alertHistoryTable == undefined) {
            $scope.alertHistoryTable = new ngTableParams({//get device
                page: 1,//show first page
                count: $scope.alertHistoryTableLimit.value,//count per page
                sorting: {}//initial sorting
            }, {
                total: 0,// length of data
                getData: function ($defer, params) {
                    var hs_alert_arg = {};
                    hs_alert_arg['limit'] = params.count();
                    hs_alert_arg['offset'] = (params.page() - 1) * hs_alert_arg['limit'];

                    var sorting_order = 'desc';//default order
                    var sorting_by = 'start_time';//default by

                    var sorting = params.sorting();
                    for (var k in sorting) {
                        var v = sorting[k];

                        sorting_order = v;
                        sorting_by = k;
                    }

                    hs_alert_arg['order'] = sorting_order;
                    hs_alert_arg['by'] = sorting_by;

                    AlertHistory.get(hs_alert_arg, function(data){
                        $timeout(function () {
                            $scope.dataSet = data.objects;
                            params.total(data.meta.total_count);
                            $defer.resolve($scope.dataSet);
                        });
                    });
                }
            });
        } else {
            $scope.alertHistoryTable.reload();
        }

        //functions - If -----------------------------------------------------------------------------------------------
        $scope.if_showAlertHistoryData = function(pk){//if show alert history table
            var show_flag = true;

            if(pk != "EMPTY"){
                show_flag = true;
            }else{
                show_flag = false;
            }

            return show_flag;
        };
    });