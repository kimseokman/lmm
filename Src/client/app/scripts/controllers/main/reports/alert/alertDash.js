/**
 * Created by ksm on 2015-04-02.
 */
'use strict';

angular.module('lmmApp')
    .controller('AlertDashCtrl', function ($scope, $rootScope, $route, SharedService) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-reports-alertDash';

        //inherit data -------------------------------------------------------------------------------------------------
        $scope.$on("USER", function(){
            $scope.user = SharedService.user
        });
        $scope.$on("CONFIG", function(){
            $scope.config_str = SharedService.str;
        });
        $scope.$on("LOGO", function(){
            $scope.logo_img_uri = SharedService.logo_src;
        });

        //default Setting ----------------------------------------------------------------------------------------------
        $scope.reportPageName = $route.current.params.reportPageName;// 'alert'
        $scope.detailPageName = $route.current.params.detailPageName;// 'dash'
    });
