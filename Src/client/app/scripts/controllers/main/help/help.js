/**
 * Created by ksm on 2015-02-09.
 */
'use strict';

angular.module('lmmApp')
    .controller('HelpCtrl', function ($scope) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-help';
        $scope.$parent.updateInformation();
        $scope.$parent.updatePath();

        //default Setting ----------------------------------------------------------------------------------------------
        $scope.helpPageName = 'resource';
        $scope.help_side_menu = [
            {icon: "whatsapp", name: "Inquiry & History", uri: 'main/help/inquiry'},
            {icon: "download", name: "Resource", uri: 'main/help/resource'}
        ];

        $scope.manuals = [
            {label: "Agent Installation Guide", file_src: null},
            {label: "Manager Manual", file_src: null},
            {label: "User Manual", file_src: null}
        ];
        //default Data -------------------------------------------------------------------------------------------------

        //functions - Side ---------------------------------------------------------------------------------------------

        //functions - Content ------------------------------------------------------------------------------------------

        //functions - If -----------------------------------------------------------------------------------------------
        $scope.if_showHelpSide = function(){
            var show_flag = false;

            if(true) {
                show_flag = true;
            }

            return show_flag;
        };

        $scope.if_disableDownBtn = function(src){
            var able_flag = true;
            if(src != undefined){
                able_flag = false;
            }
            return able_flag;
        };
        //functions - Watch --------------------------------------------------------------------------------------------
        //functions - Modal --------------------------------------------------------------------------------------------
    });
