/**
 * Created by ksm on 2015-03-26.
 */
'use strict';

angular.module('lmmApp')
    .controller('ProfileCtrl', function($scope, $rootScope, $interval, $upload, $route, Users, Company, COM, ENV, SharedService){
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-admin-profile';

        //inherit data -------------------------------------------------------------------------------------------------
        $scope.$on("USER", function(){
            $scope.user = SharedService.user;
            $scope.admin_image_load();//admin image load
        });
        $scope.$on("CONFIG", function(){
            $scope.config_str = SharedService.str;
        });
        $scope.$on("LOGO", function(){
            $scope.logo_img_uri = SharedService.logo_src;
        });

        //default Setting ----------------------------------------------------------------------------------------------
        $scope.adminPageName = $route.current.params.adminPageName;// 'profile'
        $scope.detailPageName = $route.current.params.detailPageName;

        $scope.files = null;//upload file

        $scope.profile = {//profile profile
            guide_msg: "",
            id: {name: "E-mail", type: "text", id: "profile_id", isRequired: false, field: "mail", value: null, empty: "EMPTY"},
            recept_mail: {name: "Reception Mail", type: "text", id: "profile_recept_mail", isRequired: false, field: "flag_recept_mail", value: null, empty: "EMPTY"},
            password: {name: "Password", type: "password", id: "profile_pw", isRequired: true, edit_flag: false, field: "pw", value: null, empty: "EMPTY", status_class: ""},
            retype_password: {name: "Retype Password", type: "password", id: "profile_re_pw", isRequired: true, edit_flag: false, save_flag: false,  field: "pw", value: null, empty: "EMPTY", status_class: ""},
            step2: {name: "2-Step Verification", type: "text", id: "profile_2step", isRequired: false, field: "flag_use_otp", value: null, empty: "EMPTY"},
            time_zone: {name: "Time Zone", type: "text", id: "profile_timezone", isRequired: false, field: "time_zone_id", value: null, empty: "EMPTY"},
            date_fmt: {name: "Date Format", type: "text", id: "profile_dateformat", isRequired: false, field: "date_fmt_id", value: null, empty: "EMPTY"},
            refresh: {name: "Refresh", type: "text", id: "profile_refresh", isRequired: false, field: "flag_refresh", value: null, empty: "EMPTY"},
            refresh_interval: {name: "Refresh Time", type: "text", id: "profile_refresh_interval", isRequired: false, field: "refresh_interval", value: null, empty: "EMPTY"},
            duplicate_login: {name: "Duplicate Login", type: "text", id: "profile_duplicate_login", isRequired: false, field: "flag_duplicate_prevent", value: null, empty: "EMPTY"},
            name: {name: "Name", type: "text", id: "profile_name", isRequired: false, field: "name", value: null, empty: "EMPTY"},
            phone: {name: "Mobile Phone", type: "text", id: "profile_phone", isRequired: false, field: "phone", value: null, empty: "EMPTY"},
            tel: {name: "Office Phone", type: "text", id: "profile_tel", isRequired: false, field: "tel", value: null, empty: "EMPTY"},
            position: {name: "Position", type: "text", id: "profile_position", isRequired: false, field: "position", value: null, empty: "EMPTY"},
            department: {name: "Department", type: "text", id: "profile_department", isRequired: false, field: "department", value: null, empty: "EMPTY"},
            img_refresh: {name: "Image Refresh", type: "text", id: "profile_img_refresh", isRequired: false, field: "flag_img_refresh", value: null, empty: "EMPTY"},
            img_refresh_interval: {name: "Image Refresh Time", type: "text", id: "profile_img_refresh_interval", isRequired: false, field: "img_refresh_interval", value: null, empty: "EMPTY"},
            dev_vol: {name: "Device Volume", type: "text", id: "dev_vol", isRequired: false, field: "dev_vol", value: null, empty: "UNKNOWN"},
            dev_cnt: {name: "Device Count", type: "text", id: "dev_cnt", isRequired: false, field: "dev_cnt", value: null, empty: "EMPTY"},
            create_member_vol: {name: "User Volume", type: "text", id: "create_member_vol", isRequired: false, field: "create_member_vol", value: null, empty: "UNKNOWN"},
            create_member_cnt: {name: "User Count", type: "text", id: "create_member_cnt", isRequired: false, field: "create_member_cnt", value: null, empty: "EMPTY"}
        };

        $scope.business = [//profile business
            {name: "Company", field: "name", empty: "EMPTY"},
            {name: "Business Registration Number", field: "corp_reg_no", empty: "EMPTY"},
            {name: "CEO", field: "ceo_name", empty: "EMPTY"},
            {name: "Homepage", field: "hp_uri", empty: "EMPTY"},
            {name: "Tel", field: "tel", empty: "EMPTY"},
            {name: "Fax", field: "fax", empty: "EMPTY"}
        ];

        //default Data -------------------------------------------------------------------------------------------------
        var com_arg = {};
        Company.get({pk: COM.self_pk}, com_arg, function(data){
            if(data.status == 200){
                $scope.company = data.objects;
            }
        });

        //functions - Content ------------------------------------------------------------------------------------------
        $scope.updateCorpByDB = function(field, data, pk){
            var base_pk = (pk == null) ? COM.self_pk : pk;
            var corp_arg = {};
            corp_arg[field] = data;

            Company.update({pk: base_pk}, corp_arg, function(data){
                if(data.status == 200){
                    var change_label = "";

                    switch(field){
                        case "name":
                            change_label = "Company";
                            break;
                        case "corp_reg_no":
                            change_label = "Business Registration Number";
                            break;
                        case "ceo_name":
                            change_label = "CEO";
                            break;
                        case "hp_uri":
                            change_label = "Homepage";
                            break;
                        case "tel":
                            change_label = "Tel";
                            break;
                        case "fax":
                            change_label = "Fax";
                            break;
                    }

                    $scope.profile.guide_msg = change_label + " changed";
                }
            });
        };

        $scope.updateUserByDB = function(field, data, pk){
            var base_pk = (pk == null) ? COM.self_pk : pk;
            var user_arg = {};
            user_arg[field] = data;
            var validate_msg = "";
            //validate
            if(field=="refresh_interval"){
                if(data < 3000){
                    validate_msg = "Can not be set to less than 3000.";
                    return validate_msg;
                }
            }

            if(field=="img_refresh_interval"){
                if(data < 5){
                    validate_msg = "Can not be set to less than 5.";
                    return validate_msg;
                }
            }

            Users.update({pk: base_pk}, user_arg, function(data){
                if(data.status == 200){
                    var change_label = "";

                    switch(field){
                        case "pw":
                            change_label = "Password";
                            $scope.cancelEditPassword();
                            break;
                        case "flag_use_otp":
                            change_label = "2-Step Verification";
                            break;
                        case "time_zone_id":
                            change_label = "Time Zone";
                            break;
                        case "date_fmt_id":
                            change_label = "Date Format";
                            break;
                        case "flag_duplicate_prevent":
                            change_label = "Duplicate Login";
                            break;
                        case "name":
                            change_label = "Name";
                            break;
                        case "phone":
                            change_label = "Mobile Phone";
                            break;
                        case "tel":
                            change_label = "Office Phone";
                            break;
                        case "position":
                            change_label = "Position";
                            break;
                        case "department":
                            change_label = "Department";
                            break;
                        case "flag_refresh":
                            change_label = "Refresh";
                            break;
                        case "refresh_interval":
                            change_label = "Refresh Time";
                            break;
                        case "flag_img_refresh":
                            change_label = "Image Refresh";
                            break;
                        case "img_refresh_interval":
                            change_label = "Image Refresh Time";
                            break;
                        case "flag_recept_mail":
                            change_label = "Reception Mail";
                            break;
                    }

                    $scope.profile.guide_msg = change_label + " changed";
                }
            });
        };

        $scope.otp_reset = function(pk){
            var base_pk = (pk == null) ? COM.self_pk : pk;
            var user_arg = {};
            user_arg["flag_create_qrcode"] = false;
            Users.update({pk: base_pk}, user_arg, function(data){
                if(data.status == 200){
                    $scope.profile.guide_msg = "OTP Reset";
                }
            });
        };

        $scope.upload_file = function(files){
            if (files && files.length) {
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    $upload.upload({
                        url: ENV.host + '/lmm/api/v1.0/file_upload',
                        method: 'POST',
                        //fields: {'username': $scope.username},
                        file: file
                    }).progress(function (evt) {
                        $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        //console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.file.name);
                    }).success(function (data, status, headers, config) {
                        var expander = ".png";
                        $scope.logo_img_uri = ENV.uploadImgUri + $scope.get_parse_name($scope.user.mail) + expander + "?cache=" + new Date().getTime();
                        SharedService.prepForLogoBroadcast($scope.logo_img_uri);
                        //console.log('file ' + config.file.name + 'uploaded. Response: ' + JSON.stringify(data));
                    });
                }
            }
        };

        $scope.editPassword = function(){
            $scope.profile.password.edit_flag = true;
            $scope.profile.retype_password.edit_flag = false;
            $scope.profile.retype_password.save_flag = false;
        };

        $scope.cancelEditPassword = function(){
            $scope.profile.password.value = null;
            $scope.profile.password.edit_flag = false;
            $scope.profile.retype_password.value = null;
            $scope.profile.retype_password.edit_flag = false;
            $scope.profile.retype_password.save_flag = false;

            $scope.validatePassword();
        };

        $scope.validatePassword = function(){
            if($scope.profile.password.value == null || $scope.profile.password.value == ""){
                $scope.profile.password.status_class = "";

                $scope.profile.retype_password.status_class = "";

            }else{
                $scope.profile.password.status_class = "brand-success border-success ver_success";
                $scope.profile.retype_password.edit_flag = true;

                if($scope.profile.retype_password.value == null || $scope.profile.retype_password.value == ""){
                    $scope.profile.retype_password.status_class = "brand-warning border-warning";
                }else{
                    if($scope.profile.password.value != $scope.profile.retype_password.value){
                        $scope.profile.retype_password.status_class = "brand-danger border-danger ver_danger";
                    }else{
                        $scope.profile.retype_password.status_class = "brand-success border-success ver_success";
                        $scope.profile.retype_password.save_flag = true;
                    }
                }
            }
        };

        $scope.getLogoImgProgressType = function(ratio){
            var type = '';

            if (ratio == 100) {
                type = 'success';
            } else if (ratio > 50) {
                type = 'info';
            } else if (ratio > 25) {
                type = 'warning';
            } else {
                type = 'danger';
            }

            return type;
        };

        var admin_image_interval_promise;
        $scope.admin_image_load = function(){
            var expander = ".png";
            $scope.logo_img_uri = ENV.uploadImgUri + $scope.get_parse_name($scope.user.mail) + expander;

            if($scope.user.flag_img_refresh){
                var delay = $scope.user.img_refresh_interval;

                $scope.logo_img_uri = ENV.uploadImgUri + $scope.get_parse_name($scope.user.mail) + expander + "?cache=" + new Date().getTime();//first img load

                if ( angular.isDefined(admin_image_interval_promise) ) return;

                admin_image_interval_promise = $interval(function() {
                    if(delay <= 0){
                        delay = $scope.user.img_refresh_interval;

                        $scope.logo_img_uri = ENV.uploadImgUri + $scope.get_parse_name($scope.user.mail) + expander + "?cache=" + new Date().getTime();
                    }else{
                        delay = delay - 1;
                    }
                }, 1000);
            }else{
                $scope.logo_img_uri = ENV.uploadImgUri + $scope.get_parse_name($scope.user.mail) + expander + "?cache=" + new Date().getTime();
            }
        };

        $scope.admin_image_reload_clear = function(){
            if (angular.isDefined(admin_image_interval_promise)) {
                $interval.cancel(admin_image_interval_promise);
                admin_image_interval_promise = undefined;
            }
        };

        $scope.get_parse_name = function(email){
            var user_email = "" + email;
            var res = user_email.replace("@", "_");
            var img_name = res.replace(".", "_");

            return img_name;
        };

        //functions - IF -----------------------------------------------------------------------------------------------
        $scope.if_showAdminData = function(data){
            var show_flag = false;

            if(data != null && data != undefined) {
                show_flag = true;
            }

            return show_flag;
        };

        //functions - Watch --------------------------------------------------------------------------------------------
        $scope.$watch('files', function () {
            $scope.upload_file($scope.files);
        });

        //destroy ------------------------------------------------------------------------------------------------------
        $scope.$on('$destroy', function() {
            $scope.admin_image_reload_clear();
        });
    });