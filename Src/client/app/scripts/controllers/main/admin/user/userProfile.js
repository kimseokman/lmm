/**
 * Created by ksm on 2015-04-21.
 */
'use strict';

angular.module('lmmApp')
    .controller('UserProfileCtrl', function ($scope, $rootScope, $location, $route, $timeout, SharedService, Users, DeviceGroups, DeviceGroupsConn, ngTableParams) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-admin-userProfile';

        //inherit data -------------------------------------------------------------------------------------------------
        $scope.$on("USER", function(){
            $scope.user = SharedService.user
        });
        $scope.$on("CONFIG", function(){
            $scope.config_str = SharedService.str;
        });
        $scope.$on("LOGO", function(){
            $scope.logo_img_uri = SharedService.logo_src;
        });

        //default Setting ----------------------------------------------------------------------------------------------
        $scope.adminPageName = $route.current.params.adminPageName;// 'user'
        $scope.detailPageName = $route.current.params.detailPageName;// 'profile'

        $scope.conn_guide_msg = "allows the member and the device group mapping";

        $scope.user_profile = {//profile profile
            guide_msg: "",
            id: {name: "E-mail", type: "text", id: "profile_id", isRequired: false, field: "mail", value: null, empty: "EMPTY"},
            password: {name: "Password", type: "password", id: "profile_pw", isRequired: true, edit_flag: false, field: "pw", value: null, empty: "EMPTY", status_class: ""},
            retype_password: {name: "Retype Password", type: "password", id: "profile_re_pw", isRequired: true, edit_flag: false, save_flag: false,  field: "pw", value: null, empty: "EMPTY", status_class: ""},
            step2: {name: "2-Step Verification", type: "text", id: "profile_2step", isRequired: false, field: "flag_use_otp", value: null, empty: "EMPTY"},
            time_zone: {name: "Time Zone", type: "text", id: "profile_timezone", isRequired: false, field: "time_zone_id", value: null, empty: "EMPTY"},
            date_fmt: {name: "Date Format", type: "text", id: "profile_dateformat", isRequired: false, field: "date_fmt_id", value: null, empty: "EMPTY"},
            refresh: {name: "Refresh", type: "text", id: "profile_refresh", isRequired: false, field: "flag_refresh", value: null, empty: "EMPTY"},
            refresh_interval: {name: "Refresh Time", type: "text", id: "profile_refresh_interval", isRequired: false, field: "refresh_interval", value: null, empty: "EMPTY"},
            duplicate_login: {name: "Duplicate Login", type: "text", id: "profile_duplicate_login", isRequired: false, field: "flag_duplicate_prevent", value: null, empty: "EMPTY"},
            name: {name: "Name", type: "text", id: "profile_name", isRequired: false, field: "name", value: null, empty: "EMPTY"},
            phone: {name: "Mobile Phone", type: "text", id: "profile_phone", isRequired: false, field: "phone", value: null, empty: "EMPTY"},
            tel: {name: "Office Phone", type: "text", id: "profile_tel", isRequired: false, field: "tel", value: null, empty: "EMPTY"},
            position: {name: "Position", type: "text", id: "profile_position", isRequired: false, field: "position", value: null, empty: "EMPTY"},
            department: {name: "Department", type: "text", id: "profile_department", isRequired: false, field: "department", value: null, empty: "EMPTY"},
            img_refresh: {name: "Image Refresh", type: "text", id: "profile_img_refresh", isRequired: false, field: "flag_img_refresh", value: null, empty: "EMPTY"},
            img_refresh_interval: {name: "Image Refresh Time", type: "text", id: "profile_img_refresh_interval", isRequired: false, field: "img_refresh_interval", value: null, empty: "EMPTY"},
            dev_vol: {name: "Device Volume", type: "text", id: "dev_vol", isRequired: false, field: "dev_vol", value: null, empty: "UNKNOWN"},
            dev_cnt: {name: "Device Count", type: "text", id: "dev_cnt", isRequired: false, field: "dev_cnt", value: null, empty: "EMPTY"},
            create_member_vol: {name: "User Volume", type: "text", id: "create_member_vol", isRequired: false, field: "create_member_vol", value: null, empty: "UNKNOWN"},
            create_member_cnt: {name: "User Count", type: "text", id: "create_member_cnt", isRequired: false, field: "create_member_cnt", value: null, empty: "EMPTY"}
        };

        $scope.deviceGroupTableHeader = [
            {name: "No.", className: "no", sortable: false},
            {name: "Name", className: "name", sortable: true, field: "name"},
            {name: "Create Time", className: "create_time", sortable: true, field: "create_time"}
        ];

        $scope.deviceGroupTableCol = $scope.deviceGroupTableHeader.length;//device table head col count
        $scope.deviceGroupTableLimits = [
            {value: 10},
            {value: 25},
            {value: 50},
            {value: 100}
        ];
        $scope.deviceGroupTableLimit = $scope.deviceGroupTableLimits[0];//set default limit line

        //default - data -----------------------------------------------------------------------------------------------
        var queryString = $location.search();
        if(queryString["member"] != undefined){
            $scope.member_pk = queryString["member"];
        }

        $scope.$watch('member_pk', function(newVal, newOld){
            $scope.member_pk = newVal;
            Users.get({pk: $scope.member_pk}, function(data){
                if(data.status == 200){
                    $scope.member = data.objects;
                }
            });

            if($scope.deviceGroupTable == undefined){
                $scope.deviceGroupTable = new ngTableParams({//get device
                    page: 1,//show first page
                    count: $scope.deviceGroupTableLimit.value,//count per page
                    sorting: {}//initial sorting
                }, {
                    total: 0,// length of data
                    getData: function ($defer, params) {
                        var dg_arg = {};
                        dg_arg['limit'] = params.count();
                        dg_arg['offset'] = (params.page() - 1) * dg_arg['limit'];
                        dg_arg["search_mode"] = "pagination";
                        dg_arg["member_pk"] = $scope.member_pk;

                        var sorting_order = 'desc';//default order
                        var sorting_by = 'create_time';//default by

                        var sorting = params.sorting();
                        for (var k in sorting) {
                            var v = sorting[k];

                            sorting_order = v;
                            sorting_by = k;
                        }

                        dg_arg['order'] = sorting_order;
                        dg_arg['by'] = sorting_by;

                        DeviceGroups.get(dg_arg, function(data){
                            $timeout(function () {
                                $scope.member_device_groups = data.objects;

                                angular.forEach($scope.member_device_groups, function(v, k){
                                    var conn_dg_arg = {};
                                    conn_dg_arg["member_fk"] = $scope.member_pk;
                                    conn_dg_arg["device_group_fk"] = v.pk;
                                    DeviceGroupsConn.get(conn_dg_arg, function(data){
                                        if(data.status == 200){
                                            $scope.member_device_groups[k].active = true;
                                        }else{
                                            $scope.member_device_groups[k].active = false;
                                        }
                                    });
                                });

                                params.total(data.meta.total_count);
                                $defer.resolve($scope.member_device_groups);
                            });
                        });
                    }
                });
            }else{
                $scope.deviceGroupTable.reload();
            }
        });

        //functions - IF -----------------------------------------------------------------------------------------------
        $scope.if_showMemberProfileData = function(data){
            var show_flag = false;

            if(data != null && data != undefined) {
                show_flag = true;
            }

            return show_flag;
        };

        $scope.if_showDeviceGroupData = function(data){
            var show_flag = false;

            if(data != null && data != undefined) {
                show_flag = true;
            }

            return show_flag;
        };

        //functions ----------------------------------------------------------------------------------------------------
        $scope.sign_device_group = function(item){
            $scope.conn_device_group_pk = item.pk;

            if(item.active){
                var conn_dg_add_arg = {};
                conn_dg_add_arg["member_fk"] = $scope.member_pk;
                conn_dg_add_arg["device_group_fk"] = $scope.conn_device_group_pk;
                DeviceGroupsConn.save(conn_dg_add_arg, function(data){
                    if(data.status == 200){
                        $scope.conn_guide_msg = "success";
                    }else{
                        $scope.conn_guide_msg = "fail";
                    }
                });
            }else{
                var conn_dg_arg = {};
                conn_dg_arg["member_fk"] = $scope.member_pk;
                conn_dg_arg["device_group_fk"] = $scope.conn_device_group_pk;
                DeviceGroupsConn.get(conn_dg_arg, function(data){
                    if(data.status == 200){
                        $scope.conn_member_n_device_group = data.objects;

                        angular.forEach($scope.conn_member_n_device_group, function(v, k){
                            DeviceGroupsConn.remove({pk: v.pk}, function(data){
                                if(data.status == 200){
                                    $scope.conn_guide_msg = "success";
                                }else{
                                    $scope.conn_guide_msg = "fail";
                                }
                            });
                        });
                    }
                });
            }
        };

        $scope.editPassword = function(){
            $scope.user_profile.password.edit_flag = true;
            $scope.user_profile.retype_password.edit_flag = false;
            $scope.user_profile.retype_password.save_flag = false;
        };

        $scope.cancelEditPassword = function(){
            $scope.user_profile.password.value = null;
            $scope.user_profile.password.edit_flag = false;
            $scope.user_profile.retype_password.value = null;
            $scope.user_profile.retype_password.edit_flag = false;
            $scope.user_profile.retype_password.save_flag = false;

            $scope.validatePassword();
        };

        $scope.validatePassword = function(){
            if($scope.user_profile.password.value == null || $scope.user_profile.password.value == ""){
                $scope.user_profile.password.status_class = "";

                $scope.user_profile.retype_password.status_class = "";

            }else{
                $scope.user_profile.password.status_class = "brand-success border-success ver_success";
                $scope.user_profile.retype_password.edit_flag = true;

                if($scope.user_profile.retype_password.value == null || $scope.user_profile.retype_password.value == ""){
                    $scope.user_profile.retype_password.status_class = "brand-warning border-warning";
                }else{
                    if($scope.user_profile.password.value != $scope.user_profile.retype_password.value){
                        $scope.user_profile.retype_password.status_class = "brand-danger border-danger ver_danger";
                    }else{
                        $scope.user_profile.retype_password.status_class = "brand-success border-success ver_success";
                        $scope.user_profile.retype_password.save_flag = true;
                    }
                }
            }
        };

        $scope.updateUserByDB = function(field, data){
            var base_pk = $scope.member_pk;
            var user_arg = {};
            user_arg[field] = data;
            var validate_msg = "";
            //validate
            if(field=="refresh_interval"){
                if(data < 3000){
                    validate_msg = "Can not be set to less than 3000.";
                    return validate_msg;
                }
            }

            if(field=="img_refresh_interval"){
                if(data < 5){
                    validate_msg = "Can not be set to less than 5.";
                    return validate_msg;
                }
            }

            Users.update({pk: base_pk}, user_arg, function(data){
                if(data.status == 200){
                    var change_label = "";

                    switch(field){
                        case "pw":
                            change_label = "Password";
                            $scope.cancelEditPassword();
                            break;
                        case "flag_use_otp":
                            change_label = "2-Step Verification";
                            break;
                        case "time_zone_id":
                            change_label = "Time Zone";
                            break;
                        case "date_fmt_id":
                            change_label = "Date Format";
                            break;
                        case "flag_duplicate_prevent":
                            change_label = "Duplicate Login";
                            break;
                        case "name":
                            change_label = "Name";
                            break;
                        case "phone":
                            change_label = "Mobile Phone";
                            break;
                        case "tel":
                            change_label = "Office Phone";
                            break;
                        case "position":
                            change_label = "Position";
                            break;
                        case "department":
                            change_label = "Department";
                            break;
                        case "flag_refresh":
                            change_label = "Refresh";
                            break;
                        case "refresh_interval":
                            change_label = "Refresh Time";
                            break;
                        case "flag_img_refresh":
                            change_label = "Image Refresh";
                            break;
                        case "img_refresh_interval":
                            change_label = "Image Refresh Time";
                            break;
                        case "flag_recept_mail":
                            change_label = "Reception Mail";
                            break;
                    }

                    $scope.user_profile.guide_msg = change_label + " changed";
                }
            });
        };

        $scope.otp_reset = function(){
            var base_pk = $scope.member_pk;
            var user_arg = {};
            user_arg["flag_create_qrcode"] = false;
            Users.update({pk: base_pk}, user_arg, function(data){
                if(data.status == 200){
                    $scope.user_profile.guide_msg = "OTP Reset";
                }
            });
        };

        //destroy ------------------------------------------------------------------------------------------------------
        $scope.$on('$destroy', function() {
            $location.search("member", null);
        });
    });