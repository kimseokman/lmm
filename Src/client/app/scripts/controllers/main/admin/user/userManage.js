/**
 * Created by ksm on 2015-03-26.
 */
'use strict';

angular.module('lmmApp')
    .controller('UserManageCtrl', function ($scope, $rootScope, $route, $location, $timeout, ngTableParams, SharedService, Modal, Users) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-admin-userManage';

        //inherit data -------------------------------------------------------------------------------------------------
        $scope.$on("USER", function(){
            $scope.user = SharedService.user
        });
        $scope.$on("CONFIG", function(){
            $scope.config_str = SharedService.str;
        });
        $scope.$on("LOGO", function(){
            $scope.logo_img_uri = SharedService.logo_src;
        });

        //default Setting ----------------------------------------------------------------------------------------------
        $scope.adminPageName = $route.current.params.adminPageName;// 'user'
        $scope.detailPageName = $route.current.params.detailPageName;// 'manage'
        $scope.userManagePage = {
            view_mode: false    //view mode flag - false:pagination, true:normal
        };

        $scope.userManageTableHeader = [
            {name: "No.", className: "no", sortable: false},
            {name: "ID(E-mail)", className: "mail", sortable: false},
            {name: "Phone", className: "phone", sortable: false},
            {name: "Status", className: "status", sortable: false},
            {name: "Delete", className: "del", sortable: false}
        ];

        $scope.userManageTableCol = $scope.userManageTableHeader.length;//device table head col count
        $scope.userManageTableLimits = [
            {value: 10},
            {value: 25},
            {value: 50},
            {value: 100}
        ];
        $scope.userManageTableLimit = $scope.userManageTableLimits[0];//set default limit line

        //default data -------------------------------------------------------------------------------------------------
        $scope.updateUserByDB_Manager = function(field, data, pk){
            var user_update_arg_byManager = {};
            user_update_arg_byManager[field] = data;
            Users.update({pk: pk}, user_update_arg_byManager, function(data){
               //console.log(data.status);
            });
        };

        $scope.removeUserByDB_Manager = function(pk){
            Users.remove({pk: pk}, function(data){
                if(data.status == 200){
                    $scope.userManageTable.reload();
                }
            });
        };

        $scope.$watch('userManageTable', function(newVal, oldVal){
            if($scope.userManageTable == undefined){
                $scope.userManageTable = new ngTableParams({//get device
                    page: 1,//show first page
                    count: $scope.userManageTableLimit.value,//count per page
                    sorting: {}//initial sorting
                }, {
                    total: 0,// length of data
                    getData: function ($defer, params) {
                        var user_manage_arg = {};
                        user_manage_arg['limit'] = params.count();
                        user_manage_arg['offset'] = (params.page() - 1) * user_manage_arg['limit'];
                        user_manage_arg['search_mode'] = ($scope.userManagePage.view_mode) ? 'normal' : 'pagination';
                        user_manage_arg['flag_servant'] = true;
                        var sorting_order = 'desc';//default order
                        var sorting_by = 'pk';//default by

                        var sorting = params.sorting();
                        for (var k in sorting) {
                            var v = sorting[k];

                            sorting_order = v;
                            sorting_by = k;
                        }

                        user_manage_arg['order'] = sorting_order;
                        user_manage_arg['by'] = sorting_by;

                        Users.get(user_manage_arg, function(data){
                            $timeout(function () {
                                $scope.dataSet = data.objects;
                                params.total(data.meta.total_count);
                                $defer.resolve($scope.dataSet);
                            });
                        });
                    }
                });
            }else{
                $scope.userManageTable.reload();
            }
        });

        //functions - IF -----------------------------------------------------------------------------------------------
        $scope.if_showUserManageData = function(pk){
            var show_flag = false;

            if(pk != 'EMPTY'){
                show_flag = true;
            }

            return show_flag;
        };

        //functions ----------------------------------------------------------------------------------------------------
        $scope.moveUserDetailPage = function(user_pk){
            var change_uri = 'main/admin/user/profile';

            $location.path(change_uri).search({member: user_pk});
        };

        //functions - Watch --------------------------------------------------------------------------------------------

        //functions - Modal --------------------------------------------------------------------------------------------
        $scope.openDialogConfirm_atUserManagement = function(item){//dialog confirm
            $scope.data = null;
            Modal.open(
                'views/modal/dialogConfirm.html',
                'DialogConfirmCtrl',
                'lg',
                {
                    type: function(){
                        return "MemberRemove";
                    },
                    data: function(){
                        return item;
                    }
                },
                function(data){
                    $scope.data = data;
                    if($scope.data.msg == "ok"){
                        $scope.removeUserByDB_Manager($scope.data.pk);
                    }
                }
            );
        };
    });