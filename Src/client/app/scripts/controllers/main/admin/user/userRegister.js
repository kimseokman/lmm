/**
 * Created by ksm on 2015-04-27.
 */
'use strict';

angular.module('lmmApp')
    .controller('UserRegCtrl', function ($scope, $route, $location, SharedService, Modal, Check, Users){
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-admin-userManage';

        //inherit data -------------------------------------------------------------------------------------------------
        $scope.$on("USER", function(){
            $scope.user = SharedService.user
        });
        $scope.$on("CONFIG", function(){
            $scope.config_str = SharedService.str;
        });
        $scope.$on("LOGO", function(){
            $scope.logo_img_uri = SharedService.logo_src;
        });

        //default Setting ----------------------------------------------------------------------------------------------
        $scope.adminPageName = $route.current.params.adminPageName;// 'user'
        $scope.detailPageName = $route.current.params.detailPageName;// 'register'

        $scope.reg_user_info = {
            guide_msg: "",
            mail: {name: "E-mail", type: "email", id: "user_reg_id", isRequired: false, field: "mail", value: null, status:0, notice: null, empty: "EMPTY"},
            recept_mail: {name: "Reception Mail", type: "text", id: "user_reg_recept_mail", isRequired: false, field: "flag_recept_mail", value: true, status:0, notice: null, empty: "EMPTY"},
            password: {name: "Password", type: "password", id: "user_reg_pw", isRequired: true, edit_flag: false, field: "pw", value: null, status:0, notice: null, empty: "EMPTY", status_class: ""},
            retype_password: {name: "Retype Password", type: "password", id: "user_reg_re_pw", isRequired: true, edit_flag: false, save_flag: false,  field: "pw", value: null, status:0, notice: null, empty: "EMPTY", status_class: ""},
            duplicate_login: {name: "Duplicate Login", type: "text", id: "user_reg_duplicate_login", isRequired: false, field: "flag_duplicate_prevent", value: null, status:0, notice: null, empty: "EMPTY"},
            name: {name: "Name", type: "text", id: "user_reg_name", isRequired: false, field: "name", value: null, status:0, notice: null, empty: "EMPTY"},
            phone: {name: "Mobile Phone", type: "text", id: "user_reg_phone", isRequired: false, field: "phone", value: null, status:0, notice: null, empty: "EMPTY"},
            tel: {name: "Office Phone", type: "text", id: "user_reg_tel", isRequired: false, field: "tel", value: null, status:0, notice: null, empty: "EMPTY"},
            position: {name: "Position", type: "text", id: "user_reg_position", isRequired: false, field: "position", value: null, status:0, notice: null, empty: "EMPTY"},
            department: {name: "Department", type: "text", id: "user_reg_department", isRequired: false, field: "department", value: null, status:0, notice: null, empty: "EMPTY"}
        };

        //functions - Content ------------------------------------------------------------------------------------------
        $scope.checkInputUserMail = function() {//check
            var strMail = '' + $scope.reg_user_info.mail.value;
            if($scope.reg_user_info.mail.value == null){
                $scope.reg_user_info.mail.status = 1;
                $scope.reg_user_info.mail.notice = 'Email is a required property.';
            }

            var arg_mail = {};
            arg_mail['type'] = "mail";
            arg_mail['value'] = $scope.reg_user_info.mail.value;
            console.log("test");
            Check.get(arg_mail, function(data){
                var check = data.objects;
                console.log(check);
                if(check.available){
                    $scope.reg_user_info.mail.status = 3;
                    $scope.reg_user_info.mail.notice = 'OK';

                    if(strMail.indexOf('@') == -1){
                        $scope.reg_user_info.mail.status = 1;
                        $scope.reg_user_info.mail.notice = 'Is not a suitable format.';
                    }
                }else{
                    $scope.reg_user_info.mail.status = 2;
                    $scope.reg_user_info.mail.notice = 'Email is duplicate';
                }
            });
        };

        $scope.checkInputUserPW = function(){//check
            if($scope.reg_user_info.password.value == null){
                $scope.reg_user_info.password.status = 1;
                $scope.reg_user_info.password.notice = 'Password is a required property.';
                return
            } else if($scope.reg_user_info.password.value == ''){
                $scope.reg_user_info.password.status = 1;
                $scope.reg_user_info.password.notice = 'Can not be left blank.';
                return
            }else{
                $scope.reg_user_info.password.status = 3;
                $scope.reg_user_info.password.notice = 'OK';
            }

            if($scope.reg_user_info.retype_password.value == null){
                $scope.reg_user_info.retype_password.status = 1;
                $scope.reg_user_info.retype_password.notice = 'Password is a required property.';
                return
            }else if($scope.reg_user_info.retype_password.value == ''){
                $scope.reg_user_info.retype_password.status = 1;
                $scope.reg_user_info.retype_password.notice = 'Can not be left blank.';
                return
            }

            if($scope.reg_user_info.retype_password.value != $scope.reg_user_info.password.value){
                $scope.reg_user_info.retype_password.status = 2;
                $scope.reg_user_info.retype_password.notice = 'Incorrect input';
                return
            }

            $scope.reg_user_info.retype_password.status = 3;
            $scope.reg_user_info.retype_password.notice = 'OK';
        };

        $scope.checkInputUser = function(){
            $scope.checkInputUserMail();
            $scope.checkInputUserPW();
        };

        $scope.signUpUserDisabled = function(){//signup validate
            var flag_validate = false;

            if($scope.reg_user_info.mail.status != 3)  flag_validate = true;
            if($scope.reg_user_info.password.status != 3)  flag_validate = true;
            if($scope.reg_user_info.retype_password.status != 3)  flag_validate = true;

            return flag_validate;
        };

        $scope.signUpUser = function(){
            console.log("test");
            var sign_user_arg = {};
            sign_user_arg[$scope.reg_user_info.mail.field] = $scope.reg_user_info.mail.value;
            sign_user_arg[$scope.reg_user_info.recept_mail.field] = $scope.reg_user_info.recept_mail.value;
            sign_user_arg[$scope.reg_user_info.password.field] = $scope.reg_user_info.password.value;
            //sign_user_arg[$scope.reg_user_info.duplicate_login.field] = $scope.reg_user_info.duplicate_login.value;
            sign_user_arg[$scope.reg_user_info.name.field] = $scope.reg_user_info.name.value;
            sign_user_arg[$scope.reg_user_info.phone.field] = $scope.reg_user_info.phone.value;
            sign_user_arg[$scope.reg_user_info.tel.field] = $scope.reg_user_info.tel.value;
            sign_user_arg[$scope.reg_user_info.position.field] = $scope.reg_user_info.position.value;
            sign_user_arg[$scope.reg_user_info.department.field] = $scope.reg_user_info.department.value;

            Users.save(sign_user_arg, function(data){
               if(data.status == 200){
                   $location.path('/main/admin/user/manage', true);
               }
            });
        };
        //functions - Modal --------------------------------------------------------------------------------------------
        $scope.openDialogConfirm_AddUser = function(){
            Modal.open(
                "views/modal/dialogConfirm_addUser.html",
                "DialogConfirmAddUserCtrl",
                "lg",
                {
                    reg_user: function(){
                        return $scope.reg_user_info;
                    }
                },
                function(msg){
                    $scope.dialogConfirmAddUserResultMsg = msg;
                    if($scope.dialogConfirmAddUserResultMsg == "ok"){
                        $scope.signUpUser();
                    }
                }
            );
        };
    });
