/**
 * Created by ksm on 2015-02-09.
 */
'use strict';

angular.module('lmmApp')
    .controller('AdminCtrl', function ($scope, $rootScope, $upload, ENV, COM, SharedService) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-admin';

        //inherit data -------------------------------------------------------------------------------------------------
        $scope.$parent.updateInformation();
        $scope.$parent.updatePath();

        $scope.$on("USER", function(){
            $scope.user = SharedService.user;
        });
        $scope.$on("CONFIG", function(){
            $scope.config_str = SharedService.str;
        });
        $scope.$on("LOGO", function(){
            $scope.logo_img_uri = SharedService.logo_src;
        });

        //default Setting ----------------------------------------------------------------------------------------------
        $scope.adminPageName = 'profile';//default page name set
        $scope.detailPageName = false;

        $scope.admin_side_menu = [
            {icon: "users", name: "User Management", active_tag: 'main/admin/user', uri: 'main/admin/user/manage'},
            {icon: "dollar", name: "Billing", active_tag: 'main/admin/billing', uri: 'main/admin/billing'},
            {icon: "user", name: "Profile", active_tag: 'main/admin/profile', uri: 'main/admin/profile'}
        ];


        //default Data -------------------------------------------------------------------------------------------------

        //functions - Side ---------------------------------------------------------------------------------------------

        //functions - Content ------------------------------------------------------------------------------------------

        //functions - If -----------------------------------------------------------------------------------------------
        $scope.if_showAdminSide = function(){
            var show_flag = false;

            if(true) {
                show_flag = true;
            }

            return show_flag;
        };

        //functions - Modal --------------------------------------------------------------------------------------------
    });
