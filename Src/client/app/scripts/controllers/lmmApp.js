/**
 * Created by ksm on 2015-01-26.
 */
'use strict';

angular.module('lmmApp')
    .controller('LmmAppCtrl', function ($scope, $rootScope, $route, $http, $timeout, $translate, $filter, $window, $location, ENV, COM, Session, Access, Devices, Users, Modal, SharedService) {
        //common default Setting ---------------------------------------------------------------------------------------
        $scope.pageClass = 'page-parent';
        //common uris
        $scope.loading_uri = COM.uri.loading;
        $scope.header_uri = COM.uri.header;
        $scope.footer_uri = COM.uri.footer;
        $scope.deviceSide_uri = COM.uri.device_side;
        $scope.liveSide_uri = COM.uri.live_side;
        $scope.reportSide_uri = COM.uri.report_side;
        $scope.alertSide_uri = COM.uri.alert_side;
        $scope.adminSide_uri = COM.uri.admin_side;
        $scope.helpSide_uri = COM.uri.help_side;
        //relation env
        $scope.showLiveStatusFlag = false;

        $scope.adminPageName = 'profile';   //default set
        $scope.helpPageName = 'resource';

        $scope.config_str = {
            EMPTY: "EMPTY",
            undefined: "undefined"
        };
        SharedService.prepForConfigStringBroadcast($scope.config_str);

        $scope.logo_img_uri = $scope.config_str.EMPTY;
        SharedService.prepForLogoBroadcast($scope.logo_img_uri);

        $scope.appInfo = {//strings
            ver: '0.0.1'
        };

        $scope.langs = {//language label scaffold
            en_US: "English",
            ko: "한국어",
            ja_JP: "日本語"
        };

        $scope.familySites = [
            {text: "-- Family Sites --", src: "none"},
            {text: "AnySupport", src: "anysupport.net"},
            {text: "PCAnyPro", src: "pcanypro.net"},
            {text: "Liveweb", src: "liveweb.co.kr"},
            {text: "Koino", src: "koino.net"}
        ];

        $scope.familySite = $scope.familySites[0];

        $scope.setting = COM.setting;

        $scope.agents = [
            {
                os: "Windows",
                icon: "windows",
                icon_style: {color: '#23b7e5'},
                ver_label: "Version",
                ver: "0.0.1",
                size_label: "Size",
                size: 12,
                unit: "MB",
                file_src: null
            },
            {
                os: "Linux",
                icon: "linux",
                icon_style: {color: '#1c2b36'},
                ver_label: "Version",
                ver: "0.0.1",
                size_label: "Size",
                size: 13,
                unit: "MB",
                file_src: null
            },
            {
                os: "OS X",
                icon: "apple",
                icon_style: {color: '#bbb'},
                ver_label: "Version",
                ver: "0.0.1",
                size_label: "Size",
                size: 15,
                unit: "MB",
                file_src: null
            },
            {
                os: "Android",
                icon: "android",
                icon_style: {color: '#27c24c'},
                ver_label: "Version",
                ver: "0.0.1",
                size_label: "Size",
                size: 13.4,
                unit: "MB",
                file_src: null
            }
        ];

        //Authenticate
        $scope.isAuthenticated = Access.isAuthenticated();
        if($scope.isAuthenticated){
            $scope.token = Session.get("token");
            $http.defaults.headers.get = {token: $scope.token};
            $http.defaults.headers.get['If-Modified-Since'] = new Date().getTime();
            $http.defaults.headers.get['Cache-Control'] = 'no-cache';
            $http.defaults.headers.get['Pragma'] = 'no-cache';
            $http.defaults.headers.put["token"] = $scope.token;
            $http.defaults.headers.post["token"] = $scope.token;
            $http.defaults.headers.common["token"] = $scope.token;

            //base device
            Devices.get({pk: 0}, function(data){
                $scope.base_device = data.objects;
                $scope.devicePK = $scope.base_device.pk;
                $scope.deviceVer = $scope.showDeviceOSStatus($scope.base_device.dev_os_id);
            });
        }

        //functions - Watch --------------------------------------------------------------------------------------------
        $scope.$watch(Access.isAuthenticated, function(v){//watch - isAuthenticated at services/api/access.js
            $scope.isAuthenticated = v;
            if(!v){
                $scope.userType = "Guest";
            }
        });

        //functions - common filter ------------------------------------------------------------------------------------
        $scope.showUserActiveStatus = function (value) {
            var selected = $filter('filter')($scope.setting.active.statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                return selected[0].text;
            }
        };

        $scope.showUserAuthStatus = function (value) {
            var selected = [];
            var value = value;

            if(value != undefined){
                angular.forEach($scope.setting.user_auth_type.statuses, function(v) {
                    if (value && v.value) {
                        selected.push(v.text);
                    }
                });
            }

            return selected.length ? selected.join(', ') : 'Not set';
        };

        $scope.showUserTypeStatus = function(value) {
            var selected = $filter('filter')($scope.setting.user_type.statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                return selected[0].text;
            }
        };

        $scope.showOnOffStatus = function(value) {
            var selected = $filter('filter')($scope.setting.on_off.statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                return selected[0].text;
            }
        };

        $scope.showTimeZoneStatus = function(value) {
            var selected = $filter('filter')($scope.setting.time_zone.statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                return selected[0].simple_text;
            }
        };

        $scope.showTimeZoneData = function(value) {
            var selected = $filter('filter')($scope.setting.time_zone.statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                return selected[0].data;
            }
        };

        $scope.showDateFormatStatus = function(value) {
            var selected = $filter('filter')($scope.setting.date_fmt.statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                return selected[0].text;
            }
        };

        $scope.showTimeFormatStatus = function(value) {
            if(value == undefined){
                value = 0;
            }

            var selected = $filter('filter')($scope.setting.time_fmt.statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                return selected[0].text;
            }
        };

        $scope.showAuditActName = function(value) {
            var selected = $filter('filter')($scope.setting.audit_act_name.statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                return selected[0].text;
            }
        };

        $scope.showAuditActType = function(value) {
            var selected = $filter('filter')($scope.setting.audit_act_type.statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                return selected[0].text;
            }
        };

        $scope.showNumberData = function(value){
            var to_string = "" + value;

            if(value == undefined || value == null){
                to_string = $scope.config_str.EMPTY;
            }

            return to_string;
        };

        $scope.showDeviceGroupsStatus = function(groups, pk){
            var selected = $filter('filter')(groups, {pk: pk});

            if(pk == undefined){
                return 'Error';
            }else{
                return selected[0].name;
            }
        };

        $scope.showDeviceOSStatus = function(value){
            var selected = $filter('filter')($scope.setting.device.os_statuses, {value: value});

            if(value == undefined){
                return 'Not Set';
            }else{
                return selected[0].text;
            }
        };
        //functions - common -------------------------------------------------------------------------------------------
        $scope.makeAuditDetail = function(name_id, type_id, oldValue, newValue){
            var show_string = "";
            var old_v = "" + oldValue;
            if(old_v == "null") old_v = "UNKNOWN";
            var new_v = "" + newValue;
            if(new_v == "null") new_v = "UNKNOWN";

            show_string = "(" + old_v + "→" + new_v + ")";

            return show_string;
        };

        $scope.if_showData = function(data){
            var show_flag = false;

            if(data != undefined && data != null && data != ""){
                show_flag = true;
            }

            return show_flag;
        };

        $scope.routeFamilySite = function(src){
            var src = "http://" + src;
            if(src.indexOf("none") == -1)
                $window.open(src, '_blank');
        };

        $scope.onActiveMenu = function (currentTag) {//current focus
            var fullUrl = $location.absUrl();

            if(fullUrl.indexOf(currentTag) != -1){
                return true;
            }else{
                return false;
            }
        };

        $scope.getProcessProgressType = function(ratio){
            var type = '';

            if (ratio < 25) {
                type = 'success';
            } else if (ratio < 50) {
                type = 'info';
            } else if (ratio < 75) {
                type = 'warning';
            } else {
                type = 'danger';
            }

            return type;
        };

        $scope.selectLang = $scope.langs[$translate.proposedLanguage()] || "English";

        $scope.setLang = function(langKey) {//set language
            $scope.selectLang = $scope.langs[langKey];
            $translate.use(langKey);
        };

        $scope.movePage = function(toPage){//move page
            $location.search("key", null);  //email auth key remove
            $location.search("member", null);  //email auth key remove

            var change_uri = toPage;
            $location.path(change_uri);
        };

        $scope.movePath = function(pathInfo, targetPath, defaultPath){//link path
            var change_uri = '';

            var keepGoing = true;
            angular.forEach(pathInfo, function(v, k){
                if(keepGoing){
                    change_uri += '/' + pathInfo[k].name;
                    if(k == targetPath){
                        keepGoing = false;
                        if(defaultPath){
                            change_uri += '/' + defaultPath;
                        }
                    }
                }
            });
            //console.log(change_uri);
            $location.path(change_uri);
        };

        $scope.directRefresh = function(){
            $route.reload();
        };

        $scope.tempConverter = function(to_temp_flag, temp){
            var F = 0.0;
            var C = 0.0;

            if(to_temp_flag == 'F'){//to fahrenheit ℉
                F = temp * 9.0 / 5.0 + 32;
                return F;
            }else if(to_temp_flag == 'C'){//to celsius ℃
                C = (temp - 32) * 5.0 / 9.0;
                return C;
            }
        };

        $scope.get_parse_name = function(email){
            var user_email = "" + email;
            var res = user_email.replace("@", "_");
            var img_name = res.replace(".", "_");

            return img_name;
        };

        $scope.exist_file_check = function(url){
            var exist_flag = false;
            var request = new XMLHttpRequest();
            request.open('HEAD', url, false);
            request.send();
            if(request.status == 200) {
                exist_flag = true;
            } else {
                exist_flag = false;
            }

            return exist_flag;
        };

        //functions - Modal --------------------------------------------------------------------------------------------
        $scope.openDialogConfirm = function(item){//dialog confirm
            $scope.data = null;
            Modal.open(
                'views/modal/dialogConfirm.html',
                'DialogConfirmCtrl',
                'lg',
                {
                    type: function(){
                        return "normal";
                    },
                    data: function(){
                        return item;
                    }
                },
                function(data){
                    $scope.data = data;
                }
            );
        };

        $scope.openDialogNotify = function(type){//dialog notify
            Modal.open(
                'views/modal/dialogNotify.html',
                'DialogNotifyCtrl',
                'lg',
                {
                    type: function(){
                        return type;
                    }
                }
            );
        };

        $scope.openDeviceRemoteControlModal = function(device){//modal - Remote Control
            Modal.open(
                "views/modal/remoteControl.html",
                "RemoteControlCtrl",
                "lg",
                {
                    device: function(){
                        return device;
                    }
                }
            );
        };

        //functions - update -------------------------------------------------------------------------------------------
        $scope.updatePath = function(){//update path info
            var full_uri = $location.absUrl();
            var uri_split = full_uri.split('/#/');
            var current_uri = uri_split[1];
            var content_histroy = current_uri.split('/');

            $scope.thisPath = {
                step1: null,
                step2: null,
                step3: null,
                step4: null,
                step5: null
            };
            angular.forEach(content_histroy, function(v, k){
                var stepNum = Number(k)+1;
                var pathKey = 'step' + stepNum;
                $scope.thisPath[pathKey] = {name : v};
            });
            //console.log(JSON.stringify($scope.thisPath));
        };

        $scope.updateInformation = function(){//update default info
            $scope.isAuthenticated = Access.isAuthenticated();

            $scope.token = Session.get("token");// Session
            $http.defaults.headers.get = {token: $scope.token};
            $http.defaults.headers.get['If-Modified-Since'] = new Date().getTime();
            $http.defaults.headers.get['Cache-Control'] = 'no-cache';
            $http.defaults.headers.get['Pragma'] = 'no-cache';
            $http.defaults.headers.put["token"] = $scope.token;
            $http.defaults.headers.post["token"] = $scope.token;
            $http.defaults.headers.common["token"] = $scope.token;

            if($scope.isAuthenticated){
                Devices.get({pk: 0}, function(data){
                    $scope.base_device = data.objects;
                    if($scope.base_device.pk != "EMPTY"){
                        $scope.showLiveStatusFlag = true;
                        $scope.devicePK = $scope.base_device.pk;
                        $scope.deviceVer = $scope.showDeviceOSStatus($scope.base_device.dev_os_id);
                    }else{
                        $scope.showLiveStatusFlag = false;
                    }
                });

                Users.get({pk: COM.self_pk}, function(data){
                    if(data.status == 200){
                        $scope.user = data.objects;
                        SharedService.prepForUserBroadcast($scope.user);
                        var expander = ".png";
                        $scope.logo_img_uri = ENV.uploadImgUri + $scope.get_parse_name($scope.user.mail) + expander + "?cache=" + new Date().getTime();

                        if(!$scope.exist_file_check($scope.logo_img_uri)){
                            $scope.logo_img_uri = $scope.config_str.EMPTY;
                        }
                        $scope.userType = $scope.user.type;
                    }else{
                        $scope.userType = "Guest";
                        $scope.logo_img_uri = $scope.config_str.EMPTY;
                    }
                    SharedService.prepForLogoBroadcast($scope.logo_img_uri);
                });
            }else{
                $scope.userType = "Guest";
                $scope.logo_img_uri = $scope.config_str.EMPTY;
                SharedService.prepForLogoBroadcast($scope.logo_img_uri);
            }
        };

        //functions - If -----------------------------------------------------------------------------------------------
        $scope.showLoadingBar = function(flag){
            var show_flag = flag;

            return show_flag;
        };
    });