/**
 * Created by ksm on 2015-02-02.
 */
'use strict';

angular.module('lmmApp')
    .controller('ResourcesCtrl', function ($scope, $rootScope, SharedService) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-resources';

        //inherit data -------------------------------------------------------------------------------------------------
        $scope.$parent.updateInformation();

        $scope.$on("CONFIG", function(){
            $scope.config_str = SharedService.str;
        });
        $scope.$on("LOGO", function(){
            $scope.logo_img_uri = SharedService.logo_src;
        });
    });