/**
 * Created by ksm on 2015-01-26.
 */
'use strict';

angular.module('lmmApp')
  .controller('IntroCtrl', function ($scope, $rootScope, SharedService) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-intro';

        //inherit data -------------------------------------------------------------------------------------------------
        $scope.$parent.updateInformation();

        $scope.$on("CONFIG", function(){
            $scope.config_str = SharedService.str;
        });
        $scope.$on("LOGO", function(){
            $scope.logo_img_uri = SharedService.logo_src;
        });
  });
