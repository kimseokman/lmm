/**
 * Created by ksm on 2015-02-10.
 */
'use strict';

angular.module('lmmApp')
    .controller('ForgotCtrl', function ($scope, ResetPW) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-forgot';

        //default Setting ----------------------------------------------------------------------------------------------
        $scope.user = {//user data scaffold
            mail: null
        };

        angular.element('.input_email').trigger('focus');// auto focus

        //functions ----------------------------------------------------------------------------------------------------
        $scope.resetPW = function(user){//password reset mail send
            var arg = {};
            arg['mail'] = user.mail;

            ResetPW.save(arg);
        };
    });