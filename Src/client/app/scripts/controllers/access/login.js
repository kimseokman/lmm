/**
 * Created by ksm on 2015-02-02.
 */
'use strict';

angular.module('lmmApp')
    .controller('LoginCtrl', function ($scope, $cookieStore, Access) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-login';

        //default Setting ----------------------------------------------------------------------------------------------
        $scope.user = {//user data scaffold
            mail: $cookieStore.get("userMail") || null,
            pw: null,
            code: null,
            remember: $cookieStore.get("rememberMe") || false
        };

        angular.element('.input_email').trigger('focus');// auto focus

        //functions ----------------------------------------------------------------------------------------------------
        $scope.loginValidate = function(user){//login validate
            var flag_validate = false;

            if(user.mail == null)  flag_validate = true;
            if(user.pw == null)  flag_validate = true;

            return flag_validate;
        };

        $scope.login = function(user){//login
            var arg = {};
            arg['type'] = "1STEP";
            arg['mail'] = user.mail;
            arg['pw'] = user.pw;

            $cookieStore.put("rememberMe", user.remember);
            if(user.remember){//cookie set
                $cookieStore.put("userMail", user.mail);
            }else{//cookie remove
                $cookieStore.remove("userMail");
            }

            Access.auth(arg);
        };
    });