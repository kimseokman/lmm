/**
 * Created by ksm on 2015-02-10.
 */
'use strict';

angular.module('lmmApp')
    .controller('RegisterCtrl', function ($scope, $location, Managers, Check) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-register';

        //default Setting ----------------------------------------------------------------------------------------------
        $scope.register = {//register data scaffold || status - 0: normal, 1:warning, 2:danger, 3:success
            mail: {field: "mail", value: null, status: 0, notice: null},
            pw: {field: "pw", value: null, status: 0, notice: null},
            retype_pw: {value: null, status: 0, notice: null},
            agree: {value: false, status: 0, notice: null}
        };

        angular.element('.input_email').trigger('focus');// auto focus

        //functions ----------------------------------------------------------------------------------------------------
        $scope.checkInputManagerMail = function() {//check
            var strMail = '' + $scope.register.mail.value;
            if($scope.register.mail.value == null){
                $scope.register.mail.status = 1;
                $scope.register.mail.notice = 'Email is a required property.';
            }

            var arg_mail = {};
            arg_mail['type'] = "mail";
            arg_mail['value'] = $scope.register.mail.value;

            Check.get(arg_mail, function(data){
                var check = data.objects;
                if(check.available){
                    $scope.register.mail.status = 3;
                    $scope.register.mail.notice = 'OK';

                    if(strMail.indexOf('@') == -1){
                        $scope.register.mail.status = 1;
                        $scope.register.mail.notice = 'Is not a suitable format.';
                    }
                }else{
                    $scope.register.mail.status = 2;
                    $scope.register.mail.notice = 'Email is duplicate';
                }
            });
        };

        $scope.checkInputManagerPW = function(){//check
            if($scope.register.pw.value == null){
                $scope.register.pw.status = 1;
                $scope.register.pw.notice = 'Password is a required property.';
                return
            } else if($scope.register.pw.value == ''){
                $scope.register.pw.status = 1;
                $scope.register.pw.notice = 'Can not be left blank.';
                return
            }else{
                $scope.register.pw.status = 3;
                $scope.register.pw.notice = 'OK';
            }

            if($scope.register.retype_pw.value == null){
                $scope.register.retype_pw.status = 1;
                $scope.register.retype_pw.notice = 'Password is a required property.';
                return
            }else if($scope.register.retype_pw.value == ''){
                $scope.register.retype_pw.status = 1;
                $scope.register.retype_pw.notice = 'Can not be left blank.';
                return
            }

            if($scope.register.retype_pw.value != $scope.register.pw.value){
                $scope.register.retype_pw.status = 2;
                $scope.register.retype_pw.notice = 'Incorrect input';
                return
            }

            $scope.register.retype_pw.status = 3;
            $scope.register.retype_pw.notice = 'OK';
        };

        $scope.checkInputManagerAgree = function(){
            if($scope.register.agree.value == false){
                $scope.register.agree.status = 2;
                $scope.register.agree.notice = 'Must agree.';
            }else{
                $scope.register.agree.status = 3;
                $scope.register.agree.notice = 'OK';
            }
        };

        $scope.checkInputManager = function(){
            if($scope.register.mail.status != 3){
                $scope.checkInputManagerMail();
            }

            if($scope.register.pw.status != 3 || $scope.register.retype_pw.status != 3 ){
                $scope.checkInputManagerPW();
            }

            if($scope.register.agree.status != 3){
                $scope.checkInputManagerAgree();
            }
        };

        $scope.signUpManagerDisabled = function(){//signup validate
            var flag_validate = false;

            if($scope.register.mail.status != 3)  flag_validate = true;
            if($scope.register.pw.status != 3)  flag_validate = true;
            if($scope.register.retype_pw.status != 3)  flag_validate = true;
            if($scope.register.agree.status != 3)  flag_validate = true;

            return flag_validate;
        };

        $scope.signUpManager = function(){//signup
            var arg = {};
            arg[$scope.register.mail.field] = $scope.register.mail.value;
            arg[$scope.register.pw.field] = $scope.register.pw.value;

            Managers.save(arg);

            var change_uri = '/access/login';
            $location.path(change_uri, true);
        };
    });
