/**
 * Created by ksm on 2015-02-10.
 */
'use strict';

angular.module('lmmApp')
    .controller('OtpCtrl', function ($scope, $http, Access, ENV) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-otp';
        $scope.$parent.updateInformation();
        $scope.$parent.updatePath();

        //default Setting ----------------------------------------------------------------------------------------------
        $scope.qrcodeImgUri = ENV.qrcodeImgUri;
        //functions ----------------------------------------------------------------------------------------------------
        $scope.getQRCodeImgName = function(mail){
            var mail = "" + mail;
            var userID = "";
            if(mail != undefined){
                var res = mail.replace("@", "_");
                userID = res.replace(".", "_");
            }

            return userID;
        };

        $scope.login_otp = function(user){//otp login
            var arg = {};
            arg['type'] = "3STEP";
            arg['code'] = user.code;
            Access.auth_otp(arg);
        };
    });
