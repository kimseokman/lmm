/**
 * Created by ksm on 2015-02-10.
 */
'use strict';

angular.module('lmmApp')
    .controller('LogoutCtrl', function ($scope, $rootScope, $location, SharedService, COM, Users, Session) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-logout';

        //inherit data -------------------------------------------------------------------------------------------------
        $scope.$parent.updateInformation();

        $scope.$on("CONFIG", function(){
            $scope.config_str = SharedService.str;
        });
        $scope.$on("LOGO", function(){
            $scope.logo_img_uri = SharedService.logo_src;
        });

        $scope.logo_img_uri = $scope.config_str.EMPTY;
        SharedService.prepForLogoBroadcast($scope.logo_img_uri);

        //functions - logic --------------------------------------------------------------------------------------------
        var arg = {};
        arg["logout"] = "LOGOUT";
        Users.update({pk: COM.self_pk}, arg, function(data){
            if(data.status == 200){
                Session.all_clear();
                $location.path('/access/login', true);
            }
        });
    });