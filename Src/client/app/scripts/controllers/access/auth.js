/**
 * Created by ksm on 2015-02-11.
 */
'use strict';

angular.module('lmmApp')
    .controller('AuthCtrl', function ($scope, $location, Auth) {
        //controller Setting -------------------------------------------------------------------------------------------
        $scope.pageClass = 'page-auth';

        //default Setting ----------------------------------------------------------------------------------------------
        $scope.flag_auth = false;
        $scope.auth_ment = "인증 절차 진행 중...";

        //functions ----------------------------------------------------------------------------------------------------
        var queryString = $location.search();

        var arg = {};
        arg['key'] = '' + queryString['key'];
        arg['type'] = 'auth';

        Auth.save(arg, function(data){//mail auth
            var auth = data.objects;
            if(auth.result == "Fail"){
                $scope.auth_ment = "인증 실패";
                $scope.flag_auth = false;
            }else if(auth.result == "Success"){
                $scope.auth_ment = "인증 성공";
                $scope.flag_auth = true;
            }else if(auth.result == "Already"){
                $scope.auth_ment = "이미 인증 됨";
                $scope.flag_auth = true;
            }
        });
    });