/**
 * Created by ksm on 2015-02-26.
 */
'use strict';

angular.module('lmmApp')
    .controller('AddDeviceCtrl', function ($scope, $modalInstance, agents) {
        //inherit data -------------------------------------------------------------------------------------------------
        $scope.agents = agents;

        //default setting ----------------------------------------------------------------------------------------------
        $scope.add_device_guide_ment = 'Please install according to the OS';

        //functions - basic --------------------------------------------------------------------------------------------
        $scope.ok = function() {//ok
            $modalInstance.close();
        };

        $scope.close = function(){//close
            $modalInstance.dismiss('cancel');
        };

        //functions - if -----------------------------------------------------------------------------------------------
        $scope.if_disableDownBtn = function(src){
            var able_flag = true;
            if(src != undefined){
                able_flag = false;
            }
            return able_flag;
        };
    });
