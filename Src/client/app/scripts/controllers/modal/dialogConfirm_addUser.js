/**
 * Created by ksm on 2015-04-27.
 */
'use strict';

angular.module('lmmApp')
    .controller('DialogConfirmAddUserCtrl', function ($scope, $modalInstance, reg_user) {
        //inherit data -------------------------------------------------------------------------------------------------
        $scope.confirm_reg_user_info = reg_user;

        //default setting ----------------------------------------------------------------------------------------------
        $scope.add_user_guide_ment = "Please confirm the accuracy of the user's information to be generated.";

        //functions - basic --------------------------------------------------------------------------------------------
        $scope.ok = function() {//ok
            $modalInstance.close('ok');
        };

        $scope.close = function(){//close
            $modalInstance.close('no');
        };
    });