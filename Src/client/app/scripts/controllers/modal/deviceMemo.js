/**
 * Created by ksm on 2015-03-19.
 */
'use strict';

angular.module('lmmApp')
    .controller('DeviceMemoCtrl', function ($scope, $modalInstance, device, Devices) {
        //inherit data -------------------------------------------------------------------------------------------------
        $scope.this_device = device;

        //default setting ----------------------------------------------------------------------------------------------
        $scope.mode_device_memo_guide_ment = "Please click on the item that you want to modify";

        $scope.device_memo = {name: "Memo", field: "memo", empty: "Edit Memo Click"};

        //functions - content ------------------------------------------------------------------------------------------
        $scope.updateDeviceMemoByDB = function(field, data){
            var deviceMemoArg = {};
            deviceMemoArg[field] = data;

            Devices.update({pk: $scope.this_device.pk}, deviceMemoArg, function(data){
                if(data.status == 200){

                }else{
                    alert("Fail");
                }
            });
        };

        //functions - basic --------------------------------------------------------------------------------------------
        $scope.ok = function() {//ok
            $modalInstance.close('ok');
        };

        $scope.close = function(){//close
            $modalInstance.close('no');
        };
    });

