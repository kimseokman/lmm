/**
 * Created by ksm on 2015-03-02.
 */
'use strict';

angular.module('lmmApp')
    .controller('LiveDeviceCtrl', function ($scope, $modalInstance, $interval, live_device, deviceImg_uri, user) {
        $scope.liveDevice = {
            img_uri: null
        };
        //functions - content ------------------------------------------------------------------------------------------
        var live_device_modal_image_interval_promise;
        $scope.live_device_image_load = function(){
            var expander = ".png";
            $scope.liveDevice.img_uri = deviceImg_uri + live_device.guid + expander;

            if(user.flag_img_refresh){
                var delay = user.img_refresh_interval;

                $scope.liveDevice.img_uri = deviceImg_uri + live_device.guid + expander + "?cache=" + new Date().getTime();//first img set

                if ( angular.isDefined(live_device_modal_image_interval_promise) ) return;

                live_device_modal_image_interval_promise = $interval(function() {
                    if(delay <= 0){
                        delay = user.img_refresh_interval;
                        $scope.liveDevice.img_uri = deviceImg_uri + live_device.guid + expander + "?cache=" + new Date().getTime();
                    }else{
                        delay = delay - 1;
                    }
                }, 1000);
            }else{
                $scope.liveDevice.img_uri = deviceImg_uri + live_device.guid + expander + "?cache=" + new Date().getTime();
            }
        };

        $scope.live_device_image_load();

        $scope.live_device_image_reload_clear = function(){
            if (angular.isDefined(live_device_modal_image_interval_promise)) {
                $interval.cancel(live_device_modal_image_interval_promise);
                live_device_modal_image_interval_promise = undefined;
            }
        };

        //functions - basic --------------------------------------------------------------------------------------------
        $scope.ok = function() {//ok
            $modalInstance.close();
        };

        $scope.close = function(){//close
            $modalInstance.dismiss('cancel');
        };

        // destroy -----------------------------------------------------------------------------------------------------
        $scope.$on('$destroy', function() {
            $scope.live_device_image_reload_clear();
        });
    });