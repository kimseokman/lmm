/**
 * Created by ksm on 2015-02-25.
 */
'use strict';

angular.module('lmmApp')
    .controller('AddDeviceGroupCtrl', function ($scope, $modalInstance, DeviceGroups, user, groups) {
        //inherit data -------------------------------------------------------------------------------------------------
        var user = user;
        var groups = groups;

        //default setting ----------------------------------------------------------------------------------------------
        $scope.group = {//device group data scaffold
            name: null,
            name_label: "Name",
            flag_df_group: false,
            flag_confirm: false,
            member_fk: null
        };

        $scope.add_device_group_guide_ment = "Please fill the information to continue";

        //functions ----------------------------------------------------------------------------------------------------
        $scope.add_deviceGroup = function(){//add device group
            var arg = {};

            arg["name"] = $scope.group.name;
            arg["flag_df_group"] = $scope.group.flag_df_group;
            arg["flag_confirm"] = $scope.group.flag_confirm;
            arg["member_fk"] = user.pk;

            if($scope.group.name != null){
                DeviceGroups.save(arg, function(data){
                    if(data.status == 200){
                        groups.push(data.objects);
                        $modalInstance.dismiss('cancel');
                    }else{
                        alert("Add Group Error");
                    }
                });
            }
        };

        //functions - If -----------------------------------------------------------------------------------------------
        $scope.if_disableAdd = function(){//if disabled - side btn
            var disableFlag = true;

            if($scope.group.name != null){
                disableFlag = false;
            }

            return disableFlag;
        };
    });
