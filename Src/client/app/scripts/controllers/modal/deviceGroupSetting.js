/**
 * Created by ksm on 2015-02-25.
 */
'use strict';

angular.module('lmmApp')
    .controller('DeviceGroupSettingCtrl', function ($scope, $modalInstance) {
        //functions - basic --------------------------------------------------------------------------------------------
        $scope.ok = function() {//ok
            $modalInstance.close('ok');
        };

        $scope.close = function(){//close
            $modalInstance.close('no');
        };
    });