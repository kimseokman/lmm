/**
 * Created by ksm on 2015-03-17.
 */
'use strict';

angular.module('lmmApp')
    .controller('DevicePropertyCtrl', function ($scope, $modalInstance, device, Devices) {
        //inherit data -------------------------------------------------------------------------------------------------
        $scope.this_device = device;

        //default setting ----------------------------------------------------------------------------------------------
        $scope.mode_device_property_guide_ment = "Please click on the item that you want to modify";

        $scope.deviceProperty = [//device group data scaffold
            {name: "Alias", field: "alias", empty: "EMPTY"}
        ];

        //functions - content ------------------------------------------------------------------------------------------
        $scope.updateDevicePropertyByDB = function(field, data){
            var devicePropertyArg = {};
            devicePropertyArg[field] = data;

            Devices.update({pk: $scope.this_device.pk}, devicePropertyArg, function(data){
                if(data.status == 200){

                }else{
                    alert("Fail");
                }
            });
        };

        //functions - basic --------------------------------------------------------------------------------------------
        $scope.ok = function() {//ok
            $modalInstance.close('ok');
        };

        $scope.close = function(){//close
            $modalInstance.close('no');
        };
    });
