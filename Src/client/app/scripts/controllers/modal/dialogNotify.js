/**
 * Created by ksm on 2015-02-26.
 */
'use strict';

angular.module('lmmApp')
    .controller('DialogNotifyCtrl', function ($scope, $modalInstance, type) {
        $scope.notifyType = type;
        //functions - basic --------------------------------------------------------------------------------------------
        $scope.ok = function() {//ok
            $modalInstance.close('ok');
        };
    });