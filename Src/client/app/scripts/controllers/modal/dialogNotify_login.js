/**
 * Created by ksm on 2015-03-10.
 */
'use strict';

angular.module('lmmApp')
    .controller('DialogNotifyLoginCtrl', function ($scope, $modalInstance, info) {
        $scope.info = info;
        //functions - basic --------------------------------------------------------------------------------------------
        $scope.ok = function() {//ok
            $modalInstance.close('ok');
        };

        $scope.close = function(){//close
            var result = {};
            result['sendMailFlag'] = true;
            result['mail'] = $scope.info.mail;
            result['type'] = 'resend';

            $modalInstance.close(result);
        };
    });

