/**
 * Created by ksm on 2015-02-26.
 */
'use strict';

angular.module('lmmApp')
    .controller('DialogConfirmCtrl', function ($scope, $modalInstance, type, data) {
        //inherit data -------------------------------------------------------------------------------------------------
        $scope.confirmType = type;

        //default setting ----------------------------------------------------------------------------------------------
        $scope.confirm_guide_ment = "";

        //data setting -------------------------------------------------------------------------------------------------
        if($scope.confirmType == "MemberRemove"){
            $scope.confirm_guide_ment = "Would you really delete it?";
        }
        //functions - basic --------------------------------------------------------------------------------------------
        $scope.ok = function() {//ok
            if($scope.confirmType == "MemberRemove"){
                var arg = {};
                arg["msg"] = "ok";
                arg["pk"] = data.pk;
                $modalInstance.close(arg);
            }else{
                $modalInstance.close('ok');
            }
        };

        $scope.close = function(){//close
            $modalInstance.close('no');
        };
    });