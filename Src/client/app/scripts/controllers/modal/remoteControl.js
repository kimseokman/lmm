/**
 * Created by ksm on 2015-03-19.
 */
'use strict';

angular.module('lmmApp')
    .controller('RemoteControlCtrl', function ($scope, $modalInstance, device) {
        //inherit data -------------------------------------------------------------------------------------------------
        var device = device;

        //default setting ----------------------------------------------------------------------------------------------
        $scope.remote_control_guide_ment = "Do you want to connect?";
        //functions - basic --------------------------------------------------------------------------------------------
        $scope.ok = function() {//ok
            $modalInstance.close('ok');
        };

        $scope.close = function(){//close
            $modalInstance.close('no');
        };
    });