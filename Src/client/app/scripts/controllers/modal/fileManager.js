/**
 * Created by ksm on 2015-03-19.
 */
'use strict';

angular.module('lmmApp')
    .controller('FileManagerCtrl', function ($scope, $modalInstance, device) {
        //inherit data -------------------------------------------------------------------------------------------------
        var device = device;

        //default setting ----------------------------------------------------------------------------------------------
        $scope.file_manager_guide_ment = "Do you want to execute?";
        //functions - basic --------------------------------------------------------------------------------------------
        $scope.ok = function() {//ok
            $modalInstance.close('ok');
        };

        $scope.close = function(){//close
            $modalInstance.close('no');
        };
    });