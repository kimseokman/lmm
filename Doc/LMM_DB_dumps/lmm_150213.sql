-- MySQL dump 10.13  Distrib 5.1.41, for Win32 (ia32)
--
-- Host: localhost    Database: lmm
-- ------------------------------------------------------
-- Server version	5.1.41-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_history`
--

DROP TABLE IF EXISTS `access_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_history` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `type_id` int(10) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `fk_users_pk` int(10) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='AccessHitory Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_history`
--

LOCK TABLES `access_history` WRITE;
/*!40000 ALTER TABLE `access_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `access_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corps`
--

DROP TABLE IF EXISTS `corps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corps` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `ceoName` varchar(255) DEFAULT NULL,
  `corpRegiNumber` varchar(255) DEFAULT NULL,
  `tel` varchar(30) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `homePageUri` varchar(255) DEFAULT NULL,
  `fk_users_pk` int(10) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Corpration Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corps`
--

LOCK TABLES `corps` WRITE;
/*!40000 ALTER TABLE `corps` DISABLE KEYS */;
/*!40000 ALTER TABLE `corps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_groups`
--

DROP TABLE IF EXISTS `device_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_groups` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `createDateTime` datetime DEFAULT NULL,
  `fk_users_pk` int(10) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Device Group Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_groups`
--

LOCK TABLES `device_groups` WRITE;
/*!40000 ALTER TABLE `device_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `device_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `deviceType_id` int(10) NOT NULL DEFAULT '-1',
  `deviceVersion` varchar(255) DEFAULT NULL,
  `fk_device_groups_pk` int(10) DEFAULT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Device Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `incident_history`
--

DROP TABLE IF EXISTS `incident_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `incident_history` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `occurDateTime` datetime NOT NULL,
  `deviceType_id` int(11) NOT NULL DEFAULT '-1',
  `alias` varchar(255) DEFAULT NULL,
  `error_code_id` int(10) NOT NULL DEFAULT '-1',
  `fk_devices_pk` int(10) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Incident History';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `incident_history`
--

LOCK TABLES `incident_history` WRITE;
/*!40000 ALTER TABLE `incident_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `incident_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `remote_history`
--

DROP TABLE IF EXISTS `remote_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `remote_history` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `actionDateTime` datetime NOT NULL,
  `deviceType_id` int(10) NOT NULL DEFAULT '-1',
  `actionType_id` int(10) NOT NULL DEFAULT '-1',
  `command_id` int(10) NOT NULL DEFAULT '-1',
  `memo` text,
  `fk_devices_pk` int(10) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Remote History';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `remote_history`
--

LOCK TABLES `remote_history` WRITE;
/*!40000 ALTER TABLE `remote_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `remote_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servers`
--

DROP TABLE IF EXISTS `servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servers` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `clientDomain` varchar(255) NOT NULL,
  `serverDomain` varchar(255) NOT NULL,
  `secretServerKey` varchar(255) NOT NULL,
  `label` varchar(40) DEFAULT NULL,
  `defaultDeviceVolume` int(5) NOT NULL DEFAULT '10',
  `defaultUserVolume` int(5) NOT NULL DEFAULT '10',
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Server Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servers`
--

LOCK TABLES `servers` WRITE;
/*!40000 ALTER TABLE `servers` DISABLE KEYS */;
INSERT INTO `servers` VALUES (1,'http://192.168.1.201:8000/#/','http://192.168.1.201:8888/','2135434','@koino.net',10,10);
/*!40000 ALTER TABLE `servers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `fk_users_pk` int(10) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8 COMMENT='Token Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_groups`
--

DROP TABLE IF EXISTS `user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_groups` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `createDateTime` datetime DEFAULT NULL,
  `fk_users_pk` int(10) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='User Group Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_groups`
--

LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `authEmail` int(5) NOT NULL DEFAULT '0',
  `pw` varchar(255) NOT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `timeZone_id` int(10) NOT NULL DEFAULT '17',
  `dateFormat_id` int(10) NOT NULL DEFAULT '0',
  `userType_id` int(10) NOT NULL DEFAULT '0',
  `userAuth_id` int(10) NOT NULL DEFAULT '0',
  `autoRefreshInterval` int(10) NOT NULL DEFAULT '3000',
  `flag_autoRefresh` tinyint(1) NOT NULL DEFAULT '0',
  `flag_sendEmail` tinyint(1) NOT NULL DEFAULT '0',
  `flag_useOTP` tinyint(1) NOT NULL DEFAULT '0',
  `flag_createQRCode` tinyint(1) NOT NULL DEFAULT '0',
  `flag_duplicateLoginPrevent` tinyint(1) NOT NULL DEFAULT '0',
  `secretOtpKey` varchar(255) DEFAULT NULL,
  `addDeviceVolume` int(10) NOT NULL DEFAULT '-1',
  `createUserVolume` int(10) NOT NULL DEFAULT '-1',
  `latestLoginDateTime` datetime DEFAULT NULL,
  `latestLogoutDateTime` datetime DEFAULT NULL,
  `fk_users_pk` int(10) DEFAULT NULL,
  `fk_user_groups_pk` int(10) DEFAULT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='User Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-13 16:30:08
