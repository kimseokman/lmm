-- MySQL dump 10.13  Distrib 5.1.41, for Win32 (ia32)
--
-- Host: localhost    Database: lmm
-- ------------------------------------------------------
-- Server version	5.1.41-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_history`
--

DROP TABLE IF EXISTS `access_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_history` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `type_id` int(10) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `fk_users_pk` int(10) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='AccessHitory Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_history`
--

LOCK TABLES `access_history` WRITE;
/*!40000 ALTER TABLE `access_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `access_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corps`
--

DROP TABLE IF EXISTS `corps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corps` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `ceoName` varchar(255) DEFAULT NULL,
  `corpRegiNumber` varchar(255) DEFAULT NULL,
  `tel` varchar(30) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `homePageUri` varchar(255) DEFAULT NULL,
  `fk_users_pk` int(10) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Corpration Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corps`
--

LOCK TABLES `corps` WRITE;
/*!40000 ALTER TABLE `corps` DISABLE KEYS */;
/*!40000 ALTER TABLE `corps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_groups`
--

DROP TABLE IF EXISTS `device_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_groups` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `createDateTime` datetime DEFAULT NULL,
  `lastUpdateDateTime` datetime DEFAULT NULL,
  `flag_isRootGroup` tinyint(1) NOT NULL DEFAULT '0',
  `flag_needConfirm` tinyint(1) NOT NULL DEFAULT '1',
  `fk_users_pk` int(10) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='Device Group Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_groups`
--

LOCK TABLES `device_groups` WRITE;
/*!40000 ALTER TABLE `device_groups` DISABLE KEYS */;
INSERT INTO `device_groups` VALUES (18,'ROOT','2015-02-16 07:30:01','0000-00-00 00:00:00',1,0,24),(23,'test','2015-02-16 08:05:59','0000-00-00 00:00:00',0,0,24),(42,'testtttttttttttttttttttttttttttttttttttt','2015-03-03 02:31:35','2015-03-03 02:31:35',0,0,24);
/*!40000 ALTER TABLE `device_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `deviceType_id` int(10) NOT NULL DEFAULT '-1',
  `deviceOS_id` int(10) NOT NULL DEFAULT '-1',
  `deviceVersion` varchar(255) DEFAULT NULL,
  `fk_users_pk` int(10) DEFAULT NULL,
  `fk_device_groups_pk` int(10) DEFAULT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='Device Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
INSERT INTO `devices` VALUES (1,'1','window1',0,0,'window',24,18),(2,'2','window2',0,0,'window',24,18),(3,'3','window3',0,0,'window',24,18),(4,'4','window4',0,0,'window',24,18),(5,'5','linux1',0,1,'linux',24,18),(6,'6','linux2',0,1,'linux',24,18),(7,'7','linux3',0,1,'linux',24,18),(8,'8','linux4',0,1,'linux',24,18),(9,'9','linux5',0,1,'linux',24,18),(10,'10','android1',2,3,'android',24,18),(11,'11','android2',1,3,'android',24,18),(12,'12','android3',2,3,'android',24,18),(13,'13','android4',1,3,'android',24,18),(14,'14','ipone1',1,4,'ios',24,18),(15,'15','ipone2',1,4,'ios',24,18),(16,'16','ipone3',2,4,'ios',24,18),(18,'18','OS X1',0,2,'OS X',24,18),(19,'19','OS X2',0,2,'OS X',24,18),(20,'20','OS X3',0,2,'OS X',24,18);
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `incident_history`
--

DROP TABLE IF EXISTS `incident_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `incident_history` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `occurDateTime` datetime NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `deviceType_id` int(10) NOT NULL DEFAULT '-1',
  `deviceOS_id` int(10) NOT NULL DEFAULT '-1',
  `deviceVersion` varchar(255) DEFAULT NULL,
  `error_code_id` int(10) NOT NULL DEFAULT '-1',
  `fk_devices_pk` int(10) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Incident History';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `incident_history`
--

LOCK TABLES `incident_history` WRITE;
/*!40000 ALTER TABLE `incident_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `incident_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `remote_history`
--

DROP TABLE IF EXISTS `remote_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `remote_history` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `actionDateTime` datetime NOT NULL,
  `deviceType_id` int(10) NOT NULL DEFAULT '-1',
  `deviceOS_id` int(10) NOT NULL DEFAULT '-1',
  `deviceVersion` varchar(255) DEFAULT NULL,
  `actionType_id` int(10) NOT NULL DEFAULT '-1',
  `command_id` int(10) NOT NULL DEFAULT '-1',
  `memo` text,
  `fk_devices_pk` int(10) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Remote History';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `remote_history`
--

LOCK TABLES `remote_history` WRITE;
/*!40000 ALTER TABLE `remote_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `remote_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servers`
--

DROP TABLE IF EXISTS `servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servers` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) NOT NULL,
  `domain_label` varchar(255) NOT NULL,
  `secretServerKey` varchar(255) NOT NULL,
  `label` varchar(40) DEFAULT NULL,
  `defaultDeviceVolume` int(5) NOT NULL DEFAULT '10',
  `defaultUserVolume` int(5) NOT NULL DEFAULT '10',
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Server Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servers`
--

LOCK TABLES `servers` WRITE;
/*!40000 ALTER TABLE `servers` DISABLE KEYS */;
INSERT INTO `servers` VALUES (1,'http://192.168.1.201:8000/#/','local','2135434','@linkmemine.com',10,10),(2,'http://linkmemine.com','test','1111111','@linkmemine.com',10,10);
/*!40000 ALTER TABLE `servers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `fk_users_pk` int(10) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=234 DEFAULT CHARSET=utf8 COMMENT='Token Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
INSERT INTO `tokens` VALUES (165,'ksm@koino.net','cea3de29b222c0391a0a2e8f5614e8fb4de5387a',23),(166,'ksm@koino.net','7fe377b5333e29124fd68d44c38d68a8196bc9f0',24),(167,'ksm@koino.net','6c6e2390b629a906fdce7141ec280001703c61c1',24),(168,'ksm@koino.net','6ad90a019bdc77951e2e1d82849a6d1c2d9e9558',24),(169,'ksm@koino.net','afbd9513e43fa31be12f184b7b90fffb7e8982df',24),(170,'ksm@koino.net','38262690bfa53dd003a15ef60ea58fdd0e9a9d39',24),(171,'ksm@koino.net','663d7beb95733ffaf566370aeb9f0ad8405e3131',24),(172,'ksm@koino.net','53379f89bacf55033bbc24e902249339349eb061',24),(173,'ksm@koino.net','a0efc5678892d8501dbca566b93d45114053ebe7',24),(174,'ksm@koino.net','3254aaf7a6c38ec0cbf6c5dd8dd7f1c6cae1d973',24),(175,'ksm@koino.net','94ec98376171a2859096e12cdec1df5be3bae1f4',24),(176,'ksm@koino.net','4ddc6dcb1e2a0f3edd26ce0ab58be6edd06c935d',24),(177,'ksm@koino.net','19e5845a2063419fc44f8224072a4bfc8df726b0',24),(178,'ksm@koino.net','ab62e20a2f5afc8d8f3786f761fbdb137b6c6b1b',24),(179,'ksm@koino.net','f18367709b6f8f683990dfb8798161f0d429d0a5',24),(180,'ksm@koino.net','bddfbac9f8cff786a87d8c3d65c8689141269049',24),(181,'ksm@koino.net','9d6786a95c256acbc2f83912df3a15f7c004bffb',24),(182,'ksm@koino.net','33a071dacaff179233cc0da8f79b8756dc100361',24),(183,'ksm@koino.net','de5b416bc790b3e469448df9501af1bf4a4bb3db',24),(184,'ksm@koino.net','31cad8d21b0210d13479f768b0cacd2c6c42f1af',24),(185,'ksm@koino.net','8f4df9c64df87edaa7726236d541b909337a238d',24),(186,'ksm@koino.net','7b6b03a5d4705820483976386625ac2db2ab140c',24),(187,'ksm@koino.net','427d380fb2532181df14114f822521ee1fbd4a73',24),(188,'ksm@koino.net','a50cea56626bddd86f494db4ddb1cbcc324e6e0d',24),(189,'ksm@koino.net','95d11ac1db35a5c56f38214992b669dabb7b51f8',24),(190,'ksm@koino.net','76662742ce52c664de07cebad5def0e60b446a21',24),(191,'ksm@koino.net','9820a26bd5179bfd4684607f888c2d132ec4ccf5',24),(192,'ksm@koino.net','2bed4942322d53a536e35c37390bcf18ed534b87',24),(193,'ksm@koino.net','440afd401a3bbf2664f9a7430262422a95f128e1',24),(194,'ksm@koino.net','5b6addafe49d34c91935681ca11f1dc48f64ace3',24),(195,'ksm@koino.net','add44bff6da03557e99e321e47ea022b84e5d90e',24),(196,'ksm@koino.net','b3fc5b55c6125c8f188f590d5d9c0e77f2959be7',24),(197,'ksm@koino.net','bc11da20786ecad7a56843f642f2a2489ea42013',24),(198,'ksm@koino.net','596c973779428f5f25038f0fda8a711db449a6a4',24),(199,'ksm@koino.net','0b422edbf745b2bc26e00e1803b9ec2a20e39459',24),(200,'ksm@koino.net','d4ec5f28c8b4c54d5768d76f677459f81c6b5d25',24),(201,'ksm@koino.net','2611db4b90d3bd28496c01bebd4e87da18aaa1ae',24),(202,'ksm@koino.net','728ed01d2cdf15aa0b6387d2ff9e8b3a24207af9',24),(203,'ksm@koino.net','7712ccf05ea7bcefbca87cefc4afc3af8fbc0334',24),(204,'ksm@koino.net','3b1bae17238cdbd3cc334d4af5e6004c6e1d0222',24),(205,'ksm@koino.net','abad873e06572b68919a232763ce099a1e7fc6a5',24),(206,'ksm@koino.net','4be2ef447e17d5bad8034c560f556553d08aab4d',24),(207,'ksm@koino.net','56d6e969ca1cc1b2595af2272de2708a4a7ed6dc',24),(208,'ksm@koino.net','285149baf31e481be0c020cfe5afc0561b5a1a28',24),(209,'ksm@koino.net','58c7661cdef6250de78a99f964cdd54e6435f10f',24),(210,'ksm@koino.net','2dce0f10951efa956610cfb9dba2656d7cedc760',24),(211,'ksm@koino.net','bea75f4b7e0187a24f8d0eee02b6fe57f3c3ad50',24),(212,'ksm@koino.net','68e2927a4b7c3228f99d6b750be013c543c9174d',24),(213,'ksm@koino.net','595816fbbc203dd0607cf42d421421cd67d16e04',24),(214,'ksm@koino.net','f233dc6eefb805dc5cc39ee35e71958ab5828a7a',24),(215,'ksm@koino.net','deeb152173a880a2771da752c3023d5ec676b659',24),(216,'ksm@koino.net','6d34719db8cf398b76916d4aa0a8d5eb910b93a8',24),(217,'ksm@koino.net','90486b5eeedc2095aa78bdacb6f9f46b164d26d3',24),(218,'ksm@koino.net','ddaec585c696d58b9bc32f7f2df6ba550e95f9b8',24),(219,'ksm@koino.net','03a2ef4b696fe9c70acc32ec81b0adb96ebd37d9',24),(220,'ksm@koino.net','e37cb23ee80b2d3a418a64765f54b0386d4603ae',24),(221,'ksm@koino.net','d98c8ab1503d6b24760a7aae85fdc18c385a0310',24),(222,'ksm@koino.net','9c9c4ccaa68a899ce5f5652f14620d63dd91a412',24),(223,'ksm@koino.net','94c2b84c01b13636e85ac06a87c15927cb3ecc9e',24),(224,'ksm@koino.net','60119fc46ad8f2da2502d7044f21039e10ce1a8c',24),(225,'ksm@koino.net','d40b2e340bfd5b7e6675924aef3dccfa2afd98f8',24),(226,'ksm@koino.net','f4ca4438da0fbda2018c701c21e0b56246375422',24),(227,'ksm@koino.net','11765385e58f6617c4db7e08d3b9dc6965b28108',24),(228,'ksm@koino.net','218aa5a7c3dec9f9b71e454cec2e6c85f3195423',24),(229,'ksm@koino.net','9eb04642c7b468e5e2a17f0a297419d9385e80d2',24),(230,'ksm@koino.net','f557e995adef42f2f33bf77c940eb0db6cc4a773',24),(231,'ksm@koino.net','d75147e1167d00b078538db5d1db055a6122f77e',24),(232,'ksm@koino.net','469d8081829decd9cc2c48c921263ab9b469e4bc',24),(233,'ksm@koino.net','fb1d217de103297381d8ffc6ccf2aac23723408f',24);
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `pk` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `authEmail` int(5) NOT NULL DEFAULT '0',
  `pw` varchar(255) NOT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `timeZone_id` int(10) NOT NULL DEFAULT '17',
  `dateFormat_id` int(10) NOT NULL DEFAULT '0',
  `userType_id` int(10) NOT NULL DEFAULT '0',
  `userAuth_id` int(10) NOT NULL DEFAULT '0',
  `autoRefreshInterval` int(10) NOT NULL DEFAULT '3000',
  `flag_autoRefresh` tinyint(1) NOT NULL DEFAULT '0',
  `flag_sendEmail` tinyint(1) NOT NULL DEFAULT '0',
  `flag_useOTP` tinyint(1) NOT NULL DEFAULT '0',
  `flag_createQRCode` tinyint(1) NOT NULL DEFAULT '0',
  `flag_duplicateLoginPrevent` tinyint(1) NOT NULL DEFAULT '0',
  `secretOtpKey` varchar(255) DEFAULT NULL,
  `addDeviceVolume` int(10) NOT NULL DEFAULT '-1',
  `createUserVolume` int(10) NOT NULL DEFAULT '-1',
  `latestLoginDateTime` datetime DEFAULT NULL,
  `latestLogoutDateTime` datetime DEFAULT NULL,
  `fk_users_pk` int(10) DEFAULT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='User Information';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (24,'ksm@koino.net',1,'*94BDCEBE19083CE2A1F959FD02F964C7AF4CFC29',NULL,NULL,17,0,4,0,3000,0,0,1,0,0,'0.793826071173911',10,10,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-10 15:50:51
